class Timer {
  constructor() {
    this.timeout = null
  }
}

Timer.prototype.reset = (cb) => {
  if(this.timeout != null) clearTimeout(this.timeout)
  this.timeout = setTimeout(function() {
    cb()
  }, 4000)
}

Timer.prototype.stop = () => {
  clearTimeout(this.timeout)
}


export default Timer
