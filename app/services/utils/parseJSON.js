import _map from 'lodash/map'
import _set from 'lodash/set'

const parseJSONArr = (arr, key) => {
  return _map(arr, (item) => {
    return parseJSONObj(item, key)
  })
}

const parseJSONObj = (obj, key) => {
	try {
    _set(obj, key, JSON.parse(obj[key]))
  } catch(e) {}
	return obj
}

export { parseJSONArr, parseJSONObj }
