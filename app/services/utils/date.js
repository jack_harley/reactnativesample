import moment from 'moment'

const formatTime = (s) => {
  addZ = (n) => {
    return (n<10? '0':'') + n
  }

  const secs = Math.round(s % 60)
  s = (s - (s % 60)) / 60
  const mins = s % 60

  return addZ(mins) + ':' + addZ(secs)

}

const formatDate = (added) => {
  const TWELVE_HOURS = 12 * 60 * 60 * 1000
  const ONE_HOUR = 60 * 60 * 1000
  const d = new Date(added)
  const x = (new Date) - d
  if (x < ONE_HOUR) {
    return Math.round(x / 60 / 1000)+' mins'
  }else if (x < TWELVE_HOURS) {
    return Math.round(x / 60 / 60 /1000)+' hrs'
  } else {
    return moment(d).format("d MMM [at] h:mm a")
  }
}

export { formatDate, formatTime }
