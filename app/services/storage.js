const config = require('../config/config')
import _remove from 'lodash/remove'
import _map from 'lodash/map'
import { AsyncStorage } from 'react-native'

const globKey = config.app.name + 's3rg3943f34'

// console.log('// FLUSH-CACHE (REMOVE!!!)');
// AsyncStorage.setItem(globKey, '{}')
//   .then(() => {
//     console.log('flushed');
//   }, (err) => {
//     console.log('err', err);
//   });

export function storageGet(key) {

  const deferred = new Promise(function(resolve, reject) {
    AsyncStorage.getItem(globKey)
      .then(function(localData) {
        if(key && localData) {
          resolve(JSON.parse(localData)[key])
        } else {
          resolve(JSON.parse(localData))
        }
      }, function(err) {
        reject(err)
      })
  });

  return deferred
}

export function storageSet(key, payload) {

  const deferred = new Promise(function(resolve, reject) {
    AsyncStorage.getItem(globKey)
      .then(function(localData) {
        if(!localData) {
          localData = {}
        } else {
          localData = JSON.parse(localData)
        }
        localData[key] = payload
        localData = JSON.stringify(localData)
        return AsyncStorage.setItem(globKey, localData)
      }, function(err) {
        reject(err)
      })
      .then(function() {
        resolve(payload)
      }, function(err) {
        reject(err)
      })
  });

  return deferred
}

export function storageAddToArray(key, payload) {
  const deferred = new Promise(function(resolve, reject) {
    AsyncStorage.getItem(globKey)
      .then(function(localData) {
        if(!localData) {
          localData = {}
        } else {
          localData = JSON.parse(localData)
        }
        if(!localData[key]) localData[key] = []
        localData[key].push(payload)
        localData = JSON.stringify(localData)
        return AsyncStorage.setItem(globKey, localData)
      }, function(err) {
        reject(err)
      })
      .then(function() {
        resolve(payload)
      }, function(err) {
        reject(err)
      })
  });

  return deferred
}

export function storageRemoveFromArray(key, dataKey, _id) {
  const deferred = new Promise(function(resolve, reject) {
    let data = [];
    AsyncStorage.getItem(globKey)
      .then(function(localData) {
        if(!localData) {
          localData = {}
        } else {
          localData = JSON.parse(localData)
        }
        if(!localData[key]) localData[key] = []
        localData[key] = _remove(localData[key], (item)=>{
        	return item[dataKey] != _id
        })
        data = localData[key]
        localData = JSON.stringify(localData)
        return AsyncStorage.setItem(globKey, localData)
      }, function(err) {
        reject(err)
      })
      .then(function() {
        resolve(data)
      }, function(err) {
        reject(err)
      })
  });

  return deferred
}

export function storageUpdateInArray(key, idKey, _id, payload) {
  const deferred = new Promise(function(resolve, reject) {
    AsyncStorage.getItem(globKey)
      .then(function(localData) {
        if(!localData) {
          localData = {}
        } else {
          localData = JSON.parse(localData)
        }
        if(!localData[key]) localData[key] = []
        localData[key] = _map(localData[key], (item) => {
          if( item[idKey] != _id ) return item
          return payload
        })
        localData = JSON.stringify(localData)
        return AsyncStorage.setItem(globKey, localData)
      }, function(err) {
        reject(err)
      })
      .then(function() {
        resolve(payload)
      }, function(err) {
        reject(err)
      })
  });

  return deferred
}
