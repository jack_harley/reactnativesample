import _map from 'lodash/map'
import { get, post, put, remove } from './fetch'
import { storageAddToArray, storageRemoveFromArray, storageUpdateInArray, storageSet, storageGet } from '../storage'
import { parseJSONArr } from '../utils'

const gameIsFinished = (gameObj, opponentId) => {
  return gameObj.data.attempt[opponentId].submitted
}

export default Game = {
  get: (userId) => {
    let serverGames
    const deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + userId + '/game')
        .then((games) => {
          serverGames = parseJSONArr(games, 'data')
          return storageGet('localGames')
        })
        .then((localGames) => {
          if(!localGames) localGames = []
          resolve(combineGames(serverGames, localGames))
        }, (err) => { reject(err) })
    })
    return deferred
  },
  getById: (gameId, userId) => {
    return new Promise(function(resolve, reject) {
      get('/api/user/' + userId + '/game/' + gameId)
        .then((gameObj) => {
          opponentId = gameObj.opponent._id
          resolve({finished: gameIsFinished(gameObj, opponentId), data: gameObj})
        })
    })
  },
  update: (userId, gameId, opponentId, payload) => {
    payload.opponentId = opponentId
    let serverGame = null
    const deferred = new Promise(function(resolve, reject) {
      put('/api/user/' + userId + '/game/' + gameId, payload)
        .then((game) => {
          console.log('game', game);
          serverGame = game
          return storageUpdateInArray('games', 'gameId', gameId, game)
        })
        .then(() => {
          return storageGet('localGames')
        })
        .then((localGames) => {
          if(!localGames) localGames = {}
          console.log(localGames);
          resolve({server: serverGame, local: localGames[serverGame.gameId]})
        })
    })
    return deferred
  },

  delete: (gameId, userId, opponentId) => {
    const deferred = new Promise(function(resolve, reject) {
      remove('/api/user/' + userId + '/game/' + gameId)
        .then((thing) => {
          return remove('/api/user/' + opponentId + '/game/' + gameId + '?userId=' + userId)
        }, (err) => { reject({ type: 'user',  err: err}) })
        .then(() => {
          resolve({ gameId: gameId })
        }, (err) => { reject({ type: 'opponent', err: err }) })
    });

    return deferred;
  },

  saveGameLocally: (gameId, data, key) => {
    const deferred = new Promise(function(resolve, reject) {
      storageGet('localGames')
        .then((games) => {
          if(key) {
            games[gameId][key] = data
          } else {
            games[gameId] = data
          }
          return storageSet('localGames', games)
        }, (err) => { reject(err) })
        .then(() => {
          resolve(data)
        }, (err) => { reject(err) })
    })
    return deferred
  },

  saveAttemptLocally: (gameId, attempt, game) => {
    const deferred = new Promise(function(resolve, reject) {
      let attempts = []
      storageGet('localGames')
        .then((games) => {
          if(!games) games = {}
          if(!games[gameId]) games[gameId] = game
          games[gameId].attempts.push(attempt)
          attempts = games[gameId].attempts
          return storageSet('localGames', games)
        }, (err) => { reject(err) })
        .then(() => {
          resolve(attempts)
        }, (err) => { reject(err) })
    })
    return deferred
  },

  sendChallenge: (userId, challenge) => {
    let game= null
    const deferred = new Promise(function(resolve, reject) {
      post('/api/user/' + userId + '/game/', challenge)
        .then((response) => {
          game = response
          return storageAddToArray('games', game)
        }, (err) => { reject(err) })
        .then(() => {
          resolve(game)
        }, (err) => { reject(err) })
    })
    return deferred
  },
  storeChallenge: (challenge) => {
    const deferred = new Promise(function(resolve, reject) {
        storageAddToArray('unsentChallenges', challenge)
          .then(() => {
            resolve(challenge)
          }, (err) => { reject(err) })
    })
    return deferred
  },
  deleteStoredChallenge: (gameId) => {
    const deferred = new Promise(function(resolve, reject) {
        storageRemoveFromArray('unsentChallenges', 'gameId', gameId)
          .then((data) => {
            resolve(data)
          }, (err) => { reject(err) })
    })
    return deferred
  },
  acceptChallenge: (gameId, userId, opponentId, state) => {
    const deferred = new Promise(function(resolve, reject) {
      let result = null
      put("/api/user/" + userId + "/game/" + gameId + "/opponent/" + opponentId + "/accept/", {state: state})
        .then((res) => {
          result = res
          return Game.deleteStoredChallenge(gameId)
        }, (err) => { reject(err) })
        .then(() => {
          resolve({server: result, local: undefined})
        }, (err) => { reject(err) })
    })
    return deferred
  },
  rejectChallenge: (gameId, userId, opponentId) => {
    const deferred = new Promise(function(resolve, reject) {
      put("/api/user/" + userId + "/game/" + gameId + "/opponent/" + opponentId + "/decline/", {msg: null})
        .then((res) => {
          return Game.deleteStoredChallenge(gameId)
        }, (err) => { reject(err) })
        .then(() => {
          resolve(gameId)
        }, (err) => { reject(err) })
    })
    return deferred
  }
}

let combineGames = (serverGames, localGames) => {
  return _map(serverGames, (game) => {
    let item = {server: game}
    item.local = localGames[Game.gameId]
    return item
  })
}
