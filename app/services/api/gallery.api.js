import { get, post, buildUrl } from './fetch'

export default Gallery = {

  get: function(params, userId) {

    // params are 'sort', 't', 'from'

    let url = userId ? '/api/user/' + userId + '/gallery' : '/api/gallery/'
        url = buildUrl(url, params)

    const deferred = new Promise((resolve, reject) => {
      get(url)
        .then((galleryItems) => {
          resolve(galleryItems)
        }, (err) => {
          reject(err)
        })
    });
    return deferred
  },

  getById: function(gameId, userId) {
    let url = userId ? '/api/user/' + userId + '/gallery/' + gameId : '/api/gallery/' + gameId
    return get(url)
  },

  getSuggested: function(userId, ids) {
    const deferred = new Promise((resolve, reject) => {
      get('/api/user/' + userId + '/gallery/suggested/' + ids[0] + '/' + ids[1])
        .then((galleryItems) => {
          resolve(galleryItems)
        }, (err) => {
          reject(err)
        })
    });
    return deferred
  },

  find: function(str) {
    const deferred = new Promise((resolve, reject) => {
      get('/api/gallery/search/' + str)
        .then((items) => {
          resolve(items)
        }, (err) => {
          reject(err)
        })
    });
    return deferred
  },

  create: function() {
    const deferred = new Promise((resolve, reject) => {
      post('/api/gallery/')
        .then((res) => {
          resolve(res)
        }, (err) => {
          reject(err)
        })
    })
  },

  upvote: function(gameId, user, targetUserId) {
    const payload = {
      userId: user._id,
      userData: {
        firstName: user.firstName,
        lastName: user.lastName,
        avatar: user.avatar
      },
      targetUserIds: targetUserId,
      notification: {
        data: {
          text: user.firstName + ' ' + user.lastName,
          img: user.avatar
        },
        type: 'upvote'
      }
    }
    const deferred = new Promise((resolve, reject) => {
      post('/api/user/' + user._id + '/gallery/' + gameId + '/upvote', payload)
        .then(() => {
          resolve()
        }, (err) => { reject(err) })
    })
    return deferred
  }
}
