import { get } from './fetch'

export default profile = {

  get: function(_id) {

    deferred = new Promise(function(resolve, reject) {
      get('/api/profile/' + _id)
        .then((user) => {
          user.user.data.avatar = JSON.parse(user.user.data.avatar)
          resolve(user.user.data)
        }, (err) => {
          reject(err)
        })
    });
    return deferred;
  }

}
