import { NetInfo } from 'react-native'

import { storageGet, storageSet } from '../storage'
import config from '../../config/config'

let isConnected = true;

const onConnectivityChange = (type) => {
  isConnected = type == 'NONE' ? false : true
}

NetInfo.fetch().done((type) => {
  onConnectivityChange(type)
  NetInfo.addEventListener('change', (type) => {onConnectivityChange(type)} )
});

export async function get(path) {

  console.log('path', config.baseUrl + path);

  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });

  // Get auth token!
  const token = await storageGet('auth_token')
  if(token) headers.append('Authorization', 'Bearer ' + token)

  if(!isConnected) return storageGet(path)
  return fetch(config.baseUrl + path, {headers: headers})
           .then((response) => response.json())
           .then((response) => {
             storageSet(path, response)
             return(response)
           })
}

export async function post(path, payload) {

  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })

  // Get auth token!
  const token = await storageGet('auth_token')
  if(token) headers.append('Authorization', 'Bearer ' + token)

  const options = {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(payload)
  }

  // return fetch(config.baseUrl + path, options).then((response) => response.json())

  return fetch(config.baseUrl + path, options).then((response) => {
    try {
      return response.json()
    } catch(e) {
      return Promise.resolve(response)
    }
  })
}

export async function put(path, payload) {

  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });

  // Get auth token!
  const token = await storageGet('auth_token')
  if(token) {
    headers.append('Authorization', 'Bearer ' + token)
  } else {
    return Promise.reject({message: 'User not logged in'})
  }

  const options = {
    method: 'PUT',
    headers: headers,
    body: JSON.stringify(payload)
  }

  return fetch(config.baseUrl + path, options).then((response) => response.json())
}

export async function remove(path) {

  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });

  // Get auth token!
  const token = await storageGet('auth_token')
  if(token) {
    headers.append('Authorization', 'Bearer ' + token)
  } else {
    return Promise.reject({message: 'User not logged in'})
  }

  const options = {
    method: 'DELETE',
    headers: headers
  }

  return fetch(config.baseUrl + path, options).then((response) => {
    try {
      return response.json()
    } catch(e) {
      return Promise.resolve(response)
    }
  })

}

// UTIL

export function buildUrl(base, params) {
  var qs = "";
  for(var key in params) {
    var value = params[key];
    if(value) qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";

  }
  if (qs.length > 0){
    qs = qs.substring(0, qs.length-1); //chop off last "&"
    base = base + "?" + qs;
  }
  return base;
}
