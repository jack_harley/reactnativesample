import { get, post, remove } from './fetch'
import { storageSet, storageGet } from '../storage'

export default feed = {

  get: function(_id) {
    const deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + _id + '/feed')
        .then((items) => {
          resolve(items)
        }, (err) => {
          reject(err)
        })
    })
    return deferred;
  },

  following: {
    get: function(userId) {
      const deferred = new Promise(function(resolve, reject) {
        get('/api/user/' + userId + '/following')
          .then((items) => {
            resolve(items)
          }, (err) => {
            reject(err)
          })
      })
      return deferred;
    },
    add: function(userId, followId, payload) {
      const deferred = new Promise(function(resolve, reject) {
        post('/api/user/' + userId + '/follow/' + followId, payload)
          .then((items) => {
            // Update local user obj
            storageGet('user')
              .then((user) => {
                user.followingCount = user.followingCount++
                return storageSet('user', user)
              }, (err) => { resolve('done') })
              .then(() => {
                resolve('done')
              }, (err) => { resolve('done') })

          }, (err) => {
            reject(err)
          })
      })
      return deferred;
    },
    remove: function(userId, followId) {
      const deferred = new Promise(function(resolve, reject) {
        remove('/api/user/' + userId + '/follow/' + followId)
          .then((items) => {
            // Update local user obj
            storageGet('user')
              .then((user) => {
                user.followingCount = user.followingCount--
                return storageSet('user', user)
              }, (err) => { resolve('done') })
              .then(() => {
                resolve('done')
              }, (err) => { resolve('done') })

          }, (err) => {
            reject(err)
          })
      })
      return deferred;
    }
  },

  followers: {
    get: function(userId) {
      const deferred = new Promise(function(resolve, reject) {
        get('/api/user/' + userId + '/followers')
          .then((items) => {
            resolve(items)
          }, (err) => {
            reject(err)
          })
      })
      return deferred;
    }
  }


}
