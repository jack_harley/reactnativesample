import _map from 'lodash/map'

import { get, put, post, remove } from './fetch'
import { storageSet, storageGet } from '../storage'

export default user = {
  login: function(accessData) {
    let userObj = null
    const deferred = new Promise(function(resolve, reject) {
      post('/api/user', accessData)
        .then((response) => {
          userObj = response.user.data
          try {
            userObj.avatar = JSON.parse(userObj.avatar)
          } catch(e) {}
          storageSet('user', userObj)
            .then(function() {
              return storageSet('auth_token', response.token)
            }, (err) => {reject(err)})
            .then(function() {
              response.user.games = ( response.user.games || [] )
              return storageSet('user.games', response.user.games)
            })
            .then(function() {
              response.user.opponents = ( response.user.opponents || [] )
              return storageSet('user.opponents', response.user.opponents)
            })
            .then(function() {
              response.user.notifications = ( response.user.notifications || [] )
              return storageSet('user.notifications', response.user.notifications)
            })
            .then(function() {
              resolve(response)
            }, (err) => {reject(err)})
        }, (err) => {
          reject(err)
        })
    });
    return deferred;
  },

  get: function(_id) {

    deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + _id)
        .then((user) => {
          try {
            user.user.data.avatar = JSON.parse(user.user.data.avatar)
          } catch(e) {}
          resolve(user.user.data)
        }, (err) => {
          storageGet('user')
            .then((user) => {
              resolve(user)
            }, (err) => {
              reject(err)
            })
        })
    });
    return deferred;
  },

  edit:function (_id, user) {
    var deferred = new Promise(function(resolve, reject) {
      put('/api/user/' + _id, user)
        .then((user) => {
          resolve(user)
        }, (err) => {
          reject(err)
        })
    });
    return deferred;
  },

  delete: function(url) {
    var deferred = new Promise(function(resolve, reject) {
      remove('/api/user/' + _id)
        .then((user) => {
          resolve(user)
        }, (err) => {
          reject(err)
        })
    });
    return deferred;
  },

  find: function(str) {
    var deferred = new Promise(function(resolve, reject) {
      get('/api/user/find/' + str)
        .then((users) => {
          users = _map(users, (user) => {
            try {
              user.avatar = JSON.parse(user.avatar)
            } catch(e) {}
            return user
          })
          resolve(users)
        }, (err) => {
          reject(err)
        })
    });
    return deferred
  },

  games: function(_id) {

    const deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + _id + '/games')
        .then((games) => {
          storageSet('user.games', games)
            .then(() => {
              resolve(games)
            }, (err) => { reject(err) })
        }, (err) => {
          storageGet('user.games')
            .then((games) => {
              resolve(games)
            }, (err) => {
              reject(err)
            })
        })
    });

    return deferred
  },

  opponents: function(_id) {

    const deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + _id + '/opponents')
        .then((opponents) => {
          storageSet('user.opponents', opponents)
            .then(() => {
              resolve(opponents)
            }, (err) => { reject(err) })
        }, (err) => {
          storageGet('user.opponents')
            .then((opponents) => {
              resolve(opponents)
            }, (err) => { reject(err) })
        })
    })

    return deferred
  },

  notifications: function(_id) {
    const deferred = new Promise(function(resolve, reject) {
      get('/api/user/' + _id + '/notifications')
        .then((notifications) => {
          storageSet('user.notifications', notifications)
            .then(() => {
              resolve(notifications)
            }, (err) => { reject(err) })
        }, (err) => {
          storageGet('user.notifications')
            .then((notifications) => {
              resolve(notifications)
            }, (err) => { reject(err) })
        })
    });

    return deferred
  },

  followers: function(_id) {
      const deferred = new Promise(function(resolve, reject) {
        get('/api/user/' + _id + '/followers')
          .then((followers) => {
            storageSet('user.followers', followers)
              .then(() => {
                followers = _map(followers, (user) => {
                  try {
                    user.user = JSON.parse(user.user)
                    try {
                      user.user.avatar = JSON.parse(user.user.avatar)
                    } catch(e) {}
                  } catch(e) {}
                  return user
                })
                resolve(followers)
              }, (err) => { reject(err) })
          }, (err) => {
            storageGet('user.followers')
              .then((followers) => {
                resolve(followers)
              }, (err) => { reject(err) })
          })
      });

      return deferred
  },

  following: function(_id) {
      const deferred = new Promise(function(resolve, reject) {
        get('/api/user/' + _id + '/following')
          .then((following) => {
            storageSet('user.following', following)
              .then(() => {
                following = _map(following, (user) => {
                  try {
                    user.user = JSON.parse(user.user)
                    try {
                      user.user.avatar = JSON.parse(user.user.avatar)
                    } catch(e) {}
                  } catch(e) {}
                  return user
                })
                resolve(following)
              }, (err) => { reject(err) })
          }, (err) => {
            storageGet('user.following')
              .then((following) => {
                resolve(following)
              }, (err) => { reject(err) })
          })
      });

      return deferred
  },

  follow: function(userId, followId, payload) {
    let response = {
      followerId: followId,
      userId: userId,
      user: payload.following
    }
    const deferred = new Promise(function(resolve, reject) {
      post('/api/user/' + userId + '/follow/' + followId, payload)
        .then((items) => {
          // Update local user obj
          storageGet('user')
            .then((user) => {
              user.followingCount = user.followingCount++
              return storageSet('user', user)
            }, (err) => { resolve('done') })
            .then(() => {
              return storageGet('user.followers')
            }, (err) => { resolve(response) })
            .then((followers) => {
              followers.push(response)
              return storageSet('user.followers', response)
            }, (err) => { resolve(response) })
            .then(() => {
              resolve(response)
            }, (err) => { resolve(response) })

        }, (err) => {
          reject(err)
        })
    })
    return deferred;
  },

  unfollow: function(userId, followingId) {
    const deferred = new Promise(function(resolve, reject) {
      remove('/api/user/' + userId + '/follow/' + followingId)
        .then((items) => {
          storageGet('user')
            .then((user) => {
              user.followingCount = user.followingCount--
              return storageSet('user', user)
            }, (err) => { resolve(followingId) })
            .then(() => {
              return storageGet('user.following')
            }, (err) => { resolve(followingId) })
            .then((following) => {
              following = _remove(following, (n) => {
                return n.followingId == followingId
              })
              return storageSet('user.following', following)
            }, (err) => { resolve(followingId) })
            .then(() => {
              resolve(followingId)
            }, (err) => { resolve(followingId) })

        }, (err) => {
          reject(err)
        })
    })
    return deferred;
  },

  challenges: function(_id) {
    const deferred = new Promise(function(resolve, reject) {
      let challenges = {}
      storageGet('unsentChallenges')
        .then((result) => {
          challenges.unsent = result
          return get('/api/user/' + _id + '/challenges')
        }, (err) => { reject(err) })
        .then((result) => {
          storageSet('user.challenges', result)
            .then(() => {
              challenges.received = result
              resolve(challenges)
            }, (err) => { reject(err) })
        }, (err) => {
          storageGet('user.challenges')
            .then((challenges) => {
              resolve(challenges)
            }, (err) => { reject(err) })
        })
    })

    return deferred
  },

  enablePushNotifications: function(userId, token) {
    return put('/api/user/' + userId + '/notifications/addToken', { deviceToken: token })
  }
}
