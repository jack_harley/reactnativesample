import User from './user.api'
import Game from './game.api'
import Gallery from './gallery.api'
import TimedDuel from './game.timedDuel.api'

export { User, Game, Gallery, TimedDuel }
