import _map from 'lodash/map'
import _omit from 'lodash/omit'
import { get, post, put } from './fetch'
import config from '../../config/config'
import { Game, Gallery } from './index'

const gameIsFinished = (gameObj, opponentId) => {
  return gameObj.data.attempt[opponentId].submitted
}

const preparePayloadForAttempt = (gameObj, userId) => {
  gameObj.data.attempt[userId] = {
    submitted: true,
    uri: `https://s3-eu-west-1.amazonaws.com/${config.aws.buckets.singleDest}/${gameObj.gameId}/${userId}/timedduel.mp4`,
    thumb: {
      small: {
        uri: `https://s3-eu-west-1.amazonaws.com/${config.aws.buckets.singleDest}/${gameObj.gameId}/${userId}/timedduel_small.png`
      },
      large: {
        uri: `https://s3-eu-west-1.amazonaws.com/${config.aws.buckets.singleDest}/${gameObj.gameId}/${userId}/timedduel_large.png`
      }
    }
  }
  const message = 'has submitted his attempt.';
  return { data: gameObj.data, notification: message }
}

export default timedDuel = {

  saveAttempt: (gameId, user) => {

    let isFinished = false,
        opponentId = null

    return new Promise(function(resolve, reject) {
      get('/api/user/' + user._id + '/game/' + gameId)
        .then((gameObj) => {
          opponentId = gameObj.opponent._id
          isFinished = gameIsFinished(gameObj, opponentId)
          if(isFinished) {
            const payload = preparePayloadForGallery(gameObj, user)
            return Gallery.create(payload);
          } else {
            const payload = preparePayloadForAttempt(gameObj, user._id)
            return Game.update(user._id, gameId, gameObj.opponent._id, payload)
          }
        })
        .then((res) => {
          // If is finished, delete game for both users
          if(isFinished) Game.delete(gameId, user._id, opponentId)
          resolve({ isFinished: isFinished, game: res })
        })
    });

  }
}
