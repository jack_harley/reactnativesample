import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { colours } from '../../styles'
import config from '../../config/config'
import Spinner from 'react-native-spinkit'

export default ({color}) => (
  <View style={styles.container}>
     {
       config.env == 'android'
       && <Spinner style={styles.spinner} isVisible={true} size={50} type='Circle' color={color?color:colours.greyscale.medium}/>
       || <Spinner style={styles.spinner} isVisible={true} size={50} type='Arc' color={color?color:colours.greyscale.medium}/>
     }
  </View>
)

var styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  spinner: {
    marginBottom: 40
  }
});
