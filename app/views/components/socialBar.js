import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableWithoutFeedback } from 'react-native'
import { colours } from '../../styles'
import Icon from 'react-native-vector-icons/FontAwesome'

import * as Animatable from 'react-native-animatable';
const AnimatedIcon = Animatable.createAnimatableComponent(Icon);

class LikeAction extends Component {
  constructor(props) {
    super(props)
  }
  state = {
    voted: this.props.voted,
    upvotes: this.props.upvotes
  }
  doAnimation() {
    this.refs.view.rubberBand(800).then((endState) => {
      if(endState.finished) {
        if(!this.state.voted) {
          this.props.vote(this.state.voted)
          this.setState({voted: !this.state.voted, upvotes: this.state.voted ? this.state.upvotes - 1 : this.state.upvotes + 1})
        }
      }
    })
  }
  render() {
    const name = this.state.voted ? "heart" : "heart-o"
    if(this.state.voted || this.props.disabled || (this.props.connectivity && !this.props.connectivity.connected)) {
      return <View style={styles.container}>
        <Icon style={{justifyContent: 'center', alignItems: 'center', marginBottom: 2}} size={14} color={colours.greyscale.dark} name={name} />
        <Text style={{fontSize: 11}}>{this.state.upvotes}</Text>
      </View>
    }
    return <View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => this.doAnimation()}>
        <Animatable.View style={styles.container} ref="view">
          <Icon name={name}
            size={14}
            iconStyle={{marginRight: 0}}
            style={{padding: 0}}
            color={colours.greyscale.dark}
            backgroundColor="transparent"></Icon>
        </Animatable.View>
      </TouchableWithoutFeedback>
      <Text style={{fontSize: 11, textAlign: 'center'}}>{this.state.upvotes}</Text>
    </View>
  }
  border(color) {
    return {
      borderColor:color,
      borderWidth: 2
    }
  }
}


export default ({upvotes, voted, vote, comments, views, connectivity, disabled}) => (
  <View style={styles.social} >
    <View style={styles.container}>
      <LikeAction vote={vote} voted={voted} upvotes={upvotes} connectivity={connectivity} disabled={disabled}/>
    </View>
    <View style={styles.container}>
      <Icon style={{justifyContent: 'center', alignItems: 'center', marginBottom: 2}} size={14} color={colours.greyscale.dark} name="comment-o" />
      <Text style={{fontSize: 11}}>{comments}</Text>
    </View>
  </View>
)

// For when views work
// <View style={styles.container}>
//   <Icon style={{justifyContent: 'center', alignItems: 'center', marginBottom: 2}} size={14} color={colours.greyscale.dark} name="eye" />
//   <Text style={{fontSize: 11}}>{views}</Text>
// </View>

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  social: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'stretch'
  }
})
