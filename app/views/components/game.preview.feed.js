const _ = require('lodash')
import React, { Component } from 'react'
import { View, TouchableHighlight, StyleSheet, Image, Text } from 'react-native'

import { colours } from '../../styles'
import SocialBar from './socialBar'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default ({route, size, _handleNavigate, data, game, disabled}) => (
  <TouchableHighlight onPress={() => _handleNavigate(route)}>
    <View style={styles.wrap}>
      <View style={size}>
        <Image style={size} source={game.source.thumb.large}>
          <View style={styles.videoLength}><Text style={styles.videoLengthText}>1:53</Text></View>
        </Image>
      </View>
      <View style={styles.content}>
        <View style={styles.row}>
          <View style={styles.titleWrap}>
            <Text style={styles.textTitle}>{_.toUpper(game.player1.firstName)} {_.toUpper(game.player1.lastName)}</Text>
            <Text style={styles.textTitle}>vs {_.toUpper(game.player2.firstName)} {_.toUpper(game.player2.lastName)}</Text>
          </View>
          <View style={styles.socialWrap}>
            <SocialBar
              upvotes={data.upvoteCount}
              comments={data.commentCount}
              views={data.playCount}
              voted={data.voted}
              disabled={disabled}/>
          </View>
        </View>
        <View style={styles.border2}>
          <Text style={styles.textDesc} ellipsizeMode='tail' numberOfLines={2}>{data.title}</Text>
        </View>
      </View>
    </View>
  </TouchableHighlight>
)


const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    marginBottom: 5
  },
  titleWrap: {
    flex: 4
  },
  socialWrap: {
    flex: 1,
    justifyContent: 'center'
  },
  wrap: {
    justifyContent: 'space-around',
    alignItems: 'stretch',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  content: {
    padding: 20,
    justifyContent: 'space-around',
    alignItems: 'stretch'
  },
  textTitle: {
    fontSize: 15,
    fontWeight: '600',
    color: colours.greyscale.dark
  },
  textDesc: {
    fontSize: 12,
    fontWeight: '300',
    color: colours.greyscale.dark
  },
  videoLength: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    backgroundColor: 'black',
    opacity: 0.5,
    width: 40,
    height: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoLengthText: {
    color: 'white',
    fontSize: 11
  }

})


// <Text></Text>
// <Image source={game.source.thumb.small} />
