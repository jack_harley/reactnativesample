import { connect } from 'react-redux'
// import layout
import UploaderView from './uploader.view'
// import action
import { uploadFinished, uploadFailed } from './uploader.actions'

function mapStateToProps(store) {
  return {
    state: store.uploaderReducer,
  }
}

export default connect(
  mapStateToProps,
  {
    uploadFinished: () => uploadFinished(),
    uploadFailed: () => uploadFailed(),
  }
)(UploaderView)
