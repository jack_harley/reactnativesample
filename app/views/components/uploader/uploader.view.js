import _omit from 'lodash/omit'
import _drop from 'lodash/drop'
import _map from 'lodash/map'
import React, { Component } from 'react'
import Slider from 'react-native-slider'
import Orientation from 'react-native-orientation'
import { post } from '../../../services/api/fetch.js'
import config from '../../../config/config'
import { colours } from '../../../styles'
import RNFetchBlob from 'react-native-fetch-blob'

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

export default class Uploader extends Component {

  // props
  // bucketName,
  // files [{bucket: 'bucketName', filename: 'fileName', filepath: 'filepath', filetype: 'type'}]

  constructor(props) {
    console.log('hu');
    super(props)

    this.camera = null

    this.state = {
      file: {},
      noOfFiles: 0,
      filesUploaded: 0,
      value: 0
    }

    this.doUpload = this.doUpload.bind(this)
    this.getUploadUrl = this.getUploadUrl.bind(this)
  }


  componentDidMount() {
    console.log('componentDidMount');
    Orientation.lockToPortrait()
    this.getUploadUrl()
  }


  setupUploads(file) {
    console.log('setup');
    this.setState({file: file})
    this.doUpload(file)
  }

  doUpload(file) {
    const dirs = RNFetchBlob.fs.dirs
    let dir = '/'

    if(config.env == 'android') dir = RNFetchBlob.fs.dirs.DCIMDir
    if(config.env == 'ios') dir = RNFetchBlob.fs.dirs.DocumentDir

    console.log('dir', dir + '/' + file.filepath)

    RNFetchBlob.fs.exists(dir + '/' + file.filepath)
      .then((res) => {
        RNFetchBlob.fetch('PUT', file.signedUrl, {
          'key': file.filename,
          'acl': 'public-read',
          'Content-Type' : file.fileType,
        }, RNFetchBlob.wrap(dir + '/' + file.filepath))
        .uploadProgress((written, total) => {
          RNFetchBlob.fs.unlink(dir + '/' + file.filepath)
            .then(() => {
              this.setState({value: (written / total) })
            })
            .catch((err) => {
              console.log(err);
            })
        })
        .then((res) => {
          console.log('uploadFinished');
          this.props.uploadFinished()
          this.props.state.callback(file.fileUrl, file.thumbUrl)
        })
        .catch((err) => {
          console.log(err);
          // error handling ..
        })
      }, (err) => { console.log(err) })


  }

  getUploadUrl() {
    console.log('getUploadUrl');

    const payload = this.props.state.file

    post('/api/upload/presignedurl', payload)
      .then((url) => {
        this.setState({file: url})
        this.setupUploads(url)
      }, (err) => {console.log(err)})
  }

  render() {
    console.log('rendering');
    return <View style={styles.wrap}>
        <View style={styles.innerWrap}>
          <View style={styles.progressInfo}>
            <Text style={styles.largeText}>Sending...</Text>
          </View>
          <Slider
            {...this.state}
            trackStyle={progressBar.track}
            disabled={true}
            thumbStyle={progressBar.thumb}
            style={progressBar.bar}
            minimumTrackTintColor={colours.scheme.primary} />
        </View>
      </View>
  }

}

const progressBar = StyleSheet.create({
  bar: {
    height: 3
  },
  track: {
    height: 3,
    borderRadius: 0,
    backgroundColor: colours.background.medium,
    borderTopColor: colours.breakLines.dark,
    borderBottomColor: colours.breakLines.dark,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  thumb: {
    width: 0,
    height: 0,
  }
});

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    backgroundColor: colours.background.dark,
    justifyContent: 'center',
    alignItems: 'center'
  },
  largeText: {
    color: 'white',
    fontSize: 24
  },
  innerWrap: {
    width: 200
  },
  progressInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20
  }
});
