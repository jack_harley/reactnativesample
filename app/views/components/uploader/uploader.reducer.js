import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  file: {},
  visible: false,
  failed: false,
  gameId: null,
}

function duelState (state = initialState, action) {
  switch (action.type) {
    case 'UPLOAD_START':
      return {
        ...state,
        visible: true,
        file: action.file,
        gameId: action.gameId,
        callback: action.cb
      }
    case 'UPLOAD_FINISHED': {
      return initialState
    }
    case 'UPLOAD_FAILED': {
      return {
        ...state,
        failed: true
      }
    }
    case 'UPLOAD_FAIL_DISMISS': {
      return initialState
    }
    default:
      return state
  }
}

export default duelState
