export function doUpload(file, gameId, cb) {
  return {
    type: 'UPLOAD_START',
    gameId,
    file,
    cb
  }
}

export function uploadFinished() {
  return {
    type: 'UPLOAD_FINISHED'
  }
}

export function uploadFailed() {
  return {
    type: 'UPLOAD_FAILED'
  }
}

export function uploadFailedDismiss() {
  return {
    type: 'UPLOAD_FAIL_DISMISS'
  }
}
