import React from 'react'

import { Text, Image, View, StyleSheet, TouchableHighlight } from 'react-native'

export default ({source, name, onPress}) => (
  <TouchableHighlight onPress={onPress}>
    <View>
      <View>
        <Image source={source} style={styles.img}/>
      </View>
      <View>
        <Text>{name}</Text>
      </View>
    </View>
  </TouchableHighlight>
)

const styles = StyleSheet.create({
  img: {
    height: 40,
    width: 40,
    borderRadius: 20
  }
})
