import React, { Component } from 'react'
import { View, NavigationExperimental, TouchableOpacity, StyleSheet, Image, PanResponder } from 'react-native'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default class GalleryItem extends Component {

  componentWillMount() {
    this._panResponder = PanResponder.create({
      onPanResponderTerminate: (evt, gestureState) => {
      },
    });
  }

  render () {
    const route = {
      type: 'push',
      route: {
        key: this.props.route,
        item: this.props.data
      }
    }
    const game = JSON.parse(this.props.data.data);
    return (
      <TouchableOpacity
        onPress={() => this.props._handleNavigate(route)}
        style={[this.props.size]}>
        <Image source={game.source.thumb.small} style={this.props.size}/>
      </TouchableOpacity>
    )
  }

}
