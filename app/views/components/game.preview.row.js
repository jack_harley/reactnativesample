const _ = require('lodash')
import React, { Component } from 'react'
import { View, TouchableOpacity, StyleSheet, Image, Text } from 'react-native'

import { OpenSans } from '../../views/components'

import { colours } from '../../styles'
import SocialBar from './socialBar'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default ({route, size, _handleNavigate, data, game, vote, connectivity, disabled}) => (
  <TouchableOpacity onPress={() => _handleNavigate(route)}>
    <View style={styles.wrap}>
      <View style={size}>
        <Image style={size} source={game.source.thumb.small}>
          <View style={styles.videoLength}><Text style={styles.videoLengthText}>1:53</Text></View>
        </Image>
      </View>
      <View style={[size, styles.content]}>
        <View style={styles.border2}>
          <OpenSans textStyle={styles.textTitle}>{_.toUpper(game.player1.firstName)} {_.toUpper(game.player1.lastName)}</OpenSans>
          <OpenSans textStyle={styles.textTitle}>vs {_.toUpper(game.player2.firstName)} {_.toUpper(game.player2.lastName)}</OpenSans>
        </View>
        <View>
          <OpenSans textStyle={styles.textDesc} ellipsizeMode='tail' numberOfLines={2}>{data.title}</OpenSans>
        </View>
        <SocialBar
          upvotes={data.upvoteCount}
          comments={data.commentCount}
          views={data.playCount}
          voted={data.voted}
          vote={() => {vote()}}
          connectivity={connectivity}
          disabled={disabled}/>
      </View>
    </View>
  </TouchableOpacity>
)


const styles = StyleSheet.create({
  wrap: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'stretch',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  content: {
    justifyContent: 'space-around',
    alignItems: 'stretch'
  },
  textTitle: {
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 14,
    color: colours.greyscale.dark
  },
  textDesc: {
    fontFamily: 'OpenSans',
    fontSize: 10,
    fontWeight: '300',
    color: colours.greyscale.medium
  },
  videoLength: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    backgroundColor: 'black',
    opacity: 0.5,
    width: 40,
    height: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  videoLengthText: {
    color: 'white',
    fontSize: 11
  }

})


// <Text></Text>
// <Image source={game.source.thumb.small} />
