import React, { Component } from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'

import { Timer } from '../../services/utils'

import { colours } from '../../styles'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default class Countdown extends Component {

  constructor(props) {
    super(props)
  }

  state = {
    hours: null,
    minutes: null,
    color: 'green'
  }

  componentWillMount() {
    this.timer = new Timer()
  }

  componentDidMount() {
    this.setCountdown()
    this.timer.reset(this.updateTimer.bind(this), 60000)
  }

  componentWillUnmount() {
    this.timer.stop()
  }

  setCountdown() {
    const now = new Date().getTime(),
          end = new Date(this.props.end).getTime(),
          duration = end - now

    let minutes = parseInt((duration/(1000*60))%60),
        hours = parseInt((duration/(1000*60*60))%24)

    hours = (hours < 10) ? "0" + hours : hours
    minutes = (minutes < 10) ? "0" + minutes : minutes

    if(hours < 6) this.setState({color: 'red'})
    this.setState({hours: hours})
    this.setState({minutes: minutes})
  }

  updateTimer() {
    this.setCountdown()
    this.timer.reset(this.updateTimer.bind(this), 10000)
  }


  render() {
    return <View style={styles.wrap}>
      <Text style={styles[this.state.color]}>{this.state.hours}<Text style={styles.small}>h</Text> {this.state.minutes}<Text style={styles.small}>m</Text></Text>
    </View>
  }

}

const styles = StyleSheet.create({
  wrap: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  small: {
    fontSize: 10
  },
  red: {
    fontFamily: 'opensans',
    color: colours.scheme.secondary,
    fontSize: 26,
    fontWeight: '100'
  },
  green: {
    fontFamily: 'opensans',
    color: colours.scheme.primary,
    fontSize: 26,
    fontWeight: '100'
  }
})
