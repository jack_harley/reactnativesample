import React from 'react'

import { View, Text, TextInput, StyleSheet } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'

export default ({currentInput, onInput, search, size, color, breakLines}) => (
  <View style={[styles.wrap, {borderTopColor: breakLines, borderBottomColor: breakLines}]}>
    { search && <View style={[styles.icon, {borderRightColor: breakLines}]}><Icon color={color} size={size} name="search"></Icon></View> }
    <View style={styles.inputWrap}>
      <TextInput
        style={[styles.input, {color: color}]}
        placeholder={search && "SEARCH"}
        placeholderTextColor={(breakLines || color)}
        underlineColorAndroid = { 'transparent' }
        onChangeText={(text) => onInput(text)}
        value={currentInput}
      />
    </View>
  </View>
)

const styles = StyleSheet.create({
  wrap: {
    flexDirection: 'row',
    position: 'relative',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  inputWrap: {
    flex: 1,
  },
  input: {
    height: 40,
  },
  icon: {
    width: 38,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
  }
})
