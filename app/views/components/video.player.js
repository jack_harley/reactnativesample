import _replace from 'lodash/replace'
import Video from 'react-native-video'
import React, { Component } from 'react'
import Slider from 'react-native-slider'
import Orientation from 'react-native-orientation'
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';


import Icon from 'react-native-vector-icons/FontAwesome'

import { Timer, formatTime } from '../../services/utils'

import { colours } from '../../styles'

class VideoSlider extends Component {
  static defaultProps = {
    value: 0,
  }

  state = {
    value: this.props.value,
  }

  render() {
    return (
      <Slider
        {...this.state}
        trackStyle={customStyles8.track}
        thumbStyle={customStyles8.thumb}
        minimumTrackTintColor='#31a4db'
        thumbTouchSize={{width: 50, height: 40}} />
    )
  }
}

export default class VideoPlayer extends Component {

  constructor(props) {
    super(props)
    this.init = this.init.bind(this)
    this.setTime = this.setTime.bind(this)
    this.toggleControls = this.toggleControls.bind(this)
    this.toggleFullscreen = this.toggleFullscreen.bind(this)
    this.videoError = this.videoError.bind(this)
    this.playPauseReplay = this.playPauseReplay.bind(this)
    this.onEnd = this.onEnd.bind(this)
    this.timer = new Timer()
  }

  _orientationDidChange(orientation) {
    if (orientation == 'LANDSCAPE') {
      Orientation.lockToLandscape()
      this.setState({isFullscreen: true})
    } else {
      Orientation.lockToPortrait()
      this.setState({isFullscreen: false})
    }
  }

  componentDidMount() {
    if(this.props.fullscreen) {
      Orientation.lockToLandscape()
      this.setState({isFullscreen: true})
    } else {
      Orientation.addOrientationListener(this._orientationDidChange.bind(this));
    }
  }

  componentWillUnmount() {
    this.timer.stop()
  }

  goBack() {
    Orientation.lockToPortrait()
    this.props._goBack()
  }

  static defaultProps = {
    value: 0,
  };

  state = {
    value: this.props.value,
    duration: 0,
    currentTime: '0:00',
    showControls: true,
    paused: this.props.autoplay ? false : true,
    isFullscreen: this.props.fullscreen ? true : false,
    loading: true,
    error: false
  };

  controlsCover() {

    if(this.state.error) {
      return (
        <View style={[styles.controlsCover, this.isFullscreen(this.state.isFullscreen)]}>
          {this.videoHeader()}
        </View>
      )
    }

    if(this.state.loading) {
      return (
        <View style={[styles.controlsCover, this.isFullscreen(this.state.isFullscreen)]}>
        {this.videoHeader()}
          <Text>Loading</Text>
        </View>
      )
    }

    if(!this.state.showControls) {
      return (
        <View style={[styles.controlsCover, this.isFullscreen(this.state.isFullscreen)]}>
          <TouchableOpacity onPress={this.toggleControls} style={styles.wrap}>
            <View>
              <Text>t</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }

    return  (
      <View style={[styles.controlsCover, this.isFullscreen(this.state.isFullscreen)]}>
        {this.videoHeader()}
        {this.videoPlayPauseReplay()}
        {this.videoToolbar()}
      </View>
    )
  }

  videoHeader() {
    if(!this.props._goBack) return
    return (
      <View style={styles.iconWrap}>
        <Icon.Button
          name="angle-left"
          onPress={() => this.goBack()}
          backgroundColor="transparent"
          style={{padding: 0, borderRadius: 0, width: 30, height: 30}}
          size={28}
          color='white'
          iconStyle={{marginLeft: 6, backgroundColor: 'transparent'}} >
        </Icon.Button>
      </View>
    )
  }

  playPauseReplayIcon() {
    let icon = 'pause'
    if(this.state.finished) icon = 'repeat'
    if(this.state.paused) icon = 'play'
    return icon
  }

  videoPlayPauseReplay() {
    if(this.state.loading) return <Text>loading...</Text>
    const icon = this.playPauseReplayIcon()
    return (
      <TouchableOpacity onPress={this.toggleControls} style={styles.playPauseWrap}>
        <View style={styles.playPauseButton} ref="playPause">
          <Icon.Button
            name={icon}
            onPress={() => this.playPauseReplay()}
            backgroundColor="transparent"
            style={{padding:3, borderRadius: 0, justifyContent: 'center'}}
            size={24}
            color='white'
            iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
          </Icon.Button>
        </View>
      </TouchableOpacity>
    )
  }

  videoToolbar() {
    return (
      <View style={styles.toolBarWrap}>
        <View style={styles.seekWrap}>
          <View style={styles.currentTime}><Text style={styles.textColourWhite}>{this.state.currentTime}</Text></View>
          <View style={styles.seekBar}>
            <Slider
              {...this.state}
              trackStyle={customStyles8.track}
              thumbStyle={customStyles8.thumb}
              minimumTrackTintColor='#31a4db'
              thumbTouchSize={{width: 50, height: 40}}
              onSlidingComplete={this.seek.bind(this)} />
          </View>
          <View style={styles.endTime}><Text style={styles.textColourWhite}>3.37</Text></View>
        </View>
        {
          this.props.enableFullscreenToggle &&
          <View style={styles.fullscreenWrap}>
            <Icon.Button
              name="arrows-alt"
              onPress={() => this.toggleFullscreen()}
              backgroundColor="transparent"
              style={{padding:3, borderRadius: 0, justifyContent: 'center'}}
              size={16}
              color='white'
              iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
            </Icon.Button>
          </View>
        }
      </View>
    )
  }

  render() {
    const source = _replace(this.props.source, 'file://', '')
    console.log('source', source);
    return (
      <View style={[styles.videoWrap, this.isFullscreen(this.state.isFullscreen)]}>
        {this.controlsCover()}
        <Video ref={(c) => this.player = c}
           source={{uri: source}}
           rate={1.0}
           volume={1.0}
           muted={true}
           paused={this.state.paused}
           repeat={false}
           resizeMode="cover"
           onLoadStart={this.loadStart} // Callback when video starts to load
           onLoad={this.init}    // Callback when video loads
           onProgress={this.setTime}    // Callback every ~250ms with currentTime
           onEnd={this.onEnd}           // Callback when playback finishes
           onError={this.videoError}    // Callback when video cannot be loaded
           style={styles.video} />
      </View>
    );
  }

  init(obj) {
    this.timer.reset(this.toggleControls, 4000)
    this.setState({duration: obj.duration, loading: false});
  }

  setTime(timeObj) {
    this.setState({value: timeObj.currentTime/this.state.duration, currentTime: formatTime(timeObj.currentTime)});
  }

  seek(value) {
    this.player.seek(value*this.state.duration);
  }

  playPauseReplay() {
    if(this.state.finished) {
      this.setState({finished: false})
      this.player.seek(0)
      return
    }
    if(!this.state.paused) {
      this.timer.stop()
    } else {
      this.timer.reset(this.toggleControls, 4000)
    }
    this.setState({paused: !this.state.paused})
  }

  replay() {
    this.setState({finished: false});
    this.player.seek(0);
  }

  toggleControls() {
    if(!this.state.showControls && !this.state.paused) this.timer.reset(this.toggleControls, 4000)
    this.setState({showControls: !this.state.showControls})
  }

  toggleFullscreen() {
    if(this.state.isFullscreen) {
      Orientation.lockToPortrait()
      this.setState({isFullscreen: false})
    } else {
      Orientation.lockToLandscape()
      this.setState({isFullscreen: true})
    }
  }

  videoError(err) {
    this.setState({error: true})
  }

  isFullscreen(isFullscreen) {
    if(isFullscreen) {
      return { bottom: 0 }
    } else {
      return this.props.videoHeight
    }
  }

  onEnd() {
    this.setState({finished: true})
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }


}

const customStyles8 = StyleSheet.create({
  track: {
    height: 2,
    backgroundColor: '#303030',
  },
  thumb: {
    width: 10,
    height: 10,
    backgroundColor: '#31a4db',
    borderRadius: 10 / 2,
  }
});

const styles = StyleSheet.create({
  wrap: {
    flex: 1
  },
  controlsCover: {
    position: 'absolute',
    zIndex: 4,
    top: 0,
    right: 0,
    left: 0,
  },
  headerWrap: {
    flex: 1
  },
  playPauseWrap: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  toolBarWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  playPauseButton: {
    width: 64,
    height: 40,
    backgroundColor: 'black',
    opacity: 0.6,
    borderRadius: 5
  },
  seekWrap: {
    flex: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fullscreenWrap: {
    flex: 1
  },
  currentTime: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  textColourWhite: {
    color: 'white'
  },
  endTime: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  seekBar: {
    flex: 5,
    height: 10,
    justifyContent: 'center',
    alignItems: 'stretch'
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  videoWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'black',
    zIndex: 1
  },
  video: {
    flex: 1
  },
  iconWrap: {
    zIndex: 9,
    justifyContent: 'flex-start',
    position: 'absolute',
    left: 0,
    top: 0,
    height: 30,
    width: 30,
  }
});
