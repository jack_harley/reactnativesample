import React, { Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'

import IconFA from 'react-native-vector-icons/FontAwesome'

import { colours } from '../../styles'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default ({thumb, fullscreenVideo}) => (
  <Image style={styles.image} source={thumb} >
    <View style={styles.cover}></View>
    <View style={styles.innerWrap}>
      <View style={styles.playButton}>
        <IconFA.Button
          name='play'
          onPress={ fullscreenVideo }
          backgroundColor="transparent"
          style={{padding:3, borderRadius: 0, justifyContent: 'center'}}
          size={24}
          color={colours.greyscale.light}
          iconStyle={{marginLeft: 12, backgroundColor: 'transparent'}} >
        </IconFA.Button>
      </View>
    </View>
  </Image>
)


const styles = StyleSheet.create({
  image: {
    position: 'relative',
    resizeMode: 'cover',
    flex: 1
  },
  cover: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: colours.background.light
  },
  innerWrap: {
    position: 'relative',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  playButton: {
    width: 60,
    height: 60,
    backgroundColor: colours.background.dark,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center'
  }
})


// <Text></Text>
// <Image source={game.source.thumb.small} />
