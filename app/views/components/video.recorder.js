import React from 'react';
import {
  Image,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import Camera from 'react-native-camera';

import Icon from 'react-native-vector-icons/FontAwesome'

import Orientation from 'react-native-orientation'

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.camera = null;

    this.state = {
      camera: {
        aspect: Camera.constants.Aspect.fill,
        captureTarget: Camera.constants.CaptureTarget.cameraRoll,
        type: props.selfieCam ? Camera.constants.Type.front : Camera.constants.Type.back,
        orientation: Camera.constants.Orientation.landscapeLeft,
        flashMode: Camera.constants.FlashMode.auto,
      },
      isRecording: false
    };

    this.startRecording = this.startRecording.bind(this);
    this.stopRecording = this.stopRecording.bind(this);
    this.switchType = this.switchType.bind(this);
    this.switchFlash = this.switchFlash.bind(this);
  }

  componentDidMount() {
    Orientation.lockToLandscape()
  }

  componentWillUnmount() {
    Orientation.lockToPortrait()
  }

  startRecording() {
    if (this.camera) {
      this.camera.capture({mode: Camera.constants.CaptureMode.video})
          .then((data) => {
            this.props.onStop({
              source: {
                thumb: {
                  large:{
                    uri:"https://placeholdit.imgix.net/~text?txtsize=15&txt=image97&w=80&h=80"
                  },
                  small:{
                    uri:"https://placeholdit.imgix.net/~text?txtsize=15&txt=image97&w=20&h=20"
                  }
                },
                uri: data.path
              }
            })
          })
          .catch(err => console.error(err));
      this.setState({
        isRecording: true
      });
    }
  }

  stopRecording() {
    if (this.camera) {
      this.camera.stopCapture();
      this.setState({
        isRecording: false
      });
    }
  }

  switchType() {
    let newType;
    const { back, front } = Camera.constants.Type;

    if (this.state.camera.type === back) {
      newType = front;
    } else if (this.state.camera.type === front) {
      newType = back;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        type: newType,
      },
    });
  }

  get typeIcon() {
    let icon;
    const { back, front } = Camera.constants.Type;

    if (this.state.camera.type === back) {
      icon = require('./assets/ic_camera_rear_white.png');
    } else if (this.state.camera.type === front) {
      icon = require('./assets/ic_camera_front_white.png');
    }

    return icon;
  }

  switchFlash() {
    let newFlashMode;
    const { auto, on, off } = Camera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      newFlashMode = on;
    } else if (this.state.camera.flashMode === on) {
      newFlashMode = off;
    } else if (this.state.camera.flashMode === off) {
      newFlashMode = auto;
    }

    this.setState({
      camera: {
        ...this.state.camera,
        flashMode: newFlashMode,
      },
    });
  }

  get flashIcon() {
    let icon;
    const { auto, on, off } = Camera.constants.FlashMode;

    if (this.state.camera.flashMode === auto) {
      icon = require('./assets/ic_flash_auto_white.png');
    } else if (this.state.camera.flashMode === on) {
      icon = require('./assets/ic_flash_on_white.png');
    } else if (this.state.camera.flashMode === off) {
      icon = require('./assets/ic_flash_off_white.png');
    }

    return icon;
  }

  render() {
    return (
      <View style={[styles.container, {backgroundColor: 'white'}]}>
        <StatusBar
          animated
          hidden
        />
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          captureQuality={Camera.constants.CaptureQuality.medium}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          defaultTouchToFocus
          mirrorImage={false}
        />
        <View style={styles.topOverlay}>
          <TouchableOpacity onPress={this.props.goBack}>
            <Icon name="angle-left" color="white" backgroundColor="transparent" size={24}></Icon>
          </TouchableOpacity>
        </View>
        <View style={styles.recordOverlay}>
          <View style={styles.buttonsSpace} >
            {
                !this.state.isRecording
                &&
                <TouchableOpacity
                    style={styles.captureButton}
                    onPress={this.startRecording}
                >
                  <Image
                      source={require('./assets/ic_videocam_36pt.png')}
                  />
                </TouchableOpacity>
                ||
                <TouchableOpacity
                    style={styles.captureButton}
                    onPress={this.stopRecording}
                >
                  <Image
                      source={require('./assets/ic_stop_36pt.png')}
                  />
                </TouchableOpacity>
            }
          </View>
        </View>
        <View style={styles.overlay, styles.rightOverlay}>
          <View style={styles.optionSpace}>
            <TouchableOpacity
              style={styles.typeButton}
              onPress={this.switchType}
            >
              <Image
                source={this.typeIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.flashButton}
              onPress={this.switchFlash}
            >
              <Image
                source={this.flashIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 9999999999999999,
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  recordOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
    width: 60,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightOverlay: {
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    width: 60,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  optionSpace: {
    padding: 10,
    position: 'absolute',
    right: 0,
    top: 0
  },
  buttonsSpace: {
  }
});
