import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'

import { colours } from '../../styles'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
export default ({record}) => (
  <TouchableOpacity onPress={ record } style={styles.outer}>
    <View style={styles.inner}></View>
  </TouchableOpacity>
)


const styles = StyleSheet.create({
  outer: {
    width: 100,
    height: 100,
    borderRadius: 50,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.background.medium,
    borderWidth: 1,
    borderColor: colours.breakLines.dark
  },
  inner: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: colours.scheme.secondary
  }
})
