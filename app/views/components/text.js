import React, { Component } from 'react'

import { Text, View, StyleSheet } from 'react-native'

export default class OpenSans extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
        <Text style={[styles.family, this.props.textStyle]}>{this.props.children}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  family: {
    fontFamily: 'OpenSans'
  }
})
