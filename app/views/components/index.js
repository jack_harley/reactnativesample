import LoadingView from './spinner'
import Button from './button'
import GamePreviewGrid from './game.preview.grid'
import GamePreviewFeed from './game.preview.feed'
import GamePreviewRow from './game.preview.row'
import SocialBar from './socialBar'
import UserListItem from './list.user.item'
import VideoPlayer from './video.player'
import TextInput from './textInput'
import VideoRecorder from './video.recorder'
import Uploader from './uploader/uploader.container'
import OpenSans from './text'
import VideoLink from './video.link'
import Countdown from './countdown'
import RecordButton from './record.button'

// export { LoadingView }
// export { Button }
// export { GamePreviewGrid }
// export { GamePreviewFeed }
// export { GamePreviewRow }
// export { SocialBar }
// export { UserListItem }
// export { VideoPlayer }
// export { TextInput }
// export { VideoRecorder }
// export { Uploader }

export {
  LoadingView,
  Button,
  GamePreviewGrid,
  GamePreviewFeed,
  GamePreviewRow,
  SocialBar,
  UserListItem,
  VideoPlayer,
  TextInput,
  VideoRecorder,
  Uploader,
  OpenSans,
  VideoLink,
  Countdown,
  RecordButton
}
