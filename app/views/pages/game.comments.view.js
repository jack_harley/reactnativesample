import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'

import { Button } from '../components'

const GameCommentView = ({_goBack}) =>  (
  <View>
    <Text>About</Text>
    <Button onPress={_goBack} label='Go Back' />
  </View>
)

export default GameCommentView
