const config = {}

config.development = {
  // baseUrl: 'http://192.168.1.101:3000',
  baseUrl: 'http://oneupserver.95epppw8qs.eu-west-1.elasticbeanstalk.com/',
  // baseUrl: 'http://localhost:3000',
  env: 'android',
  app: {
    name: 'MyApp'
  },
  aws: {
    buckets: {
      singleSrc: 'single-video-attempts-src',
      singleDest: 'single-video-attempts',
      gallery: 'gallery-final'
    }
  }
}

config.production = {
  baseUrl: 'http://localhost:3000'
}

module.exports = config[process.env.NODE_ENV]
