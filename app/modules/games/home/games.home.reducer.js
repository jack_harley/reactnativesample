import _filter from 'lodash/filter'
import _map from 'lodash/map'

import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  currentView: 'home',
  newGame: null,
  gameView: null,
  loggedIn: false,
  user: null,
  games: [],
  games_fetching: false,
  games_fetched: false,
  games_error: false,
  updating: false,
  updating_error: false,
  showingVideo: false,
  showingVideoUri: null
}

function gamesItemsState (state = initialState, action) {
  switch (action.type) {
    case 'SET_INITIAL_STATE':
      if(!action.state) return state
      return {
        ...state,
        loggedIn: (action.state.user && action.state.user._id) ? true : false,
        user: action.state.user ? action.state.user : null
      }
    case 'GET_GAMES_PENDING': {
      return {
        ...state,
        games_fetching: true,
      }
    }
    case 'GET_GAMES_REJECTED': {
      return {
        ...state,
        games_fetching: false,
        games_error: false,
      }
    }
    case 'GET_GAMES_FULFILLED': {
      return {
        ...state,
        games_fetching: false,
        games_fetched: true,
        games: action.payload
      }
    }
    case 'TIMEDDUEL_GAME_UPDATE': {
      return {
        ...state,
        games: _map(state.games, (game) => {
          console.log(action);
          console.log('game.gameId == action.route.game.server.gameId', game.gameId == action.game.server.gameId);
          console.log('action.route.game', action.game);
          if(game.gameId == action.game.server.gameId) return action.game
          else return game
        })
      }
    }
    case 'START_NEW_GAME':
      return {
        ...state,
        newGame: action.game
      }
    case 'CHANGE_CURRENT_VIEW':
      return {
        ...state,
        currentView: action.view,
        newGame: null
      }
    case 'GO_TO_GAME':
      return {
        ...state,
        gameView: action.gameType,
        game: action.game
      }
    case 'SHOW_GAME_INFO':
      return {
        ...state,
        gameInfo: action.game
      }
    case 'HIDE_GAME_INFO':
      return {
        ...state,
        gameInfo: null
      }
    case 'SEND_CHALLENGE_LATER_FULFILLED':
      return {
        ...state,
        currentView: 'home',
        newGame: null
      }
    case 'SEND_CHALLENGE_FULFILLED':
      return {
        ...state,
        currentView: 'home',
        newGame: null,
        games: [...state.games, {server: action.payload, local: undefined}]
      }
    case 'ACCEPT_CHALLENGE_FULFILLED':
      return {
        ...state,
        currentView: 'home',
        newGame: null,
        games: [...state.games, action.payload]
      }
    // case 'UPLOAD_FILE_START':
    //   return {
    //     ...state,
    //     uploading: true
    //   }
    // case 'UPLOAD_FILE_STOP':
    //   return {
    //     ...state,
    //     uploading: false
    //   }
    case 'UPDATE_GAME_PENDING':
      return {
        ...state,
        updating: true
      }
    case 'UPDATE_GAME_ERROR':
      return {
        ...state,
        updating: false,
        updating_error: true
      }
    case 'UPDATE_GAME_FULFILLED':
      return {
        ...state,
        updating: false,
        game: action.payload
      }
    case 'GO_HOME':
      return {
        ...state,
        currentView: 'home',
        gameView: null,
        newGame: null
      }
    case 'NEW_TIMED_DUEL':
      return {
        ...state,
        currentView: 'create'
      }
    case 'SHOW_FULLSCREEN_VIDEO':
      return {
        ...state,
        showingVideo: true,
        showingVideoUri: action.uri
      }
    case 'HIDE_FULLSCREEN_VIDEO':
      return {
        ...state,
        showingVideo: false,
        showingVideoUri: null
      }
    case 'TIMEDDUEL_REMOVE_FULFILLED':
      return {
        ...state,
        games: _filter(state.games, (o) => { return o.server.gameId != action.payload.gameId })
      }
    default:
      return state
  }
}

export default gamesItemsState
