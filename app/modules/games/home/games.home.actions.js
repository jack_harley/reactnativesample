import { Game } from '../../../services/api'

export function getGames(userId) {
  return {
    type: 'GET_GAMES',
    payload: Game.get(userId)
  }
}

export function goToGame(game, gameType) {
  return {
    type: 'GO_TO_GAME',
    game,
    gameType
  }
}

export function changeCurrentView(view) {
  return {
    type: 'CHANGE_CURRENT_VIEW',
    view
  }
}

export function selectGame(game) {
  return {
    type: 'START_NEW_GAME',
    game
  }
}

export function showGameInfo(game) {
  return {
    type: 'SHOW_GAME_INFO',
    game
  }
}

export function hideGameInfo() {
  return {
    type: 'HIDE_GAME_INFO'
  }
}

export function goHome() {
  return {
    type: 'GO_HOME'
  }
}

export function fullscreenVideo(show, uri) {
  return {
    type: show ? 'SHOW_FULLSCREEN_VIDEO' : 'HIDE_FULLSCREEN_VIDEO',
    uri
  }
}
