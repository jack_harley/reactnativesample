import { User, Game } from '../../../../../services/api'

export function getChallenges(userId) {
  return {
    type: 'FETCH_CHALLENGES',
    payload: User.challenges(userId)
  }
}

export function deleteUnsent(gameId) {
  return {
    type: 'DELETE_UNSENT',
    payload: Game.deleteStoredChallenge(gameId)
  }
}

export function showSendChallengeView(challenge) {
  return {
    type: 'UNSENT_CHALLENGE_VIEW_SHOW',
    challenge
  }
}

export function hideSendChallengeView() {
  return {
    type: 'UNSENT_CHALLENGE_VIEW_HIDE'
  }
}

export function sendChallenge(userId, challenge) {
  return {
    type: 'SEND_UNSENT_CHALLENGE',
    payload: Game.sendChallenge(userId, challenge)
  }
}

export function acceptChallenge(gameId, userId, opponentId, state) {

  const HOUR = 60*60*1000
  const dateNow = new Date().toISOString()
  let date24Hours = new Date()
  date24Hours.setTime(date24Hours.getTime() + (24*HOUR))
  state.timeouts[userId] = date24Hours.toISOString()
  state.accepted = true

  return {
    type: 'ACCEPT_CHALLENGE',
    payload: Game.acceptChallenge(gameId, userId, opponentId, state)
  }
}

export function rejectChallenge(gameId, userId, opponentId) {
  return {
    type: 'REJECT_CHALLENGE',
    payload: Game.rejectChallenge(gameId, userId, opponentId)
  }
}
