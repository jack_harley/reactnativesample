import { connect } from 'react-redux'
import ChallengeItems from './challenges.view'
import {
  getChallenges,
  deleteUnsent,
  sendChallenge,
  showSendChallengeView,
  hideSendChallengeView,
  acceptChallenge,
  rejectChallenge
} from './challenges.actions'

function mapStateToProps (state) {
  return {
    state: state.challengesReducer
  }
}

export default connect(
  mapStateToProps,
  {
    getChallenges: (userId) => getChallenges(userId),
    deleteUnsent: (gameId) => deleteUnsent(gameId),
    sendChallenge: (userId, challenge) => sendChallenge(userId, challenge),
    showSendChallengeView: (challenge) => showSendChallengeView(challenge),
    hideSendChallengeView: () => hideSendChallengeView(),
    acceptChallenge: (gameId, userId, opponentId, state) => acceptChallenge(gameId, userId, opponentId, state),
    rejectChallenge: (gameId, userId, opponentId) => rejectChallenge(gameId, userId, opponentId)
  }
)(ChallengeItems)
