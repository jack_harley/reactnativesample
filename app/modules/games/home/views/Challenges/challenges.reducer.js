import _remove from 'lodash/remove'
import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  key: 'challenges.items',
  items: [],
  unsent: [],
  fetching: false,
  fetchData: false,
  error: false,
  unsent_deleting_error: false,
  unsent_challenge_view: false,
  unsent_challenge_sending: false,
  unsent_challenge_error: false,
  unsent_challenge_sent: false,
  unsent_challenge: null
}

function galleryItemsState (state = initialState, action) {
  switch (action.type) {
    case 'FETCH_CHALLENGES_PENDING':
      return {
        ...state,
        fetching: true,
        fetchData: false,
        items: []
      }
    case 'FETCH_CHALLENGES_FULFILLED':
      return {
        ...state,
        fetching: false,
        items: action.payload.received,
        unsent: action.payload.unsent
      }
    case 'FETCH_CHALLENGES_REJECTED':
      return {
        fetching: false,
        error: true
      }
    case 'DELETE_UNSENT_PENDING':
      return {
        ...state,
        fetching: true,
      }
    case 'DELETE_UNSENT_FULFILLED':
      return {
        ...state,
        fetching: false,
        unsent: action.payload
      }
    case 'DELETE_UNSENT_REJECTED':
      return {
        unsent_deleting_error: true,
        fetching: false,
      }
    case 'UNSENT_CHALLENGE_VIEW_SHOW':
      return {
        ...state,
        unsent_challenge_view: true,
        unsent_challenge: action.challenge
      }
    case 'UNSENT_CHALLENGE_VIEW_HIDE':
      return {
        ...state,
        unsent_challenge_view: false,
        unsent_challenge: null
      }
    case 'SEND_UNSENT_CHALLENGE_PENDING':
      return {
        ...state,
        unsent_challenge_sending: true
      }
    case 'SEND_UNSENT_CHALLENGE_REJECTED':
      return {
        ...state,
        unsent_challenge_sending: false,
        unsent_challenge_error: true
      }
    case 'SEND_UNSENT_CHALLENGE_FULFILLED':
      return {
        ...state,
        unsent_challenge_sending: false,
        unsent_challenge_sent: true
      }
    case 'ACCEPT_CHALLENGE_PENDING':
      return {
        ...state,
        fetching: true
      }
    case 'ACCEPT_CHALLENGE_ERROR':
      return {
        ...state,
        fetching: false
      }
    case 'ACCEPT_CHALLENGE_FULFILLED':
      return {
        ...state,
        fetching: false
      }
    case 'REJECT_CHALLENGE_PENDING':
      return {
        ...state,
        fetching: true
      }
    case 'REJECT_CHALLENGE_ERROR':
      return {
        ...state,
        fetching: false
      }
    case 'REJECT_CHALLENGE_FULFILLED':
      return {
        ...state,
        fetching: false,
        items: _remove(state.items, (o) => { o.gameId == action.payload })
      }
    default:
      return state
  }
}

export default galleryItemsState
