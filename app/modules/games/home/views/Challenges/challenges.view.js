import _map from 'lodash/map';
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableHighlight, StatusBar, Text, Image, Dimensions } from 'react-native'

import { LoadingView, OpenSans } from '../../../../../views/components'

import { layout, colours } from '../../../../../styles'

import { galleryIcon, rowIcon } from '../../../../../styles/icons'

import Icon from 'react-native-vector-icons/Ionicons'
import IconFA from 'react-native-vector-icons/FontAwesome'


const screenWidth = Dimensions.get('window').width

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class ChallengesView extends Component {

  componentWillMount() {
    const { fetching } = this.props.state
    const { user } = this.props
    if(!fetching) this.props.getChallenges(user._id)
  }

  acceptChallenge(item) {
    this.props.acceptChallenge(item.gameId, this.props.user._id, item.opponent._id, item.state)
      .then((res) => {
        this.props._goToGame('timedDuel', res.action.payload)
      })
  }

  rejectChallenge(item) {
    this.props.rejectChallenge(item.gameId, this.props.user._id, item.opponent._id)
  }

  _renderDuel(item) {
    return <View style={[styles.contentContainer]}>
        <View style={[styles.contentTop]}>
          <View style={[styles.contentLeft]}><Image style={styles.opponentImg} source={item.opponent.avatar.small}/></View>
          <View style={[styles.contentRight]}>
            <OpenSans textStyle={[styles.challengeText, styles.whiteText]}>{item.opponent.firstName} {item.opponent.lastName} has challenged you to a DUEL!</OpenSans>
          </View>
        </View>
        <View style={[styles.contentBottom]}>
          <View style={[styles.contentLeft]}>
            <OpenSans textStyle={styles.whiteText}>Challenge</OpenSans>
            <OpenSans textStyle={styles.whiteText}>Stake</OpenSans>
          </View>
          <View style={[styles.contentRight]}>
            <OpenSans textStyle={styles.whiteText}>{ item.game.challenge }</OpenSans>
            <OpenSans textStyle={styles.whiteText}>{ item.game.stake }</OpenSans>
          </View>
        </View>
      </View>
  }

  _renderTimerDuel(item) {
    const { source } = item.game.challenge.video
    return <View style={[styles.contentContainer, {marginBottom: 5}]}>
      <Image style={[ styles.halfHeight, styles.threeQuarterWidth, {resizeMode: 'cover'} ]} source={source.thumb.large} >
        <View style={{position: 'relative', flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Image style={[styles.threeQuarterWidth, styles.quarterHeight, {position: 'absolute', top: 0, resizeMode: 'cover'}]} source={ require('../../../assets/dark_grad_top.png') }>
            <Text style={[styles.whiteText, {color: colours.background.light, textAlign: 'center', padding: 5, fontSize: 12}]}>{item.opponent.firstName} {item.opponent.lastName} has challenged you to a Duel.</Text>
          </Image>
          <View style={styles.playButton}>
            <IconFA.Button
              name='play'
              onPress={() => { this.props.fullscreenVideo(true, source.uri) }}
              backgroundColor="transparent"
              style={{padding:3, borderRadius: 0, justifyContent: 'center'}}
              size={24}
              color='white'
              iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
            </IconFA.Button>
          </View>
        </View>
      </Image>
    </View>
  }

  _renderChallenges(challenges) {
    return _map(challenges, (item) => {
      let gameView = null
      switch (item.game.type) {
        case 'timedDuel':
          gameView = this._renderTimerDuel(item)
          break
        case 'duel':
          gameView = this._renderDuel(item)
          break
        default:
          break
      }
      return <View style={[styles.itemContainer, styles.halfHeight]} key={item.gameId}>
          {gameView}
        <View style={[styles.actionsContainer]}>
          <TouchableHighlight onPress={() => this.rejectChallenge(item)} style={[styles.button, styles.buttonTop]}>
            <View>
              <Icon name="ios-close" color={colours.background.dark} size={48} style={{textAlign: 'center'}}></Icon>
              <OpenSans textStyle={[{fontSize: 9}]}>REJECT</OpenSans>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => this.acceptChallenge(item)} style={[styles.button, styles.buttonBottom]}>
            <View>
              <Icon name="ios-checkmark" color={colours.background.dark} size={48} style={{textAlign: 'center'}}></Icon>
              <OpenSans textStyle={[{fontSize: 9}]}>ACCEPT</OpenSans>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    })
  }

  _renderUnsent(challenges) {
    return _map(challenges, (item) => {
      return <View style={[styles.itemContainer, styles.halfHeight]} key={item.gameId}>
        <View style={[styles.contentContainer]}>
          <View style={[styles.contentTop]}>
            <View style={[styles.contentLeft]}><Image style={styles.opponentImg} source={item.opponent.avatar.small}/></View>
            <View style={[styles.contentRight]}><OpenSans textStyle={[styles.challengeText, styles.whiteText]}>Send your challenge to {item.opponent.firstName} {item.opponent.lastName}</OpenSans></View>
          </View>
          <View style={[styles.contentBottom]}>
            <View style={[styles.contentLeft]}>
              <OpenSans textStyle={styles.whiteText}>{item.data.game.type}</OpenSans>
              <OpenSans textStyle={styles.whiteText}>Stake</OpenSans>
            </View>
            <View style={[styles.contentRight]}>
              <OpenSans textStyle={styles.whiteText}>{ item.data.game.challenge }</OpenSans>
              <OpenSans textStyle={styles.whiteText}>{ item.data.game.stake }</OpenSans>
            </View>
          </View>
        </View>
        <View style={styles.actionsContainer}>
          <TouchableHighlight onPress={() => this.props.deleteUnsent(item.gameId)} style={[styles.button, styles.buttonTop]}>
            <View>
              <OpenSans textStyle={styles.whiteText}>DELETE</OpenSans>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => this.props.sendChallenge(this.props.user._id, item.gameId)} style={[styles.button, styles.buttonBottom]}>
            <View>
              <OpenSans textStyle={styles.whiteText}>SEND</OpenSans>
            </View>
          </TouchableHighlight>
        </View>
      </View>
    })
  }

  render () {

    const { fetching, items, unsent } = this.props.state

    if( fetching ) return <LoadingView />

    return <View>
      {this._renderChallenges( items )}
      { unsent
      && <View style={styles.unsentTitle}><OpenSans>Unsent Challenges</OpenSans></View>
      && this._renderUnsent( unsent )}
    </View>
  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }
  background(color) {
    return {
      backgroundColor: color
    }
  }

}


const styles = StyleSheet.create({
  itemContainer: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark,
    flexDirection: 'row',
  },
  itemBtnContainer: {
    flex: 1
  },
  contentContainer: {
    alignItems: 'center',
    flex: 3
  },
  unsentTitle: {
    backgroundColor: colours.breakLines.dark,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionsContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  contentTop: {
    flex: 6,
    flexDirection: 'row',
    alignItems: 'center'
  },
  contentBottom: {
    flex: 4,
    flexDirection: 'row',
    alignItems: 'center'
  },
  contentLeft: {
    alignItems: 'center',
    flex: 3
  },
  contentRight: {
    alignItems: 'center',
    flex: 6,
  },
  whiteText: {
    color: colours.greyscale.medium
  },
  challengeText: {
    lineHeight: 26,
    fontSize: 18,
  },
  buttonTop: {
    backgroundColor: colours.scheme.secondary,
    borderLeftWidth: 1,
    borderLeftColor: colours.breakLines.dark
  },
  buttonBottom: {
    backgroundColor: colours.scheme.primary,
    borderLeftWidth: 1,
    borderLeftColor: colours.breakLines.dark,
    borderTopWidth: 1,
    borderTopColor: colours.breakLines.dark,
  },
  breakLineRight: {
    borderRightWidth: 1,
    borderRightColor: colours.breakLines.dark
  },
  halfHeight: {
    height: screenWidth/2
  },
  halfWidth: {
    width: screenWidth/2
  },
  quarterHeight: {
    height: screenWidth/4
  },
  threeQuarterWidth: {
    width: screenWidth*3/4
  },
  opponentImg: {
    borderWidth: 1,
    borderColor: colours.breakLines.dark,
    height: screenWidth/6,
    width: screenWidth/6,
    borderRadius: screenWidth/12
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  playButton: {
    width: 58,
    height: 34,
    backgroundColor: 'black',
    opacity: 0.6,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
});
