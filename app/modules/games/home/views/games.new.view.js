import _map from 'lodash/map';

import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableHighlight, StatusBar, Text, Image, Dimensions } from 'react-native'

import { layout, colours } from '../../../../styles'

const screenWidth = Dimensions.get('window').width

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class NewGamesView extends Component {

  newDuel() {
    this.props.selectGame('duel');
  }

  showDuelInfo() {
    this.props.showGameInfo('duel')
  }

  hideGameInfo() {
    this.props.hideGameInfo()
  }

  render() {
    const { gameInfo } = this.props

    switch (gameInfo) {
      case 'duel':
        return <View>
          <TouchableHighlight onPress={this.hideGameInfo.bind(this)}><View><Text>Back</Text></View></TouchableHighlight>
          <Text>Info</Text>
        </View>
      default:

    }

    return <View style={styles.container}>
      <View style={styles.row}>
        <TouchableHighlight onPress={this.newDuel.bind(this)} style={[styles.mainButton, styles.halfHeight, styles.halfWidth, styles.breakLineRight]}>
          <View style={styles.buttonInnerWrap}>
            <TouchableHighlight onPress={this.showDuelInfo.bind(this)} style={styles.infoButton}><View><Text>i</Text></View></TouchableHighlight>
            <Text style={styles.whiteText}>Duel</Text>
          </View>
        </TouchableHighlight>
      </View>
    </View>

  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }
  background(color) {
    return {
      backgroundColor: color
    }
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row'
  },
  mainButton: {
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  buttonInnerWrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoButton: {
    position: 'absolute',
    top: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: screenWidth/8,
    width: screenWidth/8,
    borderBottomLeftRadius: screenWidth/8,
    backgroundColor: colours.breakLines.dark
  },
  breakLineRight: {
    borderRightWidth: 1,
    borderRightColor: colours.breakLines.dark
  },
  whiteText: {
    color: 'white'
  },
  halfHeight: {
    height: screenWidth/2
  },
  halfWidth: {
    width: screenWidth/2
  }
});
