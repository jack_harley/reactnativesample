import _map from 'lodash/map';
import _toUpper from 'lodash/toUpper'
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity, StatusBar, Text, Image, Dimensions } from 'react-native'

import { layout, colours } from '../../../styles'
import IconFA from 'react-native-vector-icons/FontAwesome'

import { LoadingView, VideoPlayer } from '../../../views/components'

import NewGameButton from './buttons/games.new.button.js'
import ChallengeButton from './buttons/games.challenge.button.js'
import Challenges from './views/Challenges/challenges.container.js'
import NewGamesView from './views/games.new.view.js'
import CreateTimedDuelView from './create/timedDuel/timed.duel.create.container.js'
// games

const screenWidth = Dimensions.get('window').width
const roundedHeight = (screenWidth/16)*3

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class GamesView extends Component {

  fullscreenVideo(show, uri) {
    if(show) {
      this.props.hideMenuBar()
    } else {
      this.props.showMenuBar()
    }

    this.props.fullscreenVideo(show, uri)
  }

  hasTimedOut(time) {
    //check if time is up
    const d1 = new Date(time),
          d2 = new Date();

    return d1.getTime() - d2.getTime() < 0;
  }

  getTimeLeft(time) {
    const d1 = new Date(time),
          d2 = new Date();

    const duration = Math.abs(d2.getTime() - d1.getTime());

    let minutes = parseInt((duration/(1000*60))%60),
          hours = parseInt((duration/(1000*60*60))%24);

    hours   = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;

    return hours + "h " + minutes + "m";
  }

  _renderRoundedInfo(color, icon, state) {
    return <View style={styles.roundedOuter}>
      <View style={[styles.roundedWrap,  {backgroundColor: color}]}>
        <IconFA
          name={icon}
          backgroundColor="transparent"
          size={28}
          color={'rgba(255, 255, 255, 0.8)'}>
        </IconFA>
        <Text style={{color: 'white'}}>{state}</Text>
      </View>
    </View>
  }

  _renderTimedDuelPreview(item) {
    const { user } = this.props.state
    const { game, data, opponent } = item.server
    const gameState = item.server.state
    const coverImg = ( game.challenge.video.source.thumb.large || require('./create/duel/img/duel.jpg') )

    let state = '', color = '', title = ''

    if(user._id != game.createdBy && !gameState.accepted) {
      color = colours.scheme.primary
      title = 'new'
      icon = 'hourglass-start'
    } else if(!data.attempt[user._id].submitted) {
      color = colours.scheme.tertiary
      title = 'Record your attempt before your time runs out!'
      icon = 'hourglass-half'
      state = this.getTimeLeft(gameState.timeouts[user._id])
    } else if(user._id == game.createdBy && !gameState.accepted) {
      color = colours.scheme.tertiary
      title = 'waiting for ' + opponent.firstName + ' to accept the challenge'
      state = this.getTimeLeft(gameState.acceptedTimeout)
      icon = 'clock-o'
    } else if(this.hasTimedOut(gameState.timeouts[opponent._id])){
      color = colours.scheme.secondary
      title = opponent.firstName + ' didn\'t reply in time!'
      icon = 'hourglass-o'
    } else {
      color = colours.scheme.primary
      title = 'waiting'
      state = this.getTimeLeft(gameState.timeouts[opponent._id])
      icon = 'clock-o'
    }

    return <TouchableOpacity onPress={() => {this.props._goToGame('timedDuel', item)}} key={item.server.gameId} style={[styles.backgroundImage, styles.breakLineBottom]}>
      <Image source={coverImg} style={[styles.backgroundImage]}>
        <View style={[styles.flex, styles.breakLineBottom, styles.previewWrapInner]}>
          <View style={styles.gameTopWrap}>
              <View style={[styles.duelContent, styles.alignLeft]}><Text style={styles.initials}>{_toUpper(user.firstName)} {_toUpper(user.lastName)}</Text></View>
              <View style={[styles.duelContent, styles.alignCenter]}><Text style={styles.vs}>vs</Text></View>
              <View style={[styles.duelContent, styles.alignRight]}><Text style={styles.initials}>{_toUpper(item.server.opponent.firstName)} {_toUpper(item.server.opponent.lastName)}</Text></View>
          </View>
          <View style={styles.gameBottomWrap}>
            <View style={styles.gameBottomLeft}></View>
            {this._renderRoundedInfo(color, icon, state)}
          </View>
          <View style={[styles.colorBar, {backgroundColor: color}]}>
            <Text style={styles.gameTitle}>{title}</Text>
          </View>
        </View>
      </Image>
    </TouchableOpacity>
  }

  _renderGames(games) {
    return _map(games, (item) => {
      switch (item.server.game.type) {
        case 'timedDuel':
          return this._renderTimedDuelPreview(item)
        case 'duel':
          return this._renderDuelPreview(item)
        default:
          return null
      }
    })
  }

  _renderCurrentView() {

    const { currentView, user, gameInfo } = this.props.state

    switch (currentView) {
      case 'home':
        const { games, games_fetching, games_fetched, games_error } = this.props.state
        if(!games_fetching & !games_fetched & !games_error) {
          this.props.getGames(user._id)
          return
        } else if(games_fetching) {
          return <LoadingView />
        } else {
          return this._renderGames(games)
        }
      case 'challenges':
        return <Challenges
                _goToGame={this.props._goToGame}
                fullscreenVideo={this.fullscreenVideo.bind(this)}
                user={user}/>
      case 'newGame':
        const { selectGame, showGameInfo, hideGameInfo } = this.props
        return <NewGamesView
                user={user}
                gameInfo={gameInfo}
                selectGame={selectGame}
                showGameInfo={this.props.showGameInfo}
                hideGameInfo={this.props.hideGameInfo}
                />
      default:
        return null
    }

  }

  render () {

    const { updating, newGame, game, gameView, showingVideo, showingVideoUri } = this.props.state

    if(showingVideo) return <VideoPlayer source={showingVideoUri} fullscreen={true} autoplay={true} _goBack={this.fullscreenVideo.bind(this)} />

    switch (newGame) {
      case 'timedDuel':
        return <CreateTimedDuelView _goToGame={this.props._goToGame} goBack={this.props.goHome}/>
      case 'duel':
        return <View style={styles.container}>
        </View>
      default:
    }

    return <ScrollView style={[styles.container]}>
      <View style={[styles.halfHeight, styles.topContainer]}>
        <NewGameButton
          state={this.props.state}
          changeCurrentView={this.props.changeCurrentView}
          selectGame={this.props.selectGame}
        />
        <ChallengeButton
          state={this.props.state}
          challengeCount={this.props.state.user.challengeCount}
          changeCurrentView={this.props.changeCurrentView}
        />
      </View>
      <View>
        {this._renderCurrentView()}
      </View>
    </ScrollView>
  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }
  background(color) {
    return {
      backgroundColor: color
    }
  }

}


const styles = StyleSheet.create({
  flex: {
    flex: 1
  },
  container: {
    flex: 1,
    marginBottom: 48,
    backgroundColor: colours.background.dark
  },
  topContainer: {
    flexDirection: 'row'
  },
  breakLineBottom: {
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  whiteText: {
    color: 'white'
  },
  halfHeight: {
    height: screenWidth/2
  },
  halfWidth: {
    width: screenWidth/2
  },
  quarterHeight: {
    height: screenWidth/4
  },
  quarterWidth: {
    width: screenWidth/4
  },
  colorBar: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 20,
    zIndex: 2,
    marginBottom: -1
  },
  backgroundImage: {
    width: screenWidth,
    height: screenWidth/2,
  },
  duelContent: {
    flex: 1,
    justifyContent: 'center',
  },
  alignCenter: {
    alignItems: 'center'
  },
  alignLeft: {
    alignItems: 'flex-end'
  },
  alignRight: {
    alignItems: 'flex-start'
  },
  vs: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white'
  },
  initials: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white'
  },
  gameTopWrap: {
    flex: 4,
    position: 'relative',
    flexDirection: 'row'
  },
  gameBottomWrap: {
    flex: 3,
    position: 'relative',
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between'
  },
  gameBottomLeft: {
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center',
    padding:20
  },
  gameTitle: {
    fontWeight: '600',
    fontSize: 10,
    color: 'black',
  },
  gameDesc: {
    fontSize: 12,
    backgroundColor: 'transparent',
    textAlign: 'right',
    color: 'rgba(255, 255, 255, 0.8)'
  },
  roundedWrap: {
    borderRadius: 80,
    width: screenWidth/4,
    height: screenWidth/4,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20
  },
  roundedOuter: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'stretch'
  }
});
