import { connect } from 'react-redux'
import GamesItems from './games.home.view'
import {
  changeCurrentView,
  selectGame,
  showGameInfo,
  hideGameInfo,
  getGames,
  goToGame,
  goHome,
  fullscreenVideo
} from './games.home.actions'

import {
  hideMenuBar,
  showMenuBar
} from '../../navigation/tabs.actions'

function mapStateToProps (state) {
  return {
    state: state.gamesHomeReducer
  }
}

export default connect(
  mapStateToProps,
  {
    changeCurrentView: (view) => changeCurrentView(view),
    selectGame: (game) => selectGame(game),
    showGameInfo: (game) => showGameInfo(game),
    hideGameInfo: () => hideGameInfo(),
    getGames: (userId) => getGames(userId),
    goToGame: (game, gameType) => goToGame(game, gameType),
    goHome: () => goHome(),
    fullscreenVideo: (show, uri) => fullscreenVideo(show, uri),
    hideMenuBar: () => hideMenuBar(),
    showMenuBar: () => showMenuBar()
  }
)(GamesItems)
