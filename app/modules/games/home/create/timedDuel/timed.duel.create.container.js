import { connect } from 'react-redux'
import CreatTimedDuelView from './timed.duel.create.view'
import {
  getAvailableUsers,
  searchAvailableUsers,
  selectOpponent,
  challengeTitle,
  showRecordScreen,
  hideRecordScreen,
  saveChallengeVideo,
  sendChallenge
} from './timed.duel.create.actions'

import {
  hideMenuBar,
  showMenuBar
} from '../../../../navigation/tabs.actions'

import {
  doUpload,
  uploadFinished
} from '../../../../../views/components/uploader/uploader.actions'


function mapStateToProps (state) {
  return {
    state: state.createTimedDuelReducer,
    user: state.userReducer
  }
}

export default connect(
  mapStateToProps,
  {
    getAvailableUsers: (userId) => getAvailableUsers(userId),
    searchAvailableUsers: (qry) => searchAvailableUsers(qry),
    selectOpponent: (opponentId) => selectOpponent(opponentId),
    challengeTitle: (str) => challengeTitle(str),
    showRecordScreen: () => showRecordScreen(),
    hideRecordScreen: () => hideRecordScreen(),
    saveChallengeVideo: (uri) =>saveChallengeVideo(uri),
    hideMenuBar: () => hideMenuBar(),
    showMenuBar: () => showMenuBar(),
    doUpload: (files, gameId, cb) => doUpload(files, gameId, cb),
    uploadFinished: () => uploadFinished(),
    sendChallenge: (userId, challenge) => sendChallenge(userId, challenge)
  }
)(CreatTimedDuelView)
