import { Game, User } from '../../../../../services/api'

export function getAvailableUsers(userId) {
  return {
    type: 'TIMED_DUEL_CREATE_GET_USERS',
    payload: User.followers(userId)
  }
}

export function searchAvailableUsers(qry) {
  return {
    type: 'TIMED_DUEL_CREATE_SEARCH_USERS',
    qry
  }
}

export function selectOpponent(opponentId) {
  return {
    type: 'TIMED_DUEL_SELECT_USER',
    payload: User.get(opponentId)
  }
}

export function challengeTitle(str) {
  return {
    type: 'TIMED_DUEL_CHALLENGE_TITLE',
    str
  }
}

export function showRecordScreen() {
  return {
    type: 'TIMED_DUEL_SHOW_RECORD',
    record: 'show'
  }
}

export function hideRecordScreen() {
  return {
    type: 'TIMED_DUEL_HIDE_RECORD',
    record: 'hide'
  }
}

export function saveChallengeVideo(videoObj) {
  return {
    type: 'TIMED_DUEL_CHALLENGE_VIDEO',
    videoObj
  }
}

export function sendChallenge(userId, challenge) {
  console.log('send');
  return {
    type: 'SEND_CHALLENGE',
    payload: Game.sendChallenge(userId, challenge)
  }
}
