import _filter from 'lodash/filter'
import _includes from 'lodash/includes'
import _slice from 'lodash/slice'
import _lowerCase from 'lodash/lowerCase'

import uuid from 'react-native-uuid'

import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  view: 'choose-opponent',
  loading: false,
  showRecordScreen: false,
  recorded: false,
  usersSearchQry: '',
  usersPending: true,
  usersShowing: null,
  users: null,
  gameId: null,
  challenge: {},
  challengeTitle: ''
}

let showing = []

function createTimedDuelState (state = initialState, action) {
  switch (action.type) {
    case 'TIMED_DUEL_CREATE_GET_USERS_PENDING':
      return {
        ...state,
      }
    case 'TIMED_DUEL_CREATE_GET_USERS_REJECTED':
      showing = _slice(action.payload, 0, 10)
      return {
        ...state,
        usersPending: false,
        usersShowing: showing,
        users: action.payload
      }
    case 'TIMED_DUEL_CREATE_GET_USERS_FULFILLED':
      showing = _slice(action.payload, 0, 10)
      return {
        ...state,
        usersPending: false,
        usersShowing: showing,
        users: action.payload
      }
    case 'TIMED_DUEL_CREATE_SEARCH_USERS':
      showing = _filter(state.users, (n) => {
        return ( _includes(_lowerCase(n.user.firstName), _lowerCase(action.qry)) || _includes(_lowerCase(n.user.lastName), _lowerCase(action.qry)) )
      })
      showing = _slice(showing, 0, 10)
      return {
        ...state,
        usersSearchQry: action.qry,
        usersShowing: showing
      }
    case 'TIMED_DUEL_SELECT_USER_PENDING':
      return {
        ...state,
        loading: true
      }
    case 'TIMED_DUEL_SELECT_USER_REJECTED':
      return {
        ...state,
        loading: false
      }
    case 'TIMED_DUEL_SELECT_USER_FULFILLED':
      // TODO: Mocking
      return {
        ...state,
        view: 'describe-challenge',
        loading: false,
        gameId: uuid.v4(),
        // gameId: 'the-mocking-id',
        challenge: {
          opponent: action.payload
        }
      }
    case 'TIMED_DUEL_CHALLENGE_TITLE':
      return {
        ...state,
        challengeTitle: action.str
      }
    case 'TIMED_DUEL_SHOW_RECORD':
      return {
        ...state,
        showRecordScreen: true
      }
    case 'TIMED_DUEL_HIDE_RECORD':
      return {
        ...state,
        showRecordScreen: false
      }
    case 'TIMED_DUEL_CHALLENGE_VIDEO':
      return {
        ...state,
        recorded: true,
        showRecordScreen: false,
        view: 'add-title',
        challenge: {
          ...state.challenge,
          video: action.videoObj
        }
      }
    case 'SEND_CHALLENGE_PENDING':
      return {
        ...state,
        loading: true
      }
    case 'SEND_CHALLENGE_REJECTED':
      return {
        ...state,
        loading: false
      }
    case 'START_NEW_GAME':
      return initialState
    default:
      return state
  }
}

export default createTimedDuelState
