import _map from 'lodash/map'
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity, Text, Image, ListView, Dimensions } from 'react-native'

import { layout, colours } from '../../../../../styles'

import { LoadingView, TextInput, VideoRecorder, VideoPlayer, RecordButton } from '../../../../../views/components'

import Icon from 'react-native-vector-icons/FontAwesome'

const screenWidth = Dimensions.get('window').width
const dimensions = {
  videoHeight: {
    height: screenWidth*9/16
  }
}

export default class GamesView extends Component {

  componentDidMount() {
    this.props.getAvailableUsers(this.props.user.user._id)
  }

  /*============================================================================
    Functions
  ============================================================================*/

  findUser(qry) {
    this.props.searchAvailableUsers(qry)
  }

  saveChallengeVideo(vid) {
    this.props.showMenuBar()
    this.props.saveChallengeVideo(vid)
  }

  hideRecordScreen() {
    this.props.showMenuBar()
    this.props.hideRecordScreen()
  }

  showRecordScreen() {
    // TODO: Mocking
    // const hostedThumbs = {
    //   small: 'https://s3-eu-west-1.amazonaws.com/single-video-attempts/the-mocking-id/challenge/timedduel_small.png',
    //   large: 'https://s3-eu-west-1.amazonaws.com/single-video-attempts/the-mocking-id/challenge/timedduel_large.png',
    // }
    // this.sendGame('https://s3-eu-west-1.amazonaws.com/single-video-attempts/the-mocking-id/challenge/timedduel.mp4', hostedThumbs)

    this.props.hideMenuBar()
    this.props.showRecordScreen()
  }

  sendGame(hostedUri, hostedThumbs) {
    const video = this.props.state.challenge.video
    video.source.uri = hostedUri
    video.source.thumb = {
      small: {
        uri: hostedThumbs.small
      },
      large: {
        uri: hostedThumbs.large
      }
    }

    const HOUR = 60*60*1000
    const dateNow = new Date().toISOString()
    let date12Hours = new Date()
    date12Hours.setTime(date12Hours.getTime() + (12*HOUR))
    date12Hours = date12Hours.toISOString()
    let date24Hours = new Date()
    date24Hours.setTime(date24Hours.getTime() + (24*HOUR))
    date24Hours = date24Hours.toISOString()

    const challenge = {
      data: {
        game: {
          type: "timedDuel",
          createdAt: dateNow,
          challenge: {
            title: this.props.state.challengeTitle.length > 0 ? this.props.state.challengeTitle : null,
            video: video
          }
        },
        data: {
          attempt: {}
        }
      },
      state: {
        accepted: false,
        declined: false,
        acceptedTimeout: date12Hours,
        timeouts: {}
      },
      user: this.props.user.user,
      opponent: this.props.state.challenge.opponent,
      gameId: this.props.state.gameId
    }

    challenge.data.data.attempt[this.props.user.user._id] = {
      submitted: false
    }
    challenge.data.data.attempt[this.props.state.challenge.opponent._id] = {
      submitted: false
    }

    challenge.state.timeouts[this.props.user.user._id] = date24Hours
    challenge.state.timeouts[this.props.state.challenge.opponent._id] = null

    //
    this.props.sendChallenge(this.props.user.user._id, challenge)
      .then((res) => {
        this.props._goToGame('timedDuel', {server: res.action.payload})
      })
  }

  uploadChallengeVideo() {

    const { video } = this.props.state.challenge

    const uploadFormat =  {
      bucket: 'clips',
      filename: this.props.state.gameId + '/challenge/timedduel',
      filepath: video.source.uri.replace(/^.*[\\\/]/, ''),
      fileType: 'video/mp4',
    }

    this.props.doUpload(uploadFormat, this.props.state.gameId, this.sendGame.bind(this))
  }

  /*============================================================================
    Renders
  ============================================================================*/

  _renderHeader() {
    let title = ''
    switch (this.props.state.view) {
      case 'choose-opponent':
        title = 'Choose Opponent'
        break
      case 'describe-challenge':
        title = 'Describe the Challenge'
        break
      case 'add-title':
        title = 'Title'
    }
    return (
      <View style={[layout.header, styles.dark]}>
        <View style={{flex: 1}}>
          <Icon.Button
            name="angle-left"
            onPress={ () => {this.props.goBack()} }
            backgroundColor="transparent"
            style={{padding: 5, borderRadius: 0}}
            size={28}
            color={colours.breakLines.dark}
            iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
          </Icon.Button>
        </View>
        <View style={[styles.center, {flex: 3}]}>
          <Text style={styles.text}>{title}</Text>
        </View>
        <View style={{flex: 1}}></View>
      </View>
    )
  }

  _renderOpponentRow(item) {
    const follower = item.user
    return <TouchableOpacity onPress={ () => { this.props.selectOpponent(item.followerId) } } style={styles.row}>
      <View style={styles.imgWrap}>
        <Image style={styles.img} source={follower.avatar.small}/>
      </View>
      <View style={styles.details}>
        <Text style={styles.text}>{follower.firstName} {follower.lastName}</Text>
      </View>
    </TouchableOpacity>
  }

  _renderUserList() {
    if(this.props.state.usersPending) return <LoadingView />
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    return <ScrollView style={[layout.container, styles.dark, styles.body]}>
      <ListView
        dataSource={ds.cloneWithRows(this.props.state.usersShowing)}
        renderRow={this._renderOpponentRow.bind(this)}
        enableEmptySections />
    </ScrollView>
  }

  _renderSelectUserContent() {
    return <View style={[layout.content, styles.dark]}>
      <TextInput
        currentInput={ this.props.state.usersSearchQry }
        onInput={ (qry) => this.findUser(qry) }
        search={true}
        size={18}
        color={colours.scheme.primary}
        breakLines={colours.greyscale.dark} />
      {this._renderUserList()}
    </View>
  }

  _renderChallenge() {
    const { user } = this.props.user
    const { opponent } = this.props.state.challenge

    return <View style={styles.challengeWrap}>
      <View style={styles.playerWrap}>
        <View><Image style={styles.imgBig} source={user.avatar.small}/></View>
        <View><Text style={styles.text}>{user.firstName} {user.lastName}</Text></View>
      </View>
      <View style={styles.vsWrap}><Text style={styles.text}>VS</Text></View>
      <View style={styles.playerWrap}>
        <View><Image style={styles.imgBig} source={opponent.avatar.small}/></View>
        <View><Text style={styles.text}>{opponent.firstName} {opponent.lastName}</Text></View>
      </View>
    </View>
  }

  _renderRecordAction() {
    return <View style={{flex: 2}}>
      <View style={[styles.wrap, styles.borderBottom]}>
        <Text style={[styles.text, styles.recordText]}>Record yourself describing your challenge.</Text>
        <Text style={[styles.text, styles.recordText]}>Once you’ve sent the challenge, you have 24 hours to record your attempt, but you only get one attempt.</Text>
        <Text style={[styles.text, styles.recordText]}>Attempts must be less than 20 seconds.</Text>
      </View>
      <View style={styles.wrap}>
        <RecordButton record={this.showRecordScreen.bind(this)}/>
      </View>
    </View>
  }

  _renderSendActions() {
    return <View style={{flex: 2}}>
      <View style={[styles.titleWrap, styles.borderBottom]}>
        <View style={{marginBottom: 10}}>
          <Text style={styles.text}>Give your challenge a title</Text>
        </View>
        <View style={styles.titleInner}>
          <Icon
            name="pencil"
            color={colours.background.light}
            size={24}
            style={[{textAlign: 'center', marginLeft: 23}]} >
          </Icon>
          <TextInput
            currentInput={ this.props.state.challengeTitle }
            onInput={ (qry) => this.props.challengeTitle(qry) }
            color={ colours.background.light }
            breakLines={ colours.background.dark } />
        </View>
      </View>
      <View style={styles.wrap}>
        {
          this.props.state.challengeTitle.length &&
          <TouchableOpacity style={[styles.sendButton, styles.sendButtonActive]} onPress={this.uploadChallengeVideo.bind(this)}>
            <Text style={styles.text}>SEND</Text>
          </TouchableOpacity>
          ||
          <View style={styles.sendButton}>
            <Text style={styles.text}>SEND</Text>
          </View>
        }
      </View>
    </View>
  }

  _renderDescribeChallengeContent() {
    return <View style={[layout.content, styles.dark]}>
      { this._renderChallenge() }
      { this._renderRecordAction() }
    </View>
  }

  _renderGiveTitleContent() {
    return <View style={[layout.content, styles.dark]}>
      { this._renderChallenge() }
      { this._renderSendActions() }
    </View>
  }

  _renderContent() {
    switch (this.props.state.view) {
      case 'choose-opponent':
        return this._renderSelectUserContent()
        break
      case 'describe-challenge':
        return this._renderDescribeChallengeContent()
        break
      case 'add-title':
        return this._renderGiveTitleContent()
        break
      default:
    }
  }

  render () {

    if(this.props.state.loading) return <LoadingView />

    if(this.props.state.showRecordScreen) {
      return <VideoRecorder
              onStop={this.saveChallengeVideo.bind(this)}
              selfieCam={true}
              goBack={this.hideRecordScreen.bind(this)} />
    }

    return <View style={{flex: 1}}>
      {this._renderHeader()}
      {this._renderContent()}
    </View>

  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }

}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: colours.background.dark
  },
  challengeWrap: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: colours.background.medium,
    borderTopWidth: 1,
    borderTopColor: colours.breakLines.dark,
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  playerWrap: {
    flex: 1,
    alignItems: 'center'
  },
  vsWrap: {
    width: 30,
    alignItems: 'center'
  },
  dark: {
    backgroundColor: colours.background.dark,
    borderBottomColor: colours.breakLines.dark
  },
  text: {
    color: colours.greyscale.medium,
    fontFamily: 'opensans'
  },
  recordText: {
    textAlign: 'center',
    marginBottom: 8
  },
  h1: {
    color: colours.greyscale.light,
    fontSize: 20
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  body: {
    paddingTop: 20,
    marginLeft: 40,
    marginRight: 40
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingTop: 8,
    paddingBottom: 8,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.breakLines.dark,
  },
  imgWrap: {
    width: 40,
    marginRight: 10
  },
  img: {
    height: 40,
    width: 40,
    borderRadius: 20
  },
  imgBig: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: colours.breakLines.dark
  },
  details: {
    justifyContent: 'center',
    flex: 1
  },
  recordWrap: {
    padding: 80
  },
  recordButton: {
    height: 40,
    width: 40,
    backgroundColor: colours.scheme.secondary,
    borderRadius: 20
  },
  titleWrap: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding:40
  },
  titleInner: {
    height: 30,
    width: screenWidth - 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: colours.background.light,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  sendButton: {
    borderWidth: StyleSheet.hairlineWidth,
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: colours.background.dark
  },
  sendButtonActive: {
    backgroundColor: colours.scheme.primary
  }
})
