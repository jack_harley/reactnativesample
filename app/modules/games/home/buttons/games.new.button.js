import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, Dimensions, TouchableOpacity } from 'react-native'
import { colours } from '../../../../styles'

import Icon from 'react-native-vector-icons/FontAwesome'

const screenWidth = Dimensions.get('window').width

export default class NewGameButton extends Component {

  goHome() {
    this.props.changeCurrentView('home')
  }

  goToNewGame() {
    // this.props.changeCurrentView('newGame')
  }

  newGame() {
    this.props.selectGame('timedDuel')
  }

  render() {
    const { currentView } = this.props.state

    if(currentView == 'home') {
      return <TouchableOpacity onPress={this.newGame.bind(this)} style={[styles.halfWidth, styles.halfHeight, styles.mainButton, styles.breakLineRight]}>
        <View style={{justifyContent: 'center'}}>
          <Icon name="gamepad" color={colours.greyscale.medium} size={48} style={{textAlign: 'center'}}></Icon>
          <Text style={{color: colours.greyscale.medium}}>New Game</Text>
        </View>
      </TouchableOpacity>
    }

    if(currentView == 'newGame') {
      return <View>
        <View></View>
        <TouchableOpacity onPress={this.goHome.bind(this)} style={[styles.fullWidth, styles.halfHeight, styles.mainButton]}>
          <View>
            <Text style={styles.whiteText}>NewGame</Text>
          </View>
        </TouchableOpacity>
        <View></View>
      </View>
    }

    return null

  }

}

const styles = StyleSheet.create({
  mainButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  breakLineRight: {
    borderRightWidth: 1,
    borderRightColor: colours.breakLines.dark
  },
  whiteText: {
    color: 'white'
  },
  halfHeight: {
    height: screenWidth/2
  },
  halfWidth: {
    width: screenWidth/2
  },
  fullWidth: {
    width: screenWidth
  }
});
