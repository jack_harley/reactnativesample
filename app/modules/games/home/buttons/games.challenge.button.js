import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, Dimensions, TouchableHighlight } from 'react-native'
import { colours } from '../../../../styles'

import Icon from 'react-native-vector-icons/Entypo'

const screenWidth = Dimensions.get('window').width

export default class ChallengeButton extends Component {

  goHome() {
    this.props.changeCurrentView('home')
  }

  goToChallenges() {
    this.props.changeCurrentView('challenges')
  }

  _renderChallengeCount(count) {
    if(count > 0) return <View style={styles.challengeCount}><Text style={styles.challengeCountText}>{count}</Text></View>
    return null
  }

  render () {

    const { currentView } = this.props.state
    const { challengeCount } = this.props

    if( currentView == 'home' ) {
      return <TouchableHighlight onPress={this.goToChallenges.bind(this)} style={[styles.halfWidth, styles.halfHeight, styles.mainButton]}>
        <View>
          {this._renderChallengeCount(challengeCount)}
          <Icon name="trophy" color={colours.greyscale.medium} size={48} style={{textAlign: 'center'}}></Icon>
          <Text style={{color: colours.greyscale.medium}}>Challenge</Text>
        </View>
      </TouchableHighlight>
    }

    if( currentView == 'challenges' ) {
      return <View>
        <TouchableHighlight onPress={this.goHome.bind(this)} style={[styles.fullWidth, styles.halfHeight, styles.mainButton]}>
          <View>
          {this._renderChallengeCount(challengeCount)}
          <Icon name="trophy" color={colours.greyscale.medium} size={48} style={{textAlign: 'center'}}></Icon>
          <Text style={{color: colours.greyscale.medium}}>Challenge</Text>
          </View>
        </TouchableHighlight>
      </View>
    }

    return null

  }

}

const styles = StyleSheet.create({
  mainButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: colours.breakLines.dark
  },
  whiteText: {
    color: 'white'
  },
  halfHeight: {
    height: screenWidth/2
  },
  halfWidth: {
    width: screenWidth/2
  },
  fullWidth: {
    width: screenWidth
  },
  challengeCountText: {
    fontSize: 9,
    backgroundColor: 'transparent',
    color: colours.background.dark
  },
  challengeCount: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 9,
    width: 16,
    height: 16,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.scheme.primary,
    borderWidth: 1,
    borderColor: colours.background.dark
  }
});
