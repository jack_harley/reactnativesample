export function push(route) {
  return {
    type: 'GAMES_PUSH_ROUTE',
    route
  }
}

export function pop () {
  return {
    type: 'GAMES_POP_ROUTE'
  }
}

export function fullscreenVideo(show, uri) {
  return {
    type: show ? 'SHOW_FULLSCREEN_VIDEO' : 'HIDE_FULLSCREEN_VIDEO',
    uri
  }
}
