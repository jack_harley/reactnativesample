import React, { Component } from 'react'
import GamesView from '../home/games.home.container'
import TimedDuelView from '../games/timedDuel/timedDuel.game.container'

import { VideoPlayer } from '../../../views/components'

import {
  BackAndroid,
  NavigationExperimental,
  View
} from 'react-native'

const {
  Reducer: NavigationTabsReducer,
  CardStack: NavigationCardStack
} = NavigationExperimental

class GamesRoot extends Component {
  constructor (props) {
    super(props)
    this._renderScene = this._renderScene.bind(this)
    this._handleBackAction = this._handleBackAction.bind(this)
  }
  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this._handleBackAction)
  }
  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this._handleBackAction)
  }
  _renderScene (props) {
    const { route } = props.scene
    let key = route.key
    key = key.indexOf('_') >= 0 ? key.substring(0, key.indexOf('_')) : key

    switch (key) {
      case 'timedDuel':
        return (
          <TimedDuelView
            fullscreenVideo={this.fullscreenVideo.bind(this)}
            game={route.game}
            _goBack={this._handleBackAction.bind(this)}
            changeTab={ () => { this.props.changeTab('gallery')} }
           />
        )
      case 'games':
        return (
          <GamesView
            _handleNavigate={this._handleNavigate.bind(this)}
            _goToGame={this._goToGame.bind(this)}
            state={this.props.state}
           />
        )
      default:
        return <View />
    }
  }
  _goToGame(type, game) {
    this._handleNavigate({
      type: 'push',
      route: {
        key: type,
        game
      }
    });
  }
  _handleBackAction () {
    if (this.props.state.index === 0) {
      return false
    }
    this.props.popRoute()
    return true
  }
  _handleNavigate (action) {
    switch (action && action.type) {
      case 'push':
        this.props.pushRoute(action.route)
        return true
      case 'back':
      case 'pop':
        return this._handleBackAction()
      default:
        return false
    }
  }
  fullscreenVideo(show, uri) {
    if(show) {
      this.props.hideMenuBar()
    } else {
      this.props.showMenuBar()
    }

    this.props.fullscreenVideo(show, uri)
  }
  render () {
    const { showingVideo, showingVideoUri } = this.props.state

    if(showingVideo) return <VideoPlayer source={showingVideoUri} fullscreen={true} autoplay={true} _goBack={this.fullscreenVideo.bind(this)} />

    return (
      <NavigationCardStack
        style={{flex: 1}}
        navigationState={this.props.state}
        onNavigate={this._handleNavigate.bind(this)}
        renderScene={this._renderScene} />
    )
  }
}

export default GamesRoot
