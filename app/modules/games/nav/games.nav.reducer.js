import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  index: 0,
  key: 'games.nav',
  routes: [
    {
      key: 'games'
    }
  ],
  showingVideo: false,
  showingVideoUri: null
}

function gamesState (state = initialState, action) {
  switch (action.type) {
    case 'GAMES_PUSH_ROUTE':
      let newState = {...state}
      if(action.route.key.indexOf('games.item') >= 0 || action.route.key.indexOf('profile') >= 0) {
        action.route.key = action.route.key + '_' + state.index
      }
      else if (state.routes[state.index].key === (action.route && action.route.key)) {
        return state
      }
      return NavigationStateUtils.push(newState, action.route)
    case 'GAMES_POP_ROUTE':
      if (state.index === 0 || state.routes.length === 1) return state
      return NavigationStateUtils.pop(state)
    case 'SHOW_FULLSCREEN_VIDEO':
      return {
        ...state,
        showingVideo: true,
        showingVideoUri: action.uri
      }
    case 'HIDE_FULLSCREEN_VIDEO':
      return {
        ...state,
        showingVideo: false,
        showingVideoUri: null
      }
    default:
      return state
  }
}

export default gamesState
