import { connect } from 'react-redux'
import GamesNav from './games.nav.view'
import { push, pop, fullscreenVideo } from './games.nav.actions'
function mapStateToProps (state) {
  return {
    state: state.gamesNavReducer
  }
}

import {
  hideMenuBar,
  showMenuBar
} from '../../navigation/tabs.actions'

export default connect(
  mapStateToProps,
  {
    pushRoute: (route) => push(route),
    popRoute: ()=> pop(),
    fullscreenVideo: (show, uri) => fullscreenVideo(show, uri),
    hideMenuBar: () => hideMenuBar(),
    showMenuBar: () => showMenuBar()
  }
)(GamesNav)
