import { Game, TimedDuel } from '../../../../services/api'

export function showRecordScreen() {
  return {
    type: 'TIMEDDUEL_SHOW_RECORD'
  }
}

export function hideRecordScreen() {
  return {
    type: 'TIMEDDUEL_HIDE_RECORD'
  }
}

export function saveAttempt(gameId, user) {
  return {
    type: 'TIMEDDUEL_SAVE_ATTEMPT',
    payload: TimedDuel.saveAttempt(gameId, user)
  }
}

export function accept() {
  return {
    type: 'TIMEDDUEL_ACCEPT'
  }
}

export function removeGame(gameId, userId, opponentId) {
  return {
    type: 'TIMEDDUEL_REMOVE',
    payload: Game.delete(gameId, userId, opponentId)
  }
}

export function updateGame(game) {
  return {
    type: 'TIMEDDUEL_GAME_UPDATE',
    game
  }
}

export function getGame(gameId, userId) {
  return {
    type: 'TIMEDDUEL_GAME_GET',
    payload: Game.getById(gameId, userId)
  }
}

export function showFinishedScreen() {
  return {
    type: 'TIMEDDUEL_FINISHED'
  }
}
