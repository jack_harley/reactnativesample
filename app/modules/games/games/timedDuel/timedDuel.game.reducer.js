import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  game: null,
  loading: false,
  showRecordScreen: false,
  showFinishedScreen: false
}

function timedDuelState (state = initialState, action) {
  switch (action.type) {
    case 'GAMES_PUSH_ROUTE':
      return {
        ...state,
        game: action.route.game
      }
    case 'TIMEDDUEL_SHOW_RECORD':
      return {
        ...state,
        showRecordScreen: true
      }
    case 'TIMEDDUEL_HIDE_RECORD':
      return {
        ...state,
        loading: true,
        showRecordScreen: false
      }
    case 'TIMEDDUEL_REMOVE_PENDING':
      return {
        ...state,
        loading: true
      }
    case 'TIMEDDUEL_REMOVE_REJECTED':
      return {
        ...state,
        loading: false
      }
    case 'TIMEDDUEL_GAME_GET':
      return {
        ...state,
        loading: true
      }
    case 'TIMEDDUEL_GAME_UPDATE':
      return {
        ...state,
        game: action.game,
        loading: false
      }
    case 'TIMEDDUEL_SAVE_ATTEMPT_PENDING':
      return {
        ...state,
        loading: true
      }
    case 'TIMEDDUEL_SAVE_ATTEMPT_REJECTED':
      return {
        ...state,
        loading: false
      }
    case 'TIMEDDUEL_FINISHED':
      return {
        ...state,
        showFinishedScreen: true
      }
    default:
      return state
  }
}

export default timedDuelState
