import _map from 'lodash/map'
import _toUpper from 'lodash/toUpper'
import _clone from 'lodash/clone'

import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableOpacity, StatusBar, Text, Image, Dimensions } from 'react-native'

import IconFA from 'react-native-vector-icons/FontAwesome'
import Ionicon from 'react-native-vector-icons/Ionicons'

import { layout, colours } from '../../../../styles'

import { LoadingView, VideoRecorder, VideoPlayer, VideoLink, Countdown, RecordButton } from '../../../../views/components'

const screenWidth = Dimensions.get('window').width

export default class GamesView extends Component {


  /*============================================================================
    FUNCTIONS
  ============================================================================*/

  // RECORD

  showRecordScreen() {
    this.props.hideMenuBar()
    this.props.showRecordScreen()
  }

  hideRecordScreen() {
    this.props.showMenuBar()
    this.props.hideRecordScreen()
  }

  saveAttempt() {
    const { gameId } = this.props.state.game.server
    const { user } = this.props.user
    this.props.saveAttempt(gameId, user)
      .then((result) => {
        const { game } = result.action.payload
        this.props.updateGame(game)
      })
  }

  uploadAttempt(video) {
    this.props.showMenuBar()
    this.props.hideRecordScreen()

    const { gameId } = this.props.state.game.server
    const { user } = this.props.user

    let uploadFormat =  {
      bucket: 'clips',
      filepath: video.source.uri.replace(/^.*[\\\/]/, ''),
      fileType: 'video/mp4',
    }
    this.props.getGame(gameId, user._id)
      .then((result) => {
        const gameObj = result.value.data
        if(result.value.finished) {
          uploadFormat.filename = `finished/${gameObj.gameId}/${user._id}/timedduel`
          this.props.doUpload(uploadFormat, gameId, this.props.showFinishedScreen)
        } else {
          uploadFormat.filename = `${gameObj.gameId}/${user._id}/timedduel`
          this.props.doUpload(uploadFormat, gameId, this.saveAttempt.bind(this))
        }
      })
  }

  removeGame() {
    const { user } = this.props.user
    const { opponent, gameId } = this.props.state.game.server
    this.props.removeGame(gameId, user._id, opponent._id)
      .then(() => {
        this.props._goBack()
      })
  }

  // HELPERS

  opponentTimedOut() {
    const { opponent, state } = this.props.state.game.server
    const time = state.accepted
                  ? state.timeouts[opponent._id]
                  : state.acceptedTimeout
    return this.timedOut(time)
  }

  timedOut(time) {
    const now = new Date().getTime(),
          end = new Date(time).getTime(),
          duration = end - now
    return ( duration < 0 )
  }

  /*============================================================================
    HEADER VIEW
  ============================================================================*/

  _renderHeader() {
    return <View style={header.wrap}>
      <View style={{flex: 1}}>
        <IconFA.Button
          name="angle-left"
          onPress={this.props._goBack}
          backgroundColor="transparent"
          style={{padding:5, borderRadius: 0}}
          size={28}
          color={colours.greyscale.medium}
          iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
        </IconFA.Button>
      </View>
    </View>
  }

  /*============================================================================
    DETAILS VIEWS
  ============================================================================*/

  _renderGameImage() {
    const { user } = this.props.user
    const { game } = this.props.state.game.server
    return <View style={details.wrap}>
      <VideoLink thumb={game.challenge.video.source.thumb.large} fullscreenVideo={() => { this.props.fullscreenVideo(true, game.challenge.video.source.uri) }} />
    </View>
  }

  _renderGameDetails() {
    const { user } = this.props.user
    const { opponent } = this.props.state.game.server
    return <View style={details.players}>
      <View style={details.textLeftWrap}><Text style={text.default}>{user.firstName} {user.lastName}</Text></View>
      <View style={details.vsWrap}><Text style={text.blue}>VS</Text></View>
      <View style={details.textRightWrap}><Text style={text.default}>{opponent.firstName} {opponent.lastName}</Text></View>
    </View>
  }

  /*============================================================================
    BOX VIEWS
  ============================================================================*/
  _renderTimeLeftSubmit(name, endTime) {
    const context = name
                    ? `hours left for ${name} to submit thier attempt`
                    : 'hours left for you to submit your attempt'
    return <View>
      <Countdown end={endTime}/>
      <Text style={[text.default, gamestate.text]}>{context}</Text>
    </View>
  }

  _renderTimeLeftAccept(name, endTime) {
    return <View>
      <Countdown end={endTime}/>
      <Text style={[text.default, gamestate.text]}>hours left for {name} to accept the challenge</Text>
    </View>
  }

  _renderSubmitted(name) {
    const context = name
                    ? `${name} has submitted thier attempt`
                    : 'you have submitted your attempt'
    return <View style={{alignItems: 'center'}}>
      <Ionicon
       name='ios-checkmark'
       size={48}
       color={colours.scheme.primary}>
      </Ionicon>
      <Text style={[text.default, gamestate.text]}>{context}</Text>
    </View>
  }

  _renderTimeout(name) {
    return <View>
      <Text style={[text.default, gamestate.text]}>{name} has failed to submit thier entry in time.</Text>
    </View>
  }

  getOpponentBox() {
    const { opponent } = this.props.state.game.server

    const game = this.props.state.game.server

    if(!game.state.accepted) return  this._renderTimeLeftAccept(opponent.firstName, game.state.acceptedTimeout)

    if(game.data.attempt[opponent._id].submitted) return this._renderSubmitted(opponent.firstName)

    if( this.opponentTimedOut() ) return this._renderTimeout(opponent.firstName)

    return this._renderTimeLeftSubmit(opponent.firstName, game.state.timeouts[opponent._id])
  }

  getUserBox() {
    const { user } = this.props.user
    const game = this.props.state.game.server

    if(game.data.attempt[user._id].submitted) return this._renderSubmitted()

    return this._renderTimeLeftSubmit(null, game.state.timeouts[user._id])
  }

  _renderGameState() {

    const { user } = this.props.user
    const game = this.props.state.game.server
    const { opponent } = game

    if(!game.state.accepted) {
      if(game.state.declined || this.opponentTimedOut()) {
        const declinedStr = game.state.declined ?
                            `${opponent.firstName} has declined your challenge` :
                            `${opponent.firstName} hasn't accepted your challenge in time`
        return <View style={gamestate.wrap}>
          <View style={gamestate.inner}>
            <View style={[gamestate.box]}><Text style={text.default}>{declinedStr}</Text></View>
          </View>
          <View style={gamestate.inner}>
            {this._renderRemoveGame()}
          </View>
        </View>
      }
    }

    const oppTimedOut  = game.state.timeouts[opponent._id] ? this.timedOut(game.state.timeouts[opponent._id]) : false,
          userTimedOut = this.timedOut(game.state.timeouts[user._id])

    if(oppTimedOut || userTimedOut) {
      const timedOutStr = oppTimedOut ?
                          `${opponent.firstName} has run out of time` :
                          `You have run out of time`
      return <View style={gamestate.wrap}>
        <View style={gamestate.inner}>
          <View style={[gamestate.box]}><Text style={text.default}>{timedOutStr}</Text></View>
        </View>
        <View style={gamestate.inner}>
          {this._renderRemoveGame()}
        </View>
      </View>
    }

    return <View style={gamestate.wrap}>
      <View style={gamestate.inner}>
        <View style={[gamestate.box, styles.borderRight]}>{ this.getUserBox() }</View>
        <View style={gamestate.box}>{ this.getOpponentBox() }</View>
      </View>
      <View style={gamestate.inner}>
        {this._renderRecord()}
      </View>
    </View>
  }

  /*============================================================================
    ACTION VIEWS
  ============================================================================*/

  _renderRecord() {
    const { user } = this.props.user
    const { data } = this.props.state.game.server
    return <View style={action.wrap}>
      {
        data.attempt[user._id].submitted
        ? <Text style={text.default}>Waiting</Text>
        : <RecordButton record={this.showRecordScreen.bind(this)}/>
      }
    </View>
  }
  // <RecordButton record={this.showRecordScreen.bind(this)}/>

  _renderWaiting() {
    return <View style={action.wrap}>
      <Text>Waiting</Text>
    </View>
  }

  _renderRemoveGame() {
    return <View style={action.wrap}>
      <TouchableOpacity onPress={ this.removeGame.bind(this) } style={styles.row}>
        <Text style={text.red}>OK</Text>
      </TouchableOpacity>
    </View>
  }

  /*============================================================================
    FINISHED VIEW
  ============================================================================*/

  _renderFinishedScreen() {
    return <View style={finished.wrap}>
      <Text style={finished.h1}>Game Finished</Text>
      <Text style={finished.h2}>You will recieve a notification in a few seconds when your video is ready</Text>
      <Text style={finished.p}>*if you have notifications turned off, your video will appear at the top of your feed</Text>
    </View>
  }

  /*============================================================================
    RENDER
  ============================================================================*/

  render() {

    console.log('props', this.props.state);

    if(this.props.state.loading) return <LoadingView />

    if(this.props.state.showFinishedScreen) {
      return <View style={styles.wrap}>
        {this._renderHeader()}
        {this._renderFinishedScreen()}
      </View>
    }

    if(this.props.state.showRecordScreen) {
      return <VideoRecorder
              onStop={this.uploadAttempt.bind(this)}
              selfieCam={false}
              goBack={this.hideRecordScreen.bind(this)} />
    }

    return <View style={styles.wrap}>
      {this._renderHeader()}
      {this._renderGameImage()}
      {this._renderGameDetails()}
      {this._renderGameState()}
    </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 2
    }
  }

}

const styles = StyleSheet.create({
  wrap: {
    backgroundColor: colours.background.dark,
    flex: 1,
    marginBottom: 50
  },
  halfHeight: {
    height: screenWidth/2
  },
  quarterHeight: {
    height: screenWidth/4
  },
  whiteText: {
    color: 'white'
  },
  playButton: {
    width: 58,
    height: 34,
    backgroundColor: 'black',
    opacity: 0.6,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  borderRight: {
    borderRightWidth: 1,
    borderRightColor: colours.breakLines.dark
  }
})

const header = StyleSheet.create({
  wrap: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  }
})

const details = StyleSheet.create({
  wrap: {
    flex: 1,
    borderTopWidth: 1,
    borderTopColor: colours.greyscale.dark,
    borderBottomWidth: 1,
    borderBottomColor: colours.greyscale.dark
  },
  players: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textLeftWrap: {
    justifyContent: 'flex-end'
  },
  vsWrap: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textRightWrap: {
    justifyContent: 'flex-start'
  }
})

const action = StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const gamestate = StyleSheet.create({
  wrap: {
    flex: 2,
  },
  inner: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: colours.breakLines.dark,
  },
  box: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
    marginTop: 8
  },
  text: {
    textAlign: 'center',
    padding: 15
  }
})

const text = StyleSheet.create({
  default: {
    color: colours.greyscale.medium,
    fontWeight: '400'
  },
  blue: {
    color: colours.scheme.tertiary,
    fontWeight: '400'
  },
  red: {
    color: colours.scheme.secondary,
    fontWeight: '400'
  },
  green: {
    color: colours.scheme.primary,
    fontWeight: '400'
  }
})

const finished = StyleSheet.create({
  wrap: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  h1: {
    color: colours.scheme.primary,
    fontSize: 36,
    padding: 30,
    textAlign: 'center'
  },
  h2: {
    color: 'white',
    fontSize: 24,
    padding: 30,
    textAlign: 'center'
  },
  p: {
    color: 'white',
    fontSize: 12,
    padding: 10,
    textAlign: 'center'
  }
})
