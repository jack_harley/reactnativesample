import { connect } from 'react-redux'
import TimedDuels from './timedDuel.game.view'
import {
  showRecordScreen,
  hideRecordScreen,
  saveAttempt,
  accept,
  getGame,
  removeGame,
  updateGame,
  showFinishedScreen,
} from './timedDuel.game.actions'

import {
  hideMenuBar,
  showMenuBar
} from '../../../navigation/tabs.actions'

import {
  doUpload,
  uploadFinished
} from '../../../../views/components/uploader/uploader.actions'


function mapStateToProps(state) {
  return {
    state: state.timedDuelReducer,
    user: state.userReducer
  }
}

export default connect(
  mapStateToProps,
  {
    getGame: (gameId, userId) => getGame(gameId, userId),
    updateGame: (game) => updateGame(game),
    accept: () => accept(),
    saveAttempt: (gameId, user) => saveAttempt(gameId, user),
    removeGame: (gameId, userId, opponentId) => removeGame(gameId, userId, opponentId),
    hideMenuBar: () => hideMenuBar(),
    showMenuBar: () => showMenuBar(),
    showRecordScreen: () => showRecordScreen(),
    hideRecordScreen: () => hideRecordScreen(),
    doUpload: (files, gameId, cb) => doUpload(files, gameId, cb),
    uploadFinished: () => uploadFinished(),
    showFinishedScreen: () => showFinishedScreen()
  }
)(TimedDuels)
