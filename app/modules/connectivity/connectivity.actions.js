export function connectivityChange (status, kind) {
  return {
    type: 'CONNECTIVITY_STATUS_CHANGE',
    status,
    kind
  }
}
