const initialState = {
  connected: true,
  kind: 'MOBILE'
}

function connectivityState (state = initialState, action) {
  switch (action.type) {
    case 'CONNECTIVITY_STATUS_CHANGE':
      return {
        ...state,
        connected: action.status,
        kind: action.kind
      }
    default:
      return state
  }
}


export default connectivityState
