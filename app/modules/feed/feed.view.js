import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, TouchableHighlight, StatusBar, Text, Image, Dimensions } from 'react-native'

import { LoadingView, GamePreviewFeed, OpenSans } from '../../views/components'

import { layout, colours } from '../../styles'

const screenWidth = Dimensions.get('window').width
const dimensions = {
  img: {
    width: screenWidth,
    height: screenWidth*9/16
  }
}

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class FeedView extends Component {

  componentWillMount() {
    const { fetching } = this.props.state
    if(fetching) this.props.fetchFeed(this.props.state.userId);
  }

  layoutSwitch(layout) {
    this.props.layoutSwitch(layout)
  }

  renderHeader() {
    return <View style={[layout.header, styles.header]}>
        <OpenSans textStyle={styles.headerText}>Feed</OpenSans>
      </View>
  }

  upvoteItem(game) {
    this.props.upvote(game.gameId, this.props.user.user, game.playerIds.split(' '))
  }

  _goToItem(route) {
    this.props._goToItem(route.item.data)
  }

  renderFeed() {
    return this.props.state.items.map((item, i) => {
      const route = {
        type: 'push',
        item: {
          _id: item.gameId,
          data: item
        },
        route: {
          key: 'feed.item'
        }
      }
      const game = JSON.parse(item.data);
      return (
        <GamePreviewFeed
          route={route} size={dimensions.img}
          _handleNavigate={this._goToItem.bind(this)}
          key={item.gameId}
          data={item}
          game={game}
          disabled={true} />
      )
    })
  }

  render () {
    // Check if there are any items in the current state
    // If there are, display them. If not, display a loader and sent an ajax
    // request to populate items.
    const { fetching, fetchData } = this.props.state

    if(fetching) {
      return <View style={layout.container}>
              <StatusBar hidden={true} />
              {this.renderHeader()}
              <LoadingView/>
            </View>
    }

    return <View style={layout.container}>
            <StatusBar hidden={true} />
            {this.renderHeader()}
            <ScrollView
              ref={(scrollView) => { _scrollView = scrollView; }}
              automaticallyAdjustContentInsets={false}
              scrollEventThrottle={200}
              style={layout.content}
              contentContainerStyle={styles.scrollContentContainer}>

                {this.renderFeed()}

            </ScrollView>
          </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }

}


const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
  },
  headerText: {
    color: colours.background.dark
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
});
