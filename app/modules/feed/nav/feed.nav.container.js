import { connect } from 'react-redux'
import FeedNav from './feed.nav.view'
import { push, pop } from './feed.nav.actions'
function mapStateToProps (state) {
  return {
    state: state.feedNavReducer,
    feed: state.feedReducer
  }
}

export default connect(
  mapStateToProps,
  {
    pushRoute: (route) => push(route),
    popRoute: ()=> pop()
  }
)(FeedNav)
