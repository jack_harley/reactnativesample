import React, { Component } from 'react'

import FeedView from '../feed.container'
import GameView from '../../gallery/item/gallery.item.container'
import ProfileView from '../../gallery/item/profile/profile.container'

import {
  BackAndroid,
  NavigationExperimental,
  View
} from 'react-native'

const {
  Reducer: NavigationTabsReducer,
  CardStack: NavigationCardStack
} = NavigationExperimental

class UserRoot extends Component {
  constructor (props) {
    super(props)
    this._renderScene = this._renderScene.bind(this)
    this._handleBackAction = this._handleBackAction.bind(this)
  }
  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this._handleBackAction)
  }
  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this._handleBackAction)
  }
  _renderScene (props) {
    const { route } = props.scene
    let key = route.key
    key = key.indexOf('_') >= 0 ? key.substring(0, key.indexOf('_')) : key

    switch (key) {
      case 'feed':
        return <FeedView
                _handleNavigate={this._handleNavigate.bind(this)}
                _goToItem={this._goToItem.bind(this)}
                state={this.props.state} />

      case 'feed.game':
        return <GameView
                _goToItem={this._goToItem.bind(this)}
                item={route.item}
                state={this.props.state}
                _goBack={this._handleBackAction.bind(this)}
                _goToProfile={this._goToProfile.bind(this)}
                _goToComments={this._goToComments.bind(this)} />

      case 'profile':
        return <ProfileView
                profileId={route.profileId}
                state={this.props.state}
                _goBack={this._handleBackAction.bind(this)}
                _goToItem={this._goToItem.bind(this)} />

      default:
        return <View />
    }
  }
  _goToItem (item) {
    this._handleNavigate({
      type: 'push',
      route: {
        item: item,
        key: 'feed.game'
      }
    });
  }
  _goToProfile (_id) {
    this._handleNavigate({
      type: 'push',
      route: {
        profileId: _id,
        key: 'profile'
      }
    });
  }
  _goToComments () {
    this._handleNavigate({
      type: 'push',
      route: {
        key: 'feed.game.comments'
      }
    });
  }
  _handleBackAction () {
    if (this.props.state.index === 0) {
      return false
    }
    this.props.popRoute()
    return true
  }
  _handleNavigate (action) {
    switch (action && action.type) {
      case 'push':
        this.props.pushRoute(action.route)
        return true
      case 'back':
      case 'pop':
        return this._handleBackAction()
      default:
        return false
    }
  }
  render () {
    return (
      <NavigationCardStack
        style={{flex: 1}}
        navigationState={this.props.state}
        onNavigate={this._handleNavigate.bind(this)}
        renderScene={this._renderScene} />
    )
  }
}

export default UserRoot
