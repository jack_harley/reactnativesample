export function push(route) {
  return {
    type: 'FEED_PUSH_ROUTE',
    route
  }
}

export function pop () {
  return {
    type: 'FEED_POP_ROUTE'
  }
}
