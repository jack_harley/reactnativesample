import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  index: 0,
  key: 'feed.nav',
  routes: [
    {
      key: 'feed'
    }
  ]
}

function feedState (state = initialState, action) {
  switch (action.type) {
    case 'FEED_PUSH_ROUTE':
      if(action.route.key.indexOf('feed.game') >= 0) {
        action.route.key = action.route.key + '_' + state.index
      }
      else if (state.routes[state.index].key === (action.route && action.route.key)) {
        return state
      }
      return NavigationStateUtils.push(state, action.route)
    case 'FEED_POP_ROUTE':
      if (state.index === 0 || state.routes.length === 1) return state
      return NavigationStateUtils.pop(state)
    default:
      return state
  }
}

export default feedState
