import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  key: 'feed.items',
  items: [],
  error: false,
  fetching: true,
  layout: 'rows',
  userId: null
}

function feedItemsState (state = initialState, action) {
  switch (action.type) {
    case 'SET_INITIAL_STATE':
      return {
        ...state,
        userId: (action.state.user == null) ? null : action.state.user._id
      }
    case 'FETCH_FEED_PENDING':
      return {
        ...state,
        fetching: true,
        items: []
      }
    case 'FETCH_FEED_FULFILLED':
      return {
        ...state,
        fetching: false,
        items: action.payload
      }
    case 'FETCH_FEED_REJECTED':
      return {
        fetching: false,
        error: true
      }
    default:
      return state
  }
}

export default feedItemsState
