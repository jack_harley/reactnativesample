import '../../services/api/feed.api'

export function fetchFeed(userId, from) {
  return {
    type: 'FETCH_FEED',
    payload: feed.get(userId, from)
  }
}
