import { connect } from 'react-redux'
import FeedItems from './feed.view'
import { fetchFeed } from './feed.actions'

function mapStateToProps (store) {
  return {
    state: store.feedReducer,
    connectivity: store.connectivityReducer,
  }
}

export default connect(
  mapStateToProps,
  {
    fetchFeed: (userId, from) => fetchFeed(userId, from)
  }
)(FeedItems)
