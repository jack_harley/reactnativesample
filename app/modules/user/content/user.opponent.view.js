import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'

import { Button } from '../../../views/components'
/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/

export default OpponentView = ({_goBack}) =>  (
  <View>
    <Text>About</Text>
    <Button onPress={_goBack} label='Go Back' />
  </View>
)
