import { User } from '../../../services/api'

export function fetchUserGames(userId) {
  return {
    type: 'FETCH_USER_GAMES',
    payload: User.games(userId)
  }
}

export function fetchUserOpponents(userId) {
  return {
    type: 'FETCH_USER_OPPONENTS',
    payload: User.opponents(userId)
  }
}

export function fetchUserNotifications(userId) {
  return {
    type: 'FETCH_USER_NOTIFICATIONS',
    payload: User.notifications(userId)
  }
}

export function changeTab(index) {
  return {
    type: 'USER_CHANGE_TAB',
    index
  }
}

export function loadGameIntoView(item) {
  return {
    type: 'CHANGE_CURRENT_ITEM',
    item
  }
}
