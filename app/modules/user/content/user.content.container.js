import { connect } from 'react-redux'
import UserGames from './user.content.view'
import { fetchUserGames, fetchUserOpponents, fetchUserNotifications, changeTab, loadGameIntoView } from './user.content.actions'
function mapStateToProps (state) {
  return {
    state: state.userContentReducer
  }
}

export default connect(
  mapStateToProps,
  {
    fetchUserGames: (userId) => fetchUserGames(userId),
    fetchUserOpponents: (userId) => fetchUserOpponents(userId),
    fetchUserNotifications: (userId) => fetchUserNotifications(userId),
    changeTab: (index) => changeTab(index),
    loadGameIntoView: (item) => loadGameIntoView(item),
  }
)(UserGames)
