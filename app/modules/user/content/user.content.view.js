import React, { Component } from 'react'
import { View, TouchableOpacity, StyleSheet, Text, Image, ListView, ScrollView } from 'react-native'

import { layout, colours } from '../../../styles'

import UserGames from './partials/user.games.view'
import { LoadingView, UserListItem, Button } from '../../../views/components'

// import UserOpponents from './partials/user.opponents.view'
import { formatDate } from '../../../services/utils'

export default class UserView extends Component {

  componentDidMount() {
    if(this.props.state.fetching) this.props.fetchUserGames(this.props.userId);
  }

  _goToOpponent() {
    this.props._handleNavigate({
      type: 'push',
      route: {
        key: 'user.opponent'
      }
    });
  }

  _goToGame(route) {
    this.props._goToItem(route.route.item)
  }

  _renderSingleNotif(notification, textTopSingle) {
    const dateStr = formatDate(notification.createdAt)
    return <View>
      <View><Image source={notification.thumb} /></View>
      <View>
        <Text>{textTopSingle}</Text>
        <Text>{notification.type} {dateStr}</Text>
      </View>
    </View>
  }

  _renderNotifRow(notification) {
    let textTopSingle = '';

    switch (notification.type) {
      case 'followed':
        if(notification.count > 1) {
          textTopSingle = notification.data.userName + ' and ' + (notification.count - 1) +' others stared following you'
        } else {
          textTopSingle = notification.data.userName + ' started following you'
        }
        break;
      case 'upvote':
        if(notification.count > 1) {
          textTopSingle = notification.data.userName + ' and ' + notification.count +' others liked your ' + notification.data.subject
        } else {
          textTopSingle = notification.data.userName + ' and ' + notification.count +' others liked your ' + notification.data.subject
        }
        break;
      default:
      return <View><Text>UNO - Unidentified Notification Object</Text></View>
    }
    const content = this._renderSingleNotif(notification, textTopSingle)
    return content
  }

  currentContentView() {
    if(this.props.state.fetching) {
      return <LoadingView />
    }
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    switch (this.props.state.index) {
      case 0:
        return <UserGames items={this.props.state.games} _handleNavigate={this._goToGame.bind(this)} />
      case 1:
        return <ListView dataSource={ds.cloneWithRows(this.props.state.opponents)} renderRow={this._renderOpponentRow.bind(this)} enableEmptySections />
      case 2:
        return <View>
          <View>
            <View><ListView dataSource={ds.cloneWithRows(this.props.state.notifications)} renderRow={this._renderNotifRow.bind(this)} enableEmptySections /></View>
          </View>
        </View>
      default:
        return <View />
    }
  }

  changeTab(i) {
    // start fetching
    this.props.changeTab(i)
    switch (i) {
      case 0:
        this.props.fetchUserGames(this.props.userId)
        break
      case 1:
        this.props.fetchUserOpponents(this.props.userId)
        break
      case 2:
        this.props.fetchUserNotifications(this.props.userId)
        break
      default:
        break
    }
  }

  render() {

    const content = this.currentContentView()
// <TouchableOpacity onPress={ () => this.changeTab(1) }><Text>Opponents</Text></TouchableOpacity>
    return <View style={layout.container}>
      <View style={styles.tabWrap}>
        <TouchableOpacity onPress={ () => this.changeTab(0) }><View><Text>Games</Text></View></TouchableOpacity>
        <TouchableOpacity onPress={ () => this.changeTab(2) }><View><Text>Notifications</Text></View></TouchableOpacity>
      </View>
      <ScrollView
        ref={(scrollView) => { this._scrollView = scrollView; }}
        automaticallyAdjustContentInsets={false}
        scrollEventThrottle={200}
        style={layout.container}
        contentContainerStyle={[styles.scrollContentContainer, layout.container]}>
          {content}
        </ScrollView>
    </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }

}

const styles = StyleSheet.create({
  tabWrap: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: colours.greyscale.light,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  }
});
