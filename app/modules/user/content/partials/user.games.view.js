import _chunk from 'lodash/chunk';
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, StatusBar, Text, Image, Dimensions } from 'react-native'

import { LoadingView, GamePreviewGrid } from '../../../../views/components'


const dimensions = {
  width: Dimensions.get('window').width/3,
  height: Dimensions.get('window').width/3
}

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class UserContentView extends Component {

  renderRow(items) {
    return items.map((item, i) => {
      return (
        <GamePreviewGrid route='user.game' size={dimensions} _handleNavigate={this.props._handleNavigate.bind(this)} key={item.gameId} data={item} />
      )
    })
  }

  renderItemsInGroupsOf(count) {
    return _chunk(this.props.items, count).map((itemsForRow, i) => {
      return (
        <View key={i} style={styles.itemRow}>
          {this.renderRow(itemsForRow)}
        </View>
      )
    })
  }

  render () {
    // Check if there are any items in the current state
    // If there are, display them. If not, display a loader and sent an ajax
    // request to populate items.
    // const { items } = this.props.state
    const { items } = this.props

    if(!items.length) {
      return <View style={styles.center}>
               <Text>No games</Text>
             </View>
    }

    return <View style={styles.container}>
            {this.renderItemsInGroupsOf(3)}
           </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 40
  },
  scrollWrap: {
    marginTop: 50,
  },
  sortViewLoading: {
    marginTop: 50
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
});
