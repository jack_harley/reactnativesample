import React, { Component } from 'react'
import { View, StyleSheet, StatusBar, Text, Image } from 'react-native'

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class UserContentView extends Component {

  render () {
    return <View style={[styles.container, this.border('blue')]}>
            {this.renderOpponent}
           </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 40
  },
  scrollWrap: {
    marginTop: 50,
  },
  sortViewLoading: {
    marginTop: 50
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  }
});
