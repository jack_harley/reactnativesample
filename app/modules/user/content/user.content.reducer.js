const initialState = {
  index: 0,
  fetching: true,
  error: null,
  games: [],
  notifications: [],
  opponents: []
}

function userState (state = initialState, action) {
  switch (action.type) {
    // Notifications
    case 'FETCH_USER_NOTIFICATIONS_PENDING':
      if(state.fetching) return state
      return {
        ...state,
        fetching: true
      }
    case 'FETCH_USER_NOTIFICATIONS_REJECTED':
      return {
        ...state,
        fetching: false,
        error: action.payload
      }
    case 'FETCH_USER_NOTIFICATIONS_FULFILLED':
      return {
        ...state,
        fetching: false,
        notifications: action.payload
      }
    // Games
    case 'FETCH_USER_GAMES_PENDING':
      if(state.fetching) return state
      return {
        ...state,
        fetching: true
      }
    case 'FETCH_USER_GAMES_REJECTED':
      return {
        ...state,
        fetching: false,
        error: action.payload
      }
    case 'FETCH_USER_GAMES_FULFILLED':
      return {
        ...state,
        fetching: false,
        games: action.payload
      }
    // Opponents
    case 'FETCH_USER_OPPONENTS_PENDING':
      if(state.fetching) return state
      return {
        ...state,
        fetching: true
      }
    case 'FETCH_USER_OPPONENTS_REJECTED':
      return {
        ...state,
        fetching: false,
        error: action.payload
      }
    case 'FETCH_USER_OPPONENTS_FULFILLED':
      return {
        ...state,
        fetching: false,
        opponents: action.payload
      }
    case 'USER_CHANGE_TAB':
      if(action.index === state.index) return state
      return {
        ...state,
        index: action.index
      }
    default:
      return state
  }
}

export default userState
