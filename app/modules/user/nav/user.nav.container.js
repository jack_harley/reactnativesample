import { connect } from 'react-redux'
import UserNav from './user.nav.view'
import { push, pop } from './user.nav.actions'
function mapStateToProps (state) {
  return {
    state: state.userNavReducer,
    user: state.userReducer
  }
}

export default connect(
  mapStateToProps,
  {
    pushRoute: (route) => push(route),
    popRoute: ()=> pop()
  }
)(UserNav)
