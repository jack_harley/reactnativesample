import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  index: 0,
  key: 'user.nav',
  routes: [
    {
      key: 'user'
    }
  ]
}

function galleryState (state = initialState, action) {
  switch (action.type) {
    case 'USER_PUSH_ROUTE':
      let newState = {...state}
      if(action.route.key.indexOf('user.game') >= 0) {
        action.route.key = action.route.key + '_' + state.index
      }
      else if (state.routes[state.index].key === (action.route && action.route.key)) {
        return state
      }
      return NavigationStateUtils.push(state, action.route)
    case 'USER_POP_ROUTE':
      if (state.index === 0 || state.routes.length === 1) return state
      return NavigationStateUtils.pop(state)
    default:
      return state
  }
}

export default galleryState
