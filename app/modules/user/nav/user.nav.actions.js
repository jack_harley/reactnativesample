export function push(route) {
  return {
    type: 'USER_PUSH_ROUTE',
    route
  }
}

export function pop () {
  return {
    type: 'USER_POP_ROUTE'
  }
}
