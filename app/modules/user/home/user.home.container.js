import { connect } from 'react-redux'
import UserView from './user.home.view'
import {
  toggleUserEdit,
  getFollowers,
  getFollowing,
  changeModalView,
  closeUserModal,
  unfollowUser,
  followUser
} from './user.home.actions'

function mapStateToProps (state) {
  return {
    state: state.userReducer
  }
}

export default connect(
  mapStateToProps,
  {
    fetchUser: () => fetchUser(),
    changeModalView: (view) => changeModalView(view),
    closeUserModal: () => closeUserModal(),
    getFollowers: (userId) => getFollowers(userId),
    getFollowing: (userId) => getFollowing(userId),
    unfollowUser: (userId, followingId) => unfollowUser(userId, followingId),
    followUser: (userId, followId, payload) => followUser(userId, followId, payload)
  }
)(UserView)
