import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

import FBSDK from 'react-native-fbsdk'
const { LoginButton, AccessToken } = FBSDK;

const Login = ({userLogin}) => (
  <View style={styles.container}>
    <LoginButton
      readPermissions={["user_friends"]}
      onLoginFinished={
        (error, result) => {
          if (error) {
            alert("Login failed with error: " + result.error)
          } else if (result.isCancelled) {
            alert("Login was cancelled")
          } else {
            AccessToken.getCurrentAccessToken().then(
              (data) => {
                userLogin(data)
              }
            )
          }
        }
      }
      onLogoutFinished={() => alert("User logged out")}/>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: 'white',
    marginTop: 100,
    fontSize: 24,
    textAlign: 'center'
  },
  image: {
    width: 250,
    height: 250
  }
})

export default Login
