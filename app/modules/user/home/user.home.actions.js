import { User } from '../../../services/api'

export function closeUserModal() {
  return {
    type: 'CLOSE_USER_MODAL'
  }
}

export function getFollowers(userId) {
  return {
    type: 'GET_FOLLOWERS',
    payload: User.followers(userId)
  }
}

export function getFollowing(userId) {
  return {
    type: 'GET_FOLLOWING',
    payload: User.following(userId)
  }
}

export function changeModalView(view) {
  return {
    type: 'CHANGE_MODAL_VIEW',
    view: view
  }
}

export function userLogin(accessData) {
  return {
    type: 'USER_LOGIN',
    payload: User.login(accessData)
  }
}

export function followUser(userId, followId, payload) {
  return {
    type: 'FOLLOW_USER',
    followId: followId,
    payload: User.follow(userId, followId, payload)
  }
}

export function unfollowUser(userId, followingId) {
  return {
    type: 'UNFOLLOW_USER',
    payload: User.unfollow(userId, followingId)
  }
}
