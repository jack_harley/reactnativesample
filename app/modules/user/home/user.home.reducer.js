import _remove from 'lodash/remove'
import _clone from 'lodash/clone'

let friends = [],
    followers = []

const initialState = {
  showModal: false,
  loggedIn: false,
  loggingIn: false,
  user: {},
  newUser: false,
  friends: [],
  triggerSetInitialState: false,

  followers: [],
  followersLoading: false,
  followersLoaded: false,
  followersError: false,

  following: [],
  followingLoading: false,
  followingLoaded: false,
  followingError: false,
}

function userState (state = initialState, action) {
  switch (action.type) {
    case 'SET_INITIAL_STATE':
      if(!action.state) return state
      return {
        ...state,
        loggingIn: false,
        triggerSetInitialState: false,
        loggedIn: (action.state.user && action.state.user._id) ? true : false,
        user: action.state.user ? action.state.user : null
      }
    case 'CHANGE_MODAL_VIEW':
      return {
        ...state,
        modalView: action.view,
        showModal: true
      }
    case 'CLOSE_USER_MODAL':
      return {
        ...state,
        showModal: false
      }
    case 'USER_LOGIN_PENDING':
      return {
        ...state,
        loggedIn: false,
        loggingIn: true
      }
    case 'USER_LOGIN_REJECTED':
      return {
        ...state,
        loggedIn: false,
        loggingIn: false
      }
    case 'USER_LOGIN_FULFILLED':
      // TODO: go to welcome page :)
      return {
        ...state,
        loggedIn: true,
        loggingIn: false,
        triggerSetInitialState: true,
        newUser: action.payload.newUser
      }
    case 'GET_FOLLOWING_PENDING':
      return {
        ...state,
        followingLoading: true,
        showModal: true,
        modalView: 'following'
      }
    case 'GET_FOLLOWING_REJECTED':
      return {
        ...state,
        followingLoaded: true,
        followingError: true
      }
    case 'GET_FOLLOWING_FULFILLED':
      return {
        ...state,
        followingLoaded: true,
        followingLoading: false,
        following: action.payload
      }
    case 'GET_FOLLOWERS_PENDING':
      return {
        ...state,
        followersLoading: true,
        showModal: true,
        modalView: 'followers'
      }
    case 'GET_FOLLOWERS_REJECTED':
      return {
        ...state,
        followersLoaded: true,
        followersError: true
      }
    case 'GET_FOLLOWERS_FULFILLED':
      return {
        ...state,
        followersLoaded: true,
        followersLoading: false,
        followers: action.payload,
      }
    case 'FOLLOW_USER_PENDING':
      return {
        ...state,
        followingLoading: true
      }
    case 'FOLLOW_USER_REJECTED':
      return {
        ...state,
        followingLoading: false
      }
    case 'FOLLOW_USER_FULFILLED':
      following = _clone(state.following)
      following.push(action.payload)
      return {
        ...state,
        following,
        followingLoading: false,
        user: {
          ...state.user,
          followingCount: state.user.followingCount + 1
        }
      }
    case 'UNFOLLOW_USER_PENDING':
      return {
        ...state,
        followingLoading: true
      }
    case 'UNFOLLOW_USER_REJECTED':
      return {
        ...state,
        followingLoading: false
      }
    case 'UNFOLLOW_USER_FULFILLED':
      following = _remove(state.following, (n) => {
        return n.followingId != action.payload
      })
      return {
        ...state,
        following,
        followingLoading: false,
        user: {
          ...state.user,
          followingCount: state.user.followingCount - 1
        }
      }
    case 'ACCEPT_CHALLENGE':
    case 'REJECT_CHALLENGE':
      return {
        ...state,
        user: {
          ...state.user,
          challengeCount: state.user.notificationCount - 1
        }
      }
    default:
      return state
  }
}

export default userState
