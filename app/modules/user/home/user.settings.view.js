import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, TouchableHighlight } from 'react-native'
import { layout, colours } from '../../../styles'

import { Button } from '../../../views/components'
/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/

export default SettingsView = ({closeUserModal}) =>  (
  <View>
   <View style={[styles.head]}>
    <TouchableHighlight onPress={ () => {closeUserModal()} }>
      <View>
        <Text>CANCEL</Text>
      </View>
    </TouchableHighlight>
    <View><Text>EDIT PROFILE</Text></View>
    <TouchableHighlight><View><Text>DONE</Text></View></TouchableHighlight>
   </View>

   <View style={[layout.container]}>
    <View style={[styles.changeAvatarWrap]}>
      <Image style={styles.avatar} source={user.avatar} />
      <TouchableHighlight>
        <View>
          <Text>Change Profile Picture</Text>
        </View>
      </TouchableHighlight>
    </View>
    <View style={[styles.details]}>
      <View>
        <View></View>
        <View></View>
        <View></View>
      </View>
    </View>
   </View>
 </View>
)

const styles = StyleSheet.create({
  changeAvatarWrap: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    height: 100,
    width: 100,
    marginBottom: 20,
    borderRadius: 50
  },
  details: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'stretch',
  }
});
