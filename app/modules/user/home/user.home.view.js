import _pick from 'lodash/pick'

import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, StatusBar, Text, TouchableOpacity, Image, Modal } from 'react-native'
import UserContent from '../content/user.content.container.js'
import FollowView from '../follow/user.follow.view'
import SettingsView from './user.settings.view'
import { LoadingView, OpenSans } from '../../../views/components'

import { layout, colours } from '../../../styles'

export default class UserView extends Component {

  goTo(key) {
    this.props._handleNavigate({
      type: 'push',
      route: {
        key: key
      }
    });
  }

  followUser(userId, followId, payload) {
    payload.user = _pick(this.props.state.user, ['firstName', 'lastName', 'avatar'])
    this.props.followUser(userId, followId, payload);
  }

  _renderLoadingModal() {
    return <View>
      <View style={[layout.head]}>
       <TouchableOpacity onPress={ () => {closeUserModal()} }>
         <View>
           <Text>CANCEL</Text>
         </View>
       </TouchableOpacity>
      </View>
      <View>
      <LoadingView/>
      </View>
    </View>
  }

  _renderModalContent(key) {
    switch (key) {
      case 'settings':
        return <SettingsView closeUserModal={this.props.closeUserModal}></SettingsView>
      case 'followers':
        if(this.props.state.followingLoading) return this._renderLoadingModal()
        return <FollowView type="followers" followUser={ this.followUser.bind(this) } closeUserModal={this.props.closeUserModal} items={this.props.state.followers} showActions={false}></FollowView>
      case 'following':
        if(this.props.state.followingLoading) return this._renderLoadingModal()
        return <FollowView type="following" unfollowUser={this.props.unfollowUser} closeUserModal={this.props.closeUserModal} items={this.props.state.following} showActions={true}></FollowView>
      default:
        return <View><Text>Oops! Something went wrong</Text></View>
    }
  }

  // <TouchableOpacity onPress={() => this.goTo('user.settings')}>
  //   <View>
  //     <Text>Settings</Text>
  //   </View>
  // </TouchableOpacity>

  render() {
    const { user } = this.props.state
    return <View style={layout.container}>
      <StatusBar hidden={true} />
      <View style={layout.header}>
        <View style={{flex: 3}}>
          <OpenSans textStyle={{textAlign: 'center'}}>Profile</OpenSans>
        </View>
      </View>
      <View style={[layout.content]} >

        <View style={styles.profileWrap}>
          <View style={styles.profileDetails}>
            <Image style={styles.profileAvatar} source={user.avatar.small} />
            <Text style={styles.textName}>{user.firstName} {user.lastName}</Text>
          </View>
          <View style={styles.profileStats}>
            <View style={styles.profileNumbers}>
              <View style={styles.statSingle}>
                <Text style={styles.textNumber}>{user.gameCount}</Text>
                <Text style={styles.textNumberTag}>games</Text>
              </View>
              <View>
                <TouchableOpacity onPress={() => this.props.getFollowers(this.props.state.user._id)}>
                  <View style={styles.statSingle}>
                    <Text style={styles.textNumber}>{user.followersCount}</Text>
                    <Text style={styles.textNumberTag}>followers</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity onPress={() => this.props.getFollowing(this.props.state.user._id)}>
                  <View style={styles.statSingle}>
                    <Text style={styles.textNumber}>{user.followingCount}</Text>
                    <Text style={styles.textNumberTag}>following</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.contentWrap}>
          <UserContent userId={user._id} _goToItem={this.props._goToItem.bind(this)} />
        </View>
      </View>

      <Modal
        animationType={"slide"}
        transparent={false}
        visible={this.props.state.showModal}
        onRequestClose={() => {alert("Modal has been closed.")}}
        >

      {this._renderModalContent(this.props.state.modalView)}

      </Modal>

    </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }
}

const styles = StyleSheet.create({
  sortViewLoading: {
    marginTop: 50
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  profileWrap: {
    flex: 1,
    flexDirection: 'row'
  },
  contentWrap: {
    flex: 3,
    flexDirection: 'row',
    marginBottom: 10
  },
  profileDetails: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 3
  },
  profileStats: {
    justifyContent: 'center',
    alignItems: 'stretch',
    flex: 5
  },
  profileAvatar: {
    height: 80,
    borderRadius: 40,
    width: 80,
    marginBottom: 5
  },
  profileNumbers: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 60,
    marginBottom: 5,
    marginRight: 30
  },
  statSingle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textNumber: {
    fontSize: 15,
    fontWeight: '600',
    color: colours.background.dark
  },
  textNumberTag: {
    fontSize: 12,
    fontWeight: '500',
    color: colours.greyscale.medium
  },
  textName: {
    fontSize: 12,
    fontWeight: '600',
    color: colours.greyscale.dark
  },
  profileEdit: {
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
    backgroundColor: colours.background.light
  },
  // edit profile
  head: {
    height: 40,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row'
  },
  changeAvatarWrap: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    height: 100,
    width: 100,
    marginBottom: 20,
    borderRadius: 50
  },
  details: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'stretch',
  }
});
