import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, ScrollView, TouchableOpacity, ListView, StatusBar } from 'react-native'
import { layout, colours } from '../../../styles'
import { LoadingView } from '../../../views/components'

import Icon from 'react-native-vector-icons/FontAwesome'

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class FollowingView extends Component {

  _renderHeaderType(type) {
    if(type === 'followers') {
      return <Text textStyle={{textAlign: 'center'}}>Followers</Text>
    } else {
      <Text textStyle={{textAlign: 'center'}}>Following</Text>
    }
  }

  _renderHeader() {
    return (
      <View style={layout.header}>
        <View style={{flex: 1}}>
          <Icon.Button
            name="angle-left"
            onPress={ () => {this.props.closeUserModal()} }
            backgroundColor="transparent"
            style={{padding:5, borderRadius: 0}}
            size={28}
            color={colours.background.dark}
            iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
          </Icon.Button>
        </View>
        <View style={{flex: 3}}>
          { this._renderHeaderType(this.props.type) }
        </View>
        <View style={{flex: 1}}></View>
      </View>
    )
  }

  button(item) {
    if(this.props.type === 'following') {
      return this._renderUnfollowButton(item)
    } else {
      return this._renderFollowButton(item)
    }
  }

  _renderUnfollowButton(item) {
    if(!this.props.showActions) return <View></View>
    return <View style={styles.unfollowButton}>
      <TouchableOpacity onPress={ () => { this.props.unfollowUser(item.userId, item.followingId) } }>
        <View>
          <Text>Unfollow</Text>
        </View>
      </TouchableOpacity>
    </View>
  }
  _renderFollowButton(item) {
    const payload = {
      user: {},
      following: item.user
    }
    if(!this.props.showActions) return
    return <View style={styles.unfollowButton}>
      <TouchableOpacity onPress={ () => { this.props.followUser(item.userId, item.followerId, payload) } }>
        <View>
          <Text>Follow</Text>
        </View>
      </TouchableOpacity>
    </View>
  }
  _renderFollowerRow(item) {
    const follower = item.user
    return <View style={styles.row}>
      <View style={styles.imgWrap}>
        <Image style={styles.img} source={follower.avatar.small}/>
      </View>
      <View style={styles.details}>
        <Text>{follower.firstName} {follower.lastName}</Text>
      </View>
      {this.button(item)}
    </View>
  }
  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    return <View>
      <StatusBar hidden={true} />
      {this._renderHeader()}
      <ScrollView style={styles.body}>
        <ListView dataSource={ds.cloneWithRows(this.props.items)} renderRow={this._renderFollowerRow.bind(this)} enableEmptySections />
      </ScrollView>
    </View>
  }
}



const styles = StyleSheet.create({
  body: {
    marginTop: 40,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    padding: 5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  imgWrap: {
    flex: 3
  },
  img: {
    height: 40,
    width: 40,
    borderRadius: 20
  },
  details: {
    justifyContent: 'center',
    flex: 10
  },
  unfollowButton: {
    flex: 4,
    justifyContent: 'center',
  }
});
