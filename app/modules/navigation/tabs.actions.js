import { Game } from '../../services/api'

export function changeTab (index) {
  return {
    type: 'CHANGE_TAB',
    index
  }
}

export function setInitialState(state) {
  return {
    type: 'SET_INITIAL_STATE',
    state
  }
}

export function goToChallenges() {
  return {
    type: 'GO_TO_CHALLENGES'
  }
}

export function goToGalleryItem(gameId, userId) {
  return {
    type: 'GO_TO_GALLERY',
    payload: Gallery.getById(gameId, userId)
  }
}

export function goToGame(gameId, userId) {
  return {
    type: 'GO_TO_GAME',
    payload: Game.getById(gameId, userId)
  }
}

export function hideMenuBar() {
  return {
    type: 'HIDE_MENU'
  }
}

export function showMenuBar() {
  return {
    type: 'SHOW_MENU'
  }
}
