import { connect } from 'react-redux'
// import layout
import TabsRoot from './tabs.view'
// import action
import { changeTab, setInitialState, goToChallenges, goToGame, goToGalleryItem } from './tabs.actions'
import { push as pushGame } from '../games/nav/games.nav.actions'
import { push as pushGalleryItem } from '../gallery/nav/gallery.nav.actions'
import { userLogin } from '../user/home/user.home.actions'
import { connectivityChange } from '../connectivity/connectivity.actions'

function mapStateToProps(store) {
  return {
    tabs: store.tabReducer,
    user: store.userReducer,
    uploader: store.uploaderReducer,
    connectivity: store.connectivityReducer
  }
}

export default connect(
  mapStateToProps,
  {
    changeTab: (route) => changeTab(route),
    userLogin: (accessData) => userLogin(accessData),
    goToGame: (gameId, userId) => goToGame(gameId, userId),
    pushGame: (route) => pushGame(route),
    goToGalleryItem: (gameId, userId) => goToGalleryItem(gameId, userId),
    pushGalleryItem: (route) => pushGalleryItem(route),
    goToChallenges: () => goToChallenges(),
    setInitialState: (state) => setInitialState(state),
    connectivityChange: (status, type) => connectivityChange(status, type)
  }
)(TabsRoot)
