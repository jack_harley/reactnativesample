import React, { Component } from 'react'
import { View, NavigationExperimental, TouchableOpacity, StyleSheet, StatusBar, Text, NetInfo } from 'react-native'
import { storageGet } from '../../services/storage'
import { User } from '../../services/api'
import { Uploader, LoadingView } from '../../views/components'
import Tabbar from 'react-native-tabbar'
import Orientation from 'react-native-orientation'
import Icon from 'react-native-vector-icons/FontAwesome'
import PushController from '../notifications/pushNotifications'

import { colours } from '../../styles'

// const { Reducer: NavigationReducer } = NavigationExperimental

import FeedView  from '../feed/nav/feed.nav.container'
import Gallery   from '../gallery/nav/gallery.nav.container'
import GamesView from '../games/nav/games.nav.container'
import UserView  from '../user/nav/user.nav.container'
import Login     from '../user/home/login/user.login.view'

export default class Tabs extends Component {
  onConnectivityChange(type) {
    const status = type == 'NONE' ? false : true
    this.props.connectivityChange(status, type)
  }

  getUser() {
    const { setInitialState } = this.props
    let initialState = {}
    storageGet().then((state) => {
      initialState = state ? state : {}
      if(state == null || state.user == null) return Promise.resolve(null)
      return User.get(state.user._id)
    }).then((user) => {
      initialState.user = user
      setInitialState(initialState)
    })
    .done()
  }

  componentDidUpdate() {
    if(this.props.user.triggerSetInitialState) this.getUser()
  }
  componentDidMount() {

    Orientation.lockToPortrait()

    this.getUser()

    NetInfo.fetch().done((type) => {
      this.onConnectivityChange(type)
      NetInfo.addEventListener('change', (type) => {this.onConnectivityChange(type)} )
    });

    // storageGet().then((state) => {
    //   setInitialState(state);
    // }).done();
  }
  // componentWillUnmount() {
  //   Orientation.removeOrientationListener(this.onOrientationChange);
  // }
  _changeTab(i) {
    const { changeTab } = this.props
    changeTab(i)
  }
  _userLogin(accessData) {
    this.props.userLogin(accessData)
  }
  _goToChallenges() {
    this.props.goToChallenges()
  }
  _goToGame(gameId) {
    this.props.goToGame(gameId, this.props.user.user._id)
      .then((game) => {
        this.props.pushGame({
          key: 'timedDuel',
          game: { server: game.value.data, local: undefined }
        })
      })
  }
  _goToGalleryItem(gameId) {
    this.props.goToGalleryItem(gameId, this.props.user.user._id)
      .then((item) => {
        this.props.pushGalleryItem({
          key: 'gallery.item',
          item: item.value
        })
      })
  }
  _renderTabContent() {
    const { key } = this.props.tabs
    const { user, changeTab } = this.props
    switch (key) {
      case 'feed':
        return <FeedView />
      case 'gallery':
        return <Gallery />
      case 'games':
        return <GamesView changeTab={ changeTab } />
      case 'user':
        return <UserView />
      default:
        return <View />
    }
  }
  render () {

    const { user } = this.props

    if( this.props.tabs.loading || (!user.loggedIn && user.loggingIn) ) return <LoadingView/>
    if(!user.loggedIn) return <Login userLogin={ this._userLogin.bind(this) }/>

    console.log('Uploader');
    if(this.props.uploader.visible) return <Uploader />

    let tabColour =  { backgroundColor: colours.background.light }
    let borderColour = { borderTopColor: colours.greyscale.light }
    let iconColour = colours.greyscale.dark

    if( this.props.tabs.key == 'games' ) {
      tabColour = { backgroundColor: colours.background.dark }
      borderColour = { borderTopColor: colours.breakLines.dark }
      iconColour = colours.greyscale.light
    }

    const tabs = this.props.tabs.routes.map((tab, i) => {
      const color = this.props.tabs.key == tab.key ? iconColour : colours.greyscale.medium
      return (
        <TouchableOpacity style={styles.tabItem} key={i} onPress={() => this.props.changeTab(i)}>
          <View>
            <Icon
              name={tab.icon}
              backgroundColor="transparent"
              size={24}
              color={color}
              iconStyle={{backgroundColor: 'transparent'}} >
            </Icon>
          </View>
        </TouchableOpacity>
      )
    })
    return (
      <View style={{flex:1}}>
        <StatusBar hidden={true} />
        <PushController
          user={ this.props.user.user }
          _goToChallenges={ this._goToChallenges.bind(this) }
          _goToGame={ this._goToGame.bind(this) }
          _goToGalleryItem={ this._goToGalleryItem.bind(this) } />

        <View style={[{flex:1}]}>
          {this._renderTabContent()}
        </View>

        { this.props.tabs.showMenuBar &&
          <View style={[styles.tabbar, tabColour]}>
            <View style={[styles.tabWrap, borderColour]}>
              {tabs}
            </View>
          </View>
        }

      </View>
    )
  }

  border(color) {
    return {
      borderWidth: 1,
      borderColor: color
    }
  }
}

const styles = StyleSheet.create({
  tabWrap: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: StyleSheet.hairlineWidth,
    zIndex: 1
  },
  tabItem: {
    borderRadius: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabbar: {
    height: 50,
    zIndex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0
  }
});
