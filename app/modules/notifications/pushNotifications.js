import React, { Component } from 'react'
import { TouchableOpacity, View, Text, StyleSheet, DeviceEventEmitter, AppState } from 'react-native'
import { User } from '../../services/api'
import PushNotification from 'react-native-push-notification'
import { colours } from '../../styles'
import _filter from 'lodash/filter'
import _map from 'lodash/map'

export default class PushController extends Component {

  constructor(props) {
    super(props)

    this.state = {
      notifications: []
    }

  }


  componentDidMount() {
    const { _id, pushEnabled } = this.props.user
    PushNotification.configure({

      onRegister: function(token) {
        switch (token.os) {
          case 'android':
            if(!pushEnabled) User.enablePushNotifications(_id, token.token)
            break;
          default:
        }
      },

      onNotification: (notification) => {
        console.log('notification', notification);
        let that = this;
        if(!notification.userInteraction && AppState.currentState == 'active') {
          this.setState({notifications: this.state.notifications.concat([notification])})
          // setTimeout(function() {
          //   this.id = notification.id
          //   that.setState({
          //     notifications: _filter(that.state.notifications, (o) => {
          //       return o.id != this.id
          //     })
          //   })
          // }, 5000)
        } else if(notification.userInteraction) {
          this.notificationActionHandler(notification.data)
        }
      },

      senderID: '5264602671',

      permissions: {
          alert: true,
          badge: true,
          sound: true
      },

      popInitialNotification: false,

      requestPermissions: true,
    });

  }

  notificationActionHandler(notif) {
    console.log('notificationActionHandler', notif);

    const remainingNotifs = _filter(this.state.notifications, (o) => {
      return o.id != notif.id
    })

    this.setState({
      notifications: remainingNotifs
    })

    switch (notif.type) {
      case 'challenge':
        this.props._goToChallenges()
        break
      case 'game':
        this.props._goToGame(notif.gameId)
        break
      case 'gallery.item':
        this.props._goToGalleryItem(notif.gameId)
        break
    }

  }

  _renderNotifications(notifications) {
    return _map(notifications, (notification) => {
      console.log(notification);
      return <TouchableOpacity onPress={ () => { this.notificationActionHandler(notification.data) } } key={notification.id} style={styles.container}>
        <Text style={styles.text}>{notification.message}</Text>
      </TouchableOpacity>
    })
  }

  render() {
    if(!this.state.notifications.length) return null
    return <View>{this._renderNotifications(this.state.notifications)}</View>
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 8,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: colours.scheme.primary,
    zIndex: 9999999,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 10,
    color: 'white'
  }
})
