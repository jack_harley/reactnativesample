import React, { Component } from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'

import { Button } from '../../../../views/components'

const GalleryCommentView = ({_goBack}) =>  (
  <View>
    <Text>About</Text>
    <Button onPress={_goBack} label='Go Back' />
  </View>
)

export default GalleryCommentView
