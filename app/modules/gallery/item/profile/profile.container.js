import { connect } from 'react-redux'
import ProfileItem from './profile.view'
import { fetchProfile, followProfile, unfollowProfile } from './profile.actions'
import { unfollowUser, followUser } from '../../../user/home/user.home.actions'
function mapStateToProps (state) {
  return {
    state: state.profileReducer
  }
}

export default connect(
  mapStateToProps,
  {
    fetchProfile: (id) => fetchProfile(id),
    unfollowUser: (userId, followingId) => unfollowUser(userId, followingId),
    followUser: (userId, followId, payload) => followUser(userId, followId, payload)
  }
)(ProfileItem)
