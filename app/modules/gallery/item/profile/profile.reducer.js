import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  profile: {},
  error: null,
  fetching: true,
  followLoad: false,
  userId: null
}

function profileItemState (state = initialState, action) {
  switch (action.type) {
    case 'SET_INITIAL_STATE':
      return {
        ...state,
        user: action.state.user ? action.state.user : null
      }
    case 'FETCH_PROFILE_PENDING':
      return {
        ...state,
        fetching: true,
        profile: {}
      }
    case 'FETCH_PROFILE_FULFILLED':
      return {
        ...state,
        fetching: false,
        profile: action.payload
      }
    case 'FETCH_PROFILE_REJECTED':
      return {
        fetching: false,
        error: true
      }
    case 'FOLLOW_USER_PENDING':
      return {
        ...state,
        followLoad: true
      }
    case 'FOLLOW_USER_FULFILLED':
      return {
        ...state,
        followLoad: false,
        profile: {
          ...state.profile,
          isFollowing: true,
          followersCount: state.profile.followersCount + 1
        }
      }
    case 'FOLLOW_USER_REJECTED':
      return {
        followLoad: false,
        error: true
      }
    case 'UNFOLLOW_USER_PENDING':
      return {
        ...state,
        followLoad: true
      }
    case 'UNFOLLOW_USER_FULFILLED':
      return {
        ...state,
        followLoad: false,
        profile: {
          ...state.profile,
          isFollowing: false,
          followersCount: state.profile.followersCount - 1
        }
      }
    case 'UNFOLLOW_USER_REJECTED':
      return {
        followLoad: false,
        error: true
      }
    default:
      return state
  }
}

export default profileItemState
