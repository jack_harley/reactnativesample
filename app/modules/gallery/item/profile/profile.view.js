const _ = require('lodash')
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, StatusBar, Text, TouchableHighlight, Image, Dimensions } from 'react-native'

import { LoadingView, GamePreviewRow, OpenSans } from '../../../../views/components'

import Icon from 'react-native-vector-icons/FontAwesome'

import { layout, colours } from '../../../../styles'


const screenWidth = Dimensions.get('window').width
const dimensions = {
  rowImg: {
    // width & height of image
    width: screenWidth/2 - 20,
    height: (screenWidth/2 - 20)/1.6
  }
}

export default class ProfileView extends Component {

  componentWillMount() {
    this.props.fetchProfile(this.props.profileId);
  }

  renderHeader() {
    return (
      <View style={layout.header}>
        <View style={{flex: 1}}>
          <Icon.Button
            name="angle-left"
            onPress={this.props._goBack}
            backgroundColor="transparent"
            style={{padding:5, borderRadius: 0}}
            size={28}
            color={colours.background.dark}
            iconStyle={{marginLeft: 8, backgroundColor: 'transparent'}} >
          </Icon.Button>
        </View>
        <View style={{flex: 3}}>
          <OpenSans textStyle={{textAlign: 'center'}}>Profile</OpenSans>
        </View>
        <View style={{flex: 1}}></View>
      </View>
    )
  }

  renderGames(items) {
    return items.map((item, i) => {
      const game = JSON.parse(item.data)
      return (
        <GamePreviewRow
        route={item}
        size={dimensions.rowImg}
        _handleNavigate={this.props._goToItem.bind(this)}
        key={item.gameId}
        data={item}
        game={game}
        disabled={true} />
      )
    })
  }

  renderFollowButton() {
    const { profile, followLoad, user } = this.props.state

    if(!user) return;

    const payload = {
      user: _.pick(user, ['firstName', 'lastName', 'avatar']),
      following: _.pick(profile, ['firstName', 'lastName', 'avatar'])
    }

    if(followLoad) return <LoadingView size='small' />
    if(profile.isFollowing) {
      return <TouchableHighlight style={styles.profileEdit} onPress={() => this.props.unfollowUser(user._id, profile._id)}><View><Text>Unfollow</Text></View></TouchableHighlight>
    } else {
      return <TouchableHighlight style={styles.profileEdit} onPress={() => this.props.followUser(user._id, profile._id, payload)}><View><Text>Follow</Text></View></TouchableHighlight>
    }
  }

  render() {
    const { profile, fetching } = this.props.state

    if(fetching) {
      return (
        <View>
          {this.renderHeader()}
          <LoadingView />
        </View>
      )
    }

    return <View  style={layout.container}>
      {this.renderHeader()}
      <ScrollView
        ref={(scrollView) => { this._scrollView = scrollView; }}
        automaticallyAdjustContentInsets={false}
        scrollEventThrottle={200}
        style={layout.content}
        contentContainerStyle={[styles.scrollContentContainer]}>

        <View style={styles.profileWrap}>
          <View style={styles.profileDetails}>
            <Image style={styles.profileAvatar} source={profile.avatar.small} />
            <Text style={styles.textName}>{profile.firstName} {profile.lastName}</Text>
          </View>

          <View style={styles.profileStats}>
            <View style={styles.profileNumbers}>
              <View style={styles.statSingle}>
                <Text style={styles.textNumber}>{profile.gameCount}</Text>
                <Text style={styles.textNumberTag}>games</Text>
              </View>
              <View style={styles.statSingle}>
                <Text style={styles.textNumber}>{profile.followersCount}</Text>
                <Text style={styles.textNumberTag}>followers</Text>
              </View>
              <View style={styles.statSingle}>
                <Text style={styles.textNumber}>{profile.followingCount}</Text>
                <Text style={styles.textNumberTag}>following</Text>
              </View>
            </View>
            <View>
              {this.renderFollowButton()}
            </View>
          </View>
        </View>

        {this.renderGames(profile.games)}

      </ScrollView>

    </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 40
  },
  scrollWrap: {
    marginTop: 40,
  },
  sortViewLoading: {
    marginTop: 40
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  profileWrap: {
    height: 150,
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light,
  },
  profileDetails: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 3
  },
  profileStats: {
    justifyContent: 'center',
    alignItems: 'stretch',
    flex: 5
  },
  profileAvatar: {
    height: 80,
    borderRadius: 40,
    width: 80,
    marginBottom: 5
  },
  profileNumbers: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 60,
    marginBottom: 5,
    marginRight: 30
  },
  profileEdit: {
    marginRight: 30,
    justifyContent: 'center',
    alignItems: 'center',
    height: 24,
    backgroundColor: colours.background.light
  },
  statSingle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textNumber: {
    fontSize: 15,
    fontWeight: '600',
    color: colours.background.dark
  },
  textNumberTag: {
    fontSize: 12,
    fontWeight: '500',
    color: colours.greyscale.medium
  },
  textName: {
    fontSize: 12,
    fontWeight: '600',
    color: colours.greyscale.dark
  },

  // edit profile
  head: {
    height: 40,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row'
  },
  changeAvatarWrap: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    height: 100,
    width: 100,
    marginBottom: 20,
    borderRadius: 50
  },
  details: {
    flex: 5,
    justifyContent: 'center',
    alignItems: 'stretch',
  }
});
