import '../../../../services/api/profile.api'
import '../../../../services/api/feed.api'

export function fetchProfile(_id) {
  return {
    type: 'FETCH_PROFILE',
    payload: profile.get(_id)
  }
}

// export function followProfile(userId, followId, payload) {
//   return {
//     type: 'FOLLOW_PROFILE',
//     payload: feed.following.add(userId, followId, payload)
//   }
// }
//
// export function unfollowProfile(userId, followId) {
//   return {
//     type: 'UNFOLLOW_PROFILE',
//     payload: feed.following.remove(userId, followId)
//   }
// }
