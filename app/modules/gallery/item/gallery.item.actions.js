import { Gallery } from '../../../services/api';

export function fetchSuggestedItems(userId, ids) {
  let  payload = {};
  payload.promise = Gallery.getSuggested(userId, ids);
  return {
    type: 'FETCH_SUGGESTED_ITEMS',
    payload
  }
}

export function changeCurrentItem(item) {
  return {
    type: 'CHANGE_CURRENT_ITEM',
    item
  }
}

export function removeVideoPlaceholder() {
  return {
    type: 'REMOVE_VIDEO_PLACEHOLDER'
  }
}
