import { connect } from 'react-redux'
import GalleryItem from './gallery.item.view'
import { fetchSuggestedItems, changeCurrentItem, removeVideoPlaceholder } from './gallery.item.actions'
import { upvote } from '../home/gallery.home.actions'
function mapStateToProps (store) {
  return {
    state: store.galleryItemReducer,
    user: store.userReducer,
    connectivity: store.connectivityReducer
  }
}

export default connect(
  mapStateToProps,
  {
    fetchSuggestedItems: (userId, ids) => fetchSuggestedItems(userId, ids),
    changeCurrentItem: (item) => changeCurrentItem(item),
    removeVideoPlaceholder: () => removeVideoPlaceholder(),
    upvote: (gameId, user, targetUserId) => upvote(gameId, user, targetUserId)
  }
)(GalleryItem)
