import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, ScrollView, TouchableHighlight, Dimensions, InteractionManager } from 'react-native'

import { LoadingView, GamePreviewRow } from '../../../views/components'

import { common, colours } from '../../../styles'

import { SocialBar, VideoPlayer, OpenSans } from '../../../views/components'

import { formatDate } from '../../../services/utils'

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
const route = {
  type: 'push',
  route: {
    key: 'comments'
  }
}

const screenWidth = Dimensions.get('window').width
const dimensions = {
  rowImg: {
    // width & height of image
    width: screenWidth/2 - 20,
    height: (screenWidth/2 - 20)*9/16
  },
  videoHeight: {
    height: screenWidth*9/16
  }
}

let game = null;

export default class GameView extends Component {

  componentWillMount() {
    game = {...this.props.item};
    try {
      game.data = JSON.parse(game.data)
    } catch(e) {}
    this.setState({showVideoPlaceholder: true})
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.fetchSuggestedItems()
      this.setState({showVideoPlaceholder: false})
    });
  }

  componentWillUpdate() {
    game = {...this.props.item};
    try {
      game.data = JSON.parse(game.data)
    } catch(e) {}
    // this.fetchSuggestedItems()
    // this.props.changeCurrentItemFinish();
  }

  fetchSuggestedItems() {
    const playerIds = game.playerIds.split(' ')
    const { fetching } = this.props.state
    const userId = this.props.user.user ?  this.props.user.user._id : null
    this.props.fetchSuggestedItems(userId, playerIds)
  }

  goToItem(item) {
    this.props._goToItem(item)
    // this.props.changeCurrentItem(item)
    // this.fetchSuggestedItems()
  }

  upvoteItem() {
    this.props.upvote(game.gameId, this.props.user.user, game.playerIds.split(' '))
  }

  videoView() {
    return <VideoPlayer enableFullscreenToggle={true} source={game.data.source.video} videoHeight={dimensions.videoHeight} _goBack={this.props._goBack}></VideoPlayer>
  }

  // <View style={[styles.fixed, styles.videoWrap, this.border('blue')]}>
  //   <TouchableHighlight onPress={ () => this.props._goBack()}><Text>Go Back</Text></TouchableHighlight>
  //   <VideoPlayer></VideoPlayer>
  // </View>

  decriptionBarView() {
    return <View style={styles.descBarContainer}>
      <View style={styles.descBarTitleWrap}>
        <View><OpenSans textStyle={styles.bold}>{game.title}</OpenSans></View>
        <View><OpenSans textStyle={styles.smallText}>{formatDate(game.createdAt)}</OpenSans></View>
      </View>
      <View style={styles.descBarSocialWrap}>
        <SocialBar
          upvotes={game.upvoteCount}
          comments={game.commentCount}
          views={game.playCount}
          voted={game.voted}
          vote={() => {this.upvoteItem()}}
          disabled={typeof(this.props.profileId) !== 'undefined'}
          connectivity={this.props.connectivity}/>
      </View>
    </View>
  }

  playersView() {
    return <View style={[styles.playersWrap]}>
      <View><Image source={game.data.player1.avatar.small} style={styles.avatar}/></View>
      <View><TouchableHighlight onPress={() => { this.props._goToProfile(game.data.player1._id) }}><View><OpenSans>{game.data.player1.firstName} {game.data.player1.lastName}</OpenSans></View></TouchableHighlight></View>
      <View><OpenSans>vs</OpenSans></View>
      <View><TouchableHighlight onPress={() => { this.props._goToProfile(game.data.player2._id) }}><View><OpenSans>{game.data.player2.firstName} {game.data.player2.lastName}</OpenSans></View></TouchableHighlight></View>
      <View><Image source={game.data.player2.avatar.small} style={styles.avatar}/></View>
    </View>
  }

  renderSuggested() {
    return this.props.state.suggested.map((item, i) => {
      const route = item
      const game = JSON.parse(item.data)
      return (
        <GamePreviewRow
          route={item}
          size={dimensions.rowImg}
          _handleNavigate={this.goToItem.bind(this)}
          key={item.gameId}
          data={item}
          game={game}
          disabled={true} />
      )
    })
  }

  customGameView() {
    return <View></View>
  }

  gamesView() {
    return <View>
      {this.renderSuggested()}
    </View>
  }

  render () {
    if(this.state.showVideoPlaceholder) {
      return <View style={[styles.videoWrap, dimensions.videoHeight]}>
        <LoadingView/>
        <ScrollView style={[styles.fixed, styles.scrollView]}>
          <View>
            {this.decriptionBarView(game)}
            {this.playersView(game)}
            {this.customGameView(game)}
            <View style={common.divider}>
              <Text>More from these users</Text>
            </View>
            <LoadingView />
          </View>
        </ScrollView>
      </View>
    }
    if(this.props.state.fetching) {
      return <View style={styles.container}>
                {this.videoView()}
                <ScrollView style={[styles.fixed, styles.scrollView]}>
                  <View>
                    {this.decriptionBarView(game)}
                    {this.playersView(game)}
                    {this.customGameView(game)}
                    <View style={common.divider}>
                      <Text>More from these users</Text>
                    </View>
                    <LoadingView />
                  </View>
                </ScrollView>
              </View>
    }
    return  <View style={styles.container}>
              {this.videoView()}
              <ScrollView style={[styles.fixed, styles.scrollView]}>
                <View>
                  {this.decriptionBarView(game)}
                  {this.playersView(game)}
                  {this.customGameView(game)}
                  <View style={common.divider}>
                    <Text>More from these users</Text>
                  </View>
                  {this.gamesView(game)}
                </View>
              </ScrollView>
            </View>
  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 2
    }
  }
}

const styles = StyleSheet.create({
  fixed: {
    position: 'absolute',
    left: 0,
    right: 0,
  },
  videoWrap: {
    top: 0,
    height: screenWidth*9/16
  },
  scrollView: {
    top: screenWidth*9/16,
    bottom: 20
  },
  container: {
    backgroundColor: colours.background.primary,
    flex: 1,
  },
  descBarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  descBarTitleWrap: {
    width: Dimensions.get('window').width*3/4 - 10,
  },
  descBarSocialWrap: {
    flex: 1
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20
  },
  playersWrap: {
    paddingTop: 5,
    paddingLeft: 10,
    paddingBottom: 5,
    paddingRight: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bold: {
    fontWeight: 'bold'
  },
  videoWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'black',
    zIndex: 9999999999999999
  },
  smallText: {
    fontSize: 10,
    color: colours.greyscale.medium
  }
});
