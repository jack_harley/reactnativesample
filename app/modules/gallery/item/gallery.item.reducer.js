import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  suggested: [],
  item: {},
  error: null,
  fetching: true,
  showVideoPlaceholder: true
}

function galleryItemState (state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_CURRENT_ITEM':
      return {
        ...state,
        item: action.item
      }
    case 'REMOVE_VIDEO_PLACEHOLDER':
      return {
        ...state,
        showVideoPlaceholder: false
      }
    case 'FETCH_SUGGESTED_ITEMS_PENDING':
      return {
        ...state,
        fetching: true,
        suggested: [],
      }
    case 'FETCH_SUGGESTED_ITEMS_FULFILLED':
      return {
        ...state,
        fetching: false,
        suggested: action.payload
      }
    case 'FETCH_SUGGESTED_ITEMS_REJECTED':
      return {
        fetching: false,
        error: true
      }
    default:
      return state
  }
}

export default galleryItemState
