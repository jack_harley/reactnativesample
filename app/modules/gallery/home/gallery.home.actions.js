import { User, Gallery } from '../../../services/api';

export function sortGalleryItems(sort, t) {
  return {
    type: 'SORT_GALLERY_ITEMS',
    payload: {
      sort: sort,
      t: t
    }
  }
}
export function fetchGalleryItems(params, userId) {
  let payload = params ? params : {};
  payload.promise = Gallery.get(params, userId);
  return {
    type: 'FETCH_GALLERY',
    payload
  }
}

export function showFilterDraw(type, options) {
  return {
    type: 'OPEN_SORT_DRAW',
    payload: {
      type: type,
      options: options
    }
  }
}

export function hideFilterDraw() {
  return {
    type: 'CLOSE_SORT_DRAW'
  }
}

export function layoutSwitch(layout) {
  return {
    type: 'LAYOUT_SWITCH',
    layout
  }
}

export function upvote(gameId, user, targetUserId) {
  return {
    type: 'UPVOTE_ITEM',
    payload: Gallery.upvote(gameId, user, targetUserId)
  }
}

export function updateSearchQry(qry) {
  return {
    type: 'GALLERY_UPDATE_SEARCH_QRY',
    qry
  }
}

export function searchType(kind) {
  return {
    type: 'GALLERY_UPDATE_SEARCH_TYPE',
    kind
  }
}

export function search(type, str) {

  let search = null

  switch (type) {
    case 'user':
      search = User.find(str)
      break
    default:
      search = Gallery.find(str)
  }

  return {
    type: 'GALLERY_SEARCH',
    payload: search
  }
}
