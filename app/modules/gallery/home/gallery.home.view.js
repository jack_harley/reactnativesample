import _chunk from 'lodash/chunk'
import React, { Component } from 'react'
import { View, StyleSheet, ScrollView, StatusBar, Text, Image, Dimensions, TouchableOpacity, ListView } from 'react-native'

import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable';

import { layout, colours } from '../../../styles'
import { LoadingView, GamePreviewGrid, GamePreviewRow, OpenSans, TextInput } from '../../../views/components'
import SortView from './gallery.sort.view'

import { galleryIcon, rowIcon } from '../../../styles/icons'


const screenWidth = Dimensions.get('window').width
const dimensions = {
  gridImg: {
    width: screenWidth/2,
    height: screenWidth/2,
  },
  rowImg: {
    // width & height of image
    width: screenWidth/2 - 20,
    height: (screenWidth/2 - 20)/1.6,
  }
}

let _timer = 0
function timer(cb) {
    if (_timer)
        window.clearTimeout(_timer);
    _timer = window.setTimeout(function() {
        cb();
    }, 1000);
}

/* =============================================================================
  GalleryView Component
    Takes a list of items with images and displays them in a gallery view.

    - items (Array of items to display)
==============================================================================*/
export default class GalleryView extends Component {

  componentWillMount() {
    const { fetching, fetchData } = this.props.state
    const userId = this.props.user.user ? this.props.user.user._id : null
    if(fetching && fetchData) this.props.fetchGallery(null, userId);
  }

  componentDidUpdate() {
    const { fetching, fetchData } = this.props.state
    const userId = this.props.user.user ? this.props.user.user._id : null
    if(fetching && fetchData) this.props.fetchGallery(this.props.state.sortOptions, userId);
  }

  goToItem(route) {
    this.props._handleNavigate(route)
  }

  sortOptionsChange(key, payload) {
    this.props.showFilterDraw(key, payload)
  }

  sortGalleryItems(sort, t) {
    this.props.hideFilterDraw()
    this.props.sortGalleryItems(sort, t)
  }

  layoutSwitch(layout) {
    this.props.layoutSwitch(layout)
  }

  upvoteItem(game) {
    this.props.upvote(game.gameId, this.props.user.user, game.playerIds.split(' '))
  }

  doSearch(qry) {
    if(!this.props.state.searchQry.length) return
    this.props.search(this.props.state.searchType, this.props.state.searchQry)
  }

  search(qry) {
    this.props.updateSearchQry(qry)
    timer(this.doSearch.bind(this))
  }

  _renderHeader() {
    return <View style={layout.header}>
        <View style={{flex: 1}}>

        </View>
        <View style={{flex: 3, alignItems: 'center'}}>
          <OpenSans textStyle={{fontSize: 14}}>Gallery</OpenSans>
        </View>
        <View style={{flex: 1, alignItems: 'flex-end'}}>
          { this.props.state.layout == 'gallery'
            &&
            <View style={styles.right}>
              <Icon.Button
                name="search"
                onPress={() => this.layoutSwitch('search')}
                backgroundColor="transparent"
                style={{padding:0, borderRadius: 0}}
                size={22}
                color={colours.greyscale.dark}
                iconStyle={styles.layoutCtrl}>
              </Icon.Button>
            </View>
          }
          {
            this.props.state.layout == 'search'
            &&
            <View style={styles.right}>
              <Icon.Button
                name="th-list"
                onPress={() => this.layoutSwitch('gallery')}
                backgroundColor="transparent"
                style={{padding:0, borderRadius: 0}}
                size={22}
                color={colours.greyscale.dark}
                iconStyle={styles.layoutCtrl}>
              </Icon.Button>
            </View>
          }
        </View>
      </View>
  }

  _renderRows(items) {
    if(this.props.state.fetching) return <LoadingView/>
    return items.map((item, i) => {
      const route = {
        type: 'push',
        route: {
          key: 'gallery.item',
          item: item
        }
      }
      const game = JSON.parse(item.data);
      return (
        <GamePreviewRow
          route={route}
          size={dimensions.rowImg}
          _handleNavigate={this.goToItem.bind(this)}
          key={item.gameId}
          data={item}
          game={game}
          disabled={true} />
      )
    })
  }

  _renderUserRow(person) {
    return <TouchableOpacity onPress={ () => { this.props._goToProfile(person._id) } } style={styles.row}>
      <View style={styles.imgWrap}>
        <Image style={styles.img} source={person.avatar.small}/>
      </View>
      <View style={styles.details}>
        <Text style={styles.text}>{person.firstName} {person.lastName}</Text>
      </View>
    </TouchableOpacity>
  }

  _renderUserRows(items) {
    if(this.props.state.fetching) return <LoadingView/>
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    return <ScrollView style={[layout.container, styles.dark, styles.body]}>
      <ListView
        dataSource={ds.cloneWithRows(items)}
        renderRow={this._renderUserRow.bind(this)}
        enableEmptySections />
    </ScrollView>
  }

  _renderResults() {
    const { layout, searchQry, searchType, searchResults, items } = this.props.state

    if( layout == 'search' && searchQry.length > 0 && searchType == 'video' ) {
      return this._renderRows(searchResults.video)
    } else if ( layout == 'search' && searchType == 'user' ) {
      return this._renderUserRows(searchResults.user)
    } else {
      return this._renderRows(items)
    }

  }

  _renderControls() {
    if(this.props.state.layout == 'gallery') {
      return <SortView
        sortOptionsChange={this.sortOptionsChange.bind(this)}
        showSortOptions={this.props.state.showSortOptions}
        sortOptions={this.props.state.sortOptions}
        displaySortOptions={this.props.state.displaySortOptions}
        sortGalleryItems={this.sortGalleryItems.bind(this)}
        hideFilterDraw={this.props.hideFilterDraw} />
    } else {
      return <View>
        <View>
          <TextInput
            currentInput={ this.props.state.searchQry }
            onInput={ (qry) => this.search(qry) }
            search={true}
            size={18}
            color={colours.greyscale.medium} />
        </View>
          {
            this.props.state.searchType == 'video'
            &&
            <View style={styles.searchTypeBtnWrap}>
              <TouchableOpacity style={[styles.searchTypeBtn, styles.searchTypeSelected]}>
                <Text style={styles.searchTypeBtnText}>VIDEOS</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={ () => { this.props.searchType('user') }} style={styles.searchTypeBtn}>
                <Text style={styles.searchTypeBtnText}>PEOPLE</Text>
              </TouchableOpacity>
            </View>
          }
          {
            this.props.state.searchType == 'user'
            &&
            <View style={styles.searchTypeBtnWrap}>
              <TouchableOpacity onPress={ () => { this.props.searchType('video') }} style={styles.searchTypeBtn}>
                <Text style={styles.searchTypeBtnText}>VIDEOS</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.searchTypeBtn, styles.searchTypeSelected]}>
                <Text style={styles.searchTypeBtnText}>PEOPLE</Text>
              </TouchableOpacity>
            </View>
          }
      </View>
    }
  }

  _renderContent() {
    return <ScrollView
      ref={(scrollView) => { _scrollView = scrollView; }}
      automaticallyAdjustContentInsets={false}
      scrollEventThrottle={200}
      style={layout.content}
      contentContainerStyle={[styles.scrollContentContainer]}>
        {this._renderControls()}
        {this._renderResults()}
    </ScrollView>
  }

  render () {
    // Check if there are any items in the current state
    // If there are, display them. If not, display a loader and sent an ajax
    // request to populate items.
    const { fetching, fetchData } = this.props.state

    return <View style={layout.container}>
      <StatusBar hidden={true} />
      {this._renderHeader()}
      {this._renderContent()}
    </View>

  }

  border(color) {
    return {
      borderColor:color,
      borderWidth: 4
    }
  }

}


const styles = StyleSheet.create({
  layoutCtrlWrap: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  layoutCtrl: {
    backgroundColor: 'transparent'
  },
  searchWrap: {
    width: 50
  },
  sortViewLoading: {
    marginTop: 50
  },
  scrollContentContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    flexWrap: 'wrap'
  },
  itemRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  searchTypeBtnWrap: {
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  searchTypeBtn: {
    flex: 1,
    alignItems: 'center',
    padding: 10
  },
  searchTypeSelected: {
    borderBottomColor: colours.greyscale.dark,
    borderBottomWidth: 2,
  },
  searchTypeBtnText: {
    fontSize: 10,
    color: colours.greyscale.dark,
    fontWeight: 'bold'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    padding: 5,
    paddingLeft: 40
  },
  imgWrap: {
    flex: 3
  },
  img: {
    height: 40,
    width: 40,
    borderRadius: 20
  },
  details: {
    justifyContent: 'center',
    flex: 10
  },
  right: {
    justifyContent: 'flex-end'
  }
});
