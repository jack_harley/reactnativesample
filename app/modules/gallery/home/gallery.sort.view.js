import React, { Component } from 'react'
import { View, TouchableHighlight, StyleSheet, Image, Text, LayoutAnimation } from 'react-native'

import { colours } from '../../../styles'

/* =============================================================================
  GalleryItem Component
    Displays a single gallery item.

    - item (item obj)
==============================================================================*/
let showing = null;
export default class GallerySort extends Component {

  showOptions(key) {
    if(this.props.showSortOptions) {
      this.props.hideFilterDraw()
      return
    }
    showing = key;
    let payload = [];
    switch (key) {
      case 'sort':
        payload = [
          { key: 'latest', text: 'latest', image: {uri: 'https://placehold.it/50x50'} },
          { key: 'views', text: 'viewed', image: {uri: 'https://placehold.it/50x50'} },
          { key: 'rating', text: 'upvotes', image: {uri: 'https://placehold.it/50x50'} }
        ];
        break;
      case 'range':
        payload = [
          { key: 'week', text: 'week', image: {uri: 'https://placehold.it/50x50'} },
          { key: 'month', text: 'month', image: {uri: 'https://placehold.it/50x50'} },
          { key: 'year', text: 'year', image: {uri: 'https://placehold.it/50x50'} },
          { key: 'all', text: 'all time', image: {uri: 'https://placehold.it/50x50'} }
        ];
        break;
      default:
        return;
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.props.sortOptionsChange(key, payload);
  }

  filterGallery(option) {
    switch (showing) {
      case 'sort':
        this.props.sortGalleryItems(option, null);
        break
      case 'range':
        this.props.sortGalleryItems(null, option);
        break
      default:
        return
    }
    // this.props.hideFilterDraw()
  }

  render () {

    const route = {
      type: 'push',
      route: {
        key: 'item'
      }
    }

    const displaySortOptions = this.props.displaySortOptions.options.map((option, i) => {
      return (
        <TouchableHighlight key={i} onPress={ () => this.filterGallery(option.key) }>
          <View>
            <Text style={styles.chooseFont}>{option.text}</Text>
          </View>
        </TouchableHighlight>
      )
    })

    const showSortOptions = this.props.showSortOptions ? { height: 40 } : { height: 0 }

    return (
      <View>
        <View style={[styles.chooseWrap, showSortOptions]}>{displaySortOptions}</View>
        <View style={styles.currentWrap}>
          <TouchableHighlight onPress={ () => this.showOptions('sort') }><View><Text style={styles.currentFont}>{this.props.sortOptions.sort}</Text></View></TouchableHighlight>
          <TouchableHighlight onPress={ () => this.showOptions('range') }><View><Text style={styles.currentFont}>{this.props.sortOptions.t}</Text></View></TouchableHighlight>
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  chooseFont: {
    color: 'white'
  },
  chooseWrap: {
    flex: 1,
    flexDirection: 'row',
    height: 40,
    backgroundColor: colours.greyscale.dark,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  currentWrap: {
    flex: 1,
    flexDirection: 'row',
    height: 40,
    backgroundColor: colours.background.primary,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  currentFont: {
    fontWeight: '600',
    fontSize: 10,
    color: colours.greyscale.dark
  },
})
