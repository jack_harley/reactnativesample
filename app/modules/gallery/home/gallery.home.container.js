import { connect } from 'react-redux'
import GalleryItems from './gallery.home.view'
import { fetchGalleryItems, showFilterDraw, hideFilterDraw, layoutSwitch, sortGalleryItems, updateSearchQry, searchType, search } from './gallery.home.actions'
function mapStateToProps (state) {
  return {
    state: state.galleryHomeReducer,
    user: state.userReducer
  }
}

export default connect(
  mapStateToProps,
  {
    fetchGallery: (params, userId) => fetchGalleryItems(params, userId),
    sortGalleryItems: (sort, filter, t) => sortGalleryItems(sort, filter, t),
    showFilterDraw: (type, options) => showFilterDraw(type, options),
    hideFilterDraw: () => hideFilterDraw(),
    layoutSwitch: (layout) => layoutSwitch(layout),
    updateSearchQry: (qry) => updateSearchQry(qry),
    searchType: (kind) => searchType(kind),
    search: (type, qry) => search(type, qry)
  }
)(GalleryItems)
