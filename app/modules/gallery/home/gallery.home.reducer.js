import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  key: 'gallery.items',
  items: [],
  sortOptions: {
    sort: 'latest',
    t: 'week'
  },
  error: null,
  fetching: true,
  fetchData: true,
  showSortOptions: false,
  layout: 'gallery',
  displaySortOptions: {
    type: '',
    options: []
  },
  searchView: false,
  searchQry: '',
  searchType: 'video',
  searchResults: {
    video: [],
    user: []
  }
}

function galleryItemsState (state = initialState, action) {
  switch (action.type) {
    case 'SORT_GALLERY_ITEMS':
      return {
        ...state,
        sortOptions: {
          sort: action.payload.sort ? action.payload.sort : state.sortOptions.sort,
          t: action.payload.t ? action.payload.t : state.sortOptions.t
        },
        fetching: true,
        fetchData: true
      }
    case 'FETCH_GALLERY_PENDING':
      return {
        ...state,
        fetching: true,
        fetchData: false,
        items: []
      }
    case 'FETCH_GALLERY_FULFILLED':
      return {
        ...state,
        fetching: false,
        items: action.payload
      }
    case 'FETCH_GALLERY_REJECTED':
      return {
        fetching: false,
        error: true
      }
    case 'OPEN_SORT_DRAW':
      return {
        ...state,
        showSortOptions: !state.showSortOptions || action.payload.type != state.sortOptions.type ? true : false,
        displaySortOptions: {
          type: action.payload.type,
          options: action.payload.options
        }
      }
    case 'CLOSE_SORT_DRAW':
      return {
        ...state,
        showSortOptions: false,
      }
    case 'LAYOUT_SWITCH':
      return {
        ...state,
        layout: action.layout
      }
    case 'GALLERY_UPDATE_SEARCH_QRY':
      return {
        ...state,
        searchQry: action.qry
      }
    case 'GALLERY_UPDATE_SEARCH_TYPE':
      return {
        ...state,
        searchType: action.kind
      }
    case 'GALLERY_SEARCH_PENDING':
      return {
        ...state,
        fetching: true
      }
    case 'GALLERY_SEARCH_REJECTED':
      return {
        ...state,
        fetching: false
      }
    case 'GALLERY_SEARCH_FULFILLED':
      results = state.searchResults
      results[state.searchType] = action.payload
      return {
        ...state,
        fetching: false,
        searchResults: results
      }
    default:
      return state
  }
}

export default galleryItemsState
