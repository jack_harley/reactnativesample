import { NavigationExperimental } from 'react-native'
const {
 StateUtils: NavigationStateUtils
} = NavigationExperimental

// Keys - gallery, item, comments
const initialState = {
  index: 0,
  key: 'gallery.nav',
  routes: [
    {
      key: 'gallery'
    }
  ]
}

function galleryState (state = initialState, action) {
  switch (action.type) {
    case 'GALLERY_PUSH_ROUTE':
      let newState = {...state}
      if(action.route.key.indexOf('gallery.item') >= 0 || action.route.key.indexOf('profile') >= 0) {
        action.route.key = action.route.key + '_' + state.index
      }
      else if (state.routes[state.index].key === (action.route && action.route.key)) {
        return state
      }
      return NavigationStateUtils.push(newState, action.route)
    case 'GALLERY_POP_ROUTE':
      if (state.index === 0 || state.routes.length === 1) return state
      return NavigationStateUtils.pop(state)
    default:
      return state
  }
}

export default galleryState
