import React, { Component } from 'react'
import GalleryView        from '../home/gallery.home.container'
import GameView           from '../item/gallery.item.container'
import GalleryCommentView from '../item/comments/gallery.comments.view'
import ProfileView        from '../item/profile/profile.container'

import {
  BackAndroid,
  NavigationExperimental,
  View
} from 'react-native'

const {
  Reducer: NavigationTabsReducer,
  CardStack: NavigationCardStack
} = NavigationExperimental

class GalleryRoot extends Component {
  constructor (props) {
    super(props)
    this._renderScene = this._renderScene.bind(this)
    this._handleBackAction = this._handleBackAction.bind(this)
  }
  componentDidMount () {
    BackAndroid.addEventListener('hardwareBackPress', this._handleBackAction)
  }
  componentWillUnmount () {
    BackAndroid.removeEventListener('hardwareBackPress', this._handleBackAction)
  }
  _renderScene (props) {
    const { route } = props.scene
    let key = route.key
    key = key.indexOf('_') >= 0 ? key.substring(0, key.indexOf('_')) : key

    switch (key) {
      case 'gallery':
        return (
          <GalleryView
            _handleNavigate={this._handleNavigate.bind(this)}
            state={this.props.state}
            fetchGallery={this.props.fetchGallery}
            _goToProfile={this._goToProfile.bind(this)}
           />
        )
      case 'gallery.item':
        return (
          <GameView
            item={route.item}
            state={this.props.state}
            _goBack={this._handleBackAction.bind(this)}
            _goToProfile={this._goToProfile.bind(this)}
            _goToComments={this._goToComments.bind(this)}
            _goToItem={this._goToItem.bind(this)} />
        )
      case 'comments':
        return <GalleryCommentView _goBack={this._handleBackAction.bind(this)} />
      case 'profile':
        return (
          <ProfileView
            profileId={route.profileId}
            state={this.props.state}
            _goBack={this._handleBackAction.bind(this)}
            _goToItem={this._goToItem.bind(this)} />
        )
      default:
        return <View />
    }
  }
  _goToItem (item) {
    this._handleNavigate({
      type: 'push',
      route: {
        item: item,
        key: 'gallery.item'
      }
    });
  }
  _goToProfile (_id) {
    this._handleNavigate({
      type: 'push',
      route: {
        profileId: _id,
        key: 'profile'
      }
    });
  }
  _goToComments () {
    this._handleNavigate({
      type: 'push',
      route: {
        key: 'comments'
      }
    });
  }
  _handleBackAction () {
    if (this.props.state.index === 0) {
      return false
    }
    this.props.popRoute()
    return true
  }
  _handleNavigate (action) {
    switch (action && action.type) {
      case 'push':
        this.props.pushRoute(action.route)
        return true
      case 'back':
      case 'pop':
        return this._handleBackAction()
      default:
        return false
    }
  }
  render () {
    return (
      <NavigationCardStack
        style={{flex: 1}}
        navigationState={this.props.state}
        onNavigate={this._handleNavigate.bind(this)}
        renderScene={this._renderScene} />
    )
  }
}

export default GalleryRoot
