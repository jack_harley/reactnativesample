export function push(route) {
  return {
    type: 'GALLERY_PUSH_ROUTE',
    route
  }
}

export function pop () {
  return {
    type: 'GALLERY_POP_ROUTE'
  }
}
