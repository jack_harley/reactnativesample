import { connect } from 'react-redux'
import GalleryNav from './gallery.nav.view'
import { push, pop } from './gallery.nav.actions'
function mapStateToProps (state) {
  return {
    state: state.galleryNavReducer
  }
}

export default connect(
  mapStateToProps,
  {
    pushRoute: (route) => push(route),
    popRoute: ()=> pop()
  }
)(GalleryNav)
