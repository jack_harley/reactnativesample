import { StyleSheet } from 'react-native'
import { colours } from './variables'

export default layout = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colours.background.primary
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colours.background.light,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  },
  headerCenter: {},
  headerSide: {
    width: 50,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  content: {
    marginTop: 40,
    flex: 1,
    marginBottom: 50
  }
})
