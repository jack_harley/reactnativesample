const colours = {
  greyscale: {
    light: '#E9E9E9',
    medium: '#535353',
    dark: '#313131'
  },
  breakLines: {
    dark: '#242424'
  },
  background: {
    primary: 'white',
    light: '#f4f4f4',
    medium: '#1b1b1c',
    dark: '#161616'
  },
  scheme: {
    primary: '#8cdfa8',
    secondary: '#EC6C55',
    tertiary: '#87BFD2'
  },
  opaque: {
    primary: 'rgba(140, 223, 168, 0.4)',
    secondary: 'rgba(236, 108, 85, 0.4)',
    tertiary: 'rgba(135, 191, 210, 0.4)'
  }
}

export { colours }
