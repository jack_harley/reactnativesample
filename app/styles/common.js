import { StyleSheet } from 'react-native'
import { colours } from './variables'

export default common = StyleSheet.create({
  divider: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.background.light,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: colours.greyscale.light,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colours.greyscale.light
  }
})
