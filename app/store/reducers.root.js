import { combineReducers } from 'redux'
import tabReducer from '../modules/navigation/tabs.reducer'

import galleryNavReducer from '../modules/gallery/nav/gallery.nav.reducer'
import galleryHomeReducer from '../modules/gallery/home/gallery.home.reducer'
import galleryItemReducer from '../modules/gallery/item/gallery.item.reducer'

import userNavReducer from '../modules/user/nav/user.nav.reducer'
import userReducer from '../modules/user/home/user.home.reducer'
import userContentReducer from '../modules/user/content/user.content.reducer'

import feedNavReducer from '../modules/feed/nav/feed.nav.reducer'
import feedReducer from '../modules/feed/feed.reducer'

import profileReducer from '../modules/gallery/item/profile/profile.reducer'

import gamesHomeReducer from '../modules/games/home/games.home.reducer'
import gamesNavReducer from '../modules/games/nav/games.nav.reducer'
import challengesReducer from '../modules/games/home/views/Challenges/challenges.reducer'

import timedDuelReducer from '../modules/games/games/timedDuel/timedDuel.game.reducer'

import createTimedDuelReducer from '../modules/games/home/create/timedDuel/timed.duel.create.reducer'

import uploaderReducer from '../views/components/uploader/uploader.reducer'

import connectivityReducer from '../modules/connectivity/connectivity.reducer'


const rootReducer = combineReducers({
  tabReducer,
  galleryNavReducer,
  galleryHomeReducer,
  galleryItemReducer,
  userNavReducer,
  userReducer,
  userContentReducer,
  feedNavReducer,
  feedReducer,
  profileReducer,
  gamesHomeReducer,
  gamesNavReducer,
  challengesReducer,
  uploaderReducer,
  connectivityReducer,
  createTimedDuelReducer,
  timedDuelReducer
})

export default rootReducer
