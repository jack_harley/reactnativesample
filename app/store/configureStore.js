import { applyMiddleware, createStore } from 'redux'
import rootReducer from '../store/reducers.root'

// Middleware
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

export default function configureStore() {
  // const middleware = applyMiddleware(promise(), thunk, logger())
  const middleware = applyMiddleware(promise(), thunk, logger())
  const store = createStore(rootReducer, middleware)
  return store
}
